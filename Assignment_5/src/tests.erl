%% simple test
-module(tests).
-compile(export_all).
simple_test() ->
    {ok, MR}  = mr:start(),
    {ok, Sum} = mr:job(MR,
                       4,
                       fun(X) -> X end,
                       {fun(X, Acc) -> X + Acc end, multi},
                       0,
                       lists:seq(1,10)),
    {ok, Fac} = mr:job(MR,
                       20,
                       fun(X) -> X end,
                       {fun(X, Acc) -> X * Acc end, multi},
                       1,
                       lists:seq(1,20)),
    mr:stop(MR),
    {Sum, Fac}.
    %Sum.

word_count() ->
  M_func = fun(Word) -> 
                 {Word, 1} 
               end,
  R_func = fun(V1, Acc) ->
             Acc + V1
           end,
  {ok, MR} = mr:start(),
  {ok, Res} = mr:job(MR,
                       4,
                       M_func,
                       {R_func, multi},
                       0,
                       [this, is, a, boy, 
                        this, is, a, girl, 
                        this, is, lovely, boy]),
  mr:stop(MR),
  Res.
