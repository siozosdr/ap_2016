-module(mr).

-behaviour(gen_server).
-export([start/0, job/6, stop/1]).

%% gen_server callbacks
-export([init/1, terminate/2, 
	handle_call/3, handle_cast/2, 
	handle_info/2, code_change/3]).

%======== API

start()	->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


stop(Pid) ->
	gen_server:cast(?MODULE, stop_master).


job(Pid, NWorkers, MapFun, RedFun, Initial, Data) ->
	Res = gen_server:call(?MODULE, {Pid, NWorkers, MapFun, RedFun, Initial, Data}, infinity),
	%io:format("Final result of job:~p~n", [Res]),
	{ok, Res}.
	
%===== Internal functions


init(_) ->
	process_flag(trap_exit,true),
	{ok,[]}.

handle_info({'EXIT', Pid, Reason}, State) ->
    %io:format("EXIT code caught..."),
    {noreply, State};
handle_info(_,_) -> undefined.
handle_call({Pid, NWorkers, MapFun, {RedFun, NumOfReducers}, Initial, Data},_From,State) -> 
	{NMappers, NReducers} = split_workers(NWorkers, NumOfReducers),
	%% Spawn reducers
	Reducers = repeat_f(NReducers,
		fun(_) ->
			gen_server:start_link(mr_reducer, [Initial, RedFun], [])
		end),
	ReducerPids = lists:map(fun({_, Pid}) -> Pid end, Reducers),
	%io:format("Reducers: ~p~n", [ReducerPids]),
	% Spawn mappers
	Mappers = repeat_f(NMappers,
		fun(_) -> 
			gen_server:start_link(mr_mapper, [ReducerPids, MapFun], [])
		end),
	MapperPids = lists:map(fun({_, Pid}) -> Pid end, Mappers),
	%io:format("Mappers: ~p~n", [MapperPids]),
	%gen_server:cast(mapper1, crash_bitch),
	%Res = gen_server:start_link(mr_reducer, [], []),
	%%io:format("Res: ~p~n", [Res]),

	% send data to mappers
	Extract_fun = 
		fun(N) ->
			DataChunk = lists:nth(N+1, Data),
			% find mapper and send datachunk
			%io:format("DataChunk sent:~p~n",[DataChunk]),
			Mapper = find_worker(MapperPids),
			gen_server:cast(Mapper, {map, DataChunk})
		end,
	repeat_f(length(Data), Extract_fun),
	timer:sleep(2000),




	%%io:format("Collect all data from reduce processes~n"),
	All_results = 
		repeat_f(length(ReducerPids),
		fun(N) ->
			Res = collect(lists:nth(N+1, ReducerPids)),
			%io:format("Result collected:~p~n", [Res]),
			Res
		end),
	%io:format("All_results:~p~n", [All_results]),
	Res = lists:flatten(All_results),


	%final aggregation
	Reducer_proc = find_worker(ReducerPids),
	lists:foreach(
		fun({K, V}) ->
					gen_server:cast(Reducer_proc, {reduce, {K, V}})
		end, Res),

	Final_results = collect(Reducer_proc),
	%io:format("FINAL_results:~p~n", [Final_results]),
	FinFlat = lists:flatten(Final_results),
	

	%io:format("flattened result:~p~n", [FinFlat]),
	{reply, FinFlat, State}.

handle_cast(stop_master,_State) -> {stop, normal, _State};
handle_cast(Msg, _State) -> 
	%io:format("Uncaught cast message: ~p~n", [Msg]),
	ok.



code_change(_,_,_) -> undefined.


%%============ Helper functions

%%% Collect result synchronously from 
%%%   a reducer process
collect(Reduce_proc) ->
	gen_server:call(Reduce_proc, collect, infinity).

split_workers(NWorkers, single) ->
	{NWorkers-1, 1};
split_workers(NWorkers, multi) ->
	if 
		NWorkers - NWorkers/2 =< 1 ->
			{NWorkers-1, 1};
		true ->
			{round(NWorkers/2),round(NWorkers/2)}
	end.

repeat_f(N,F) ->
	%io:format("N:~p~n", [N]),
	lists:map(F, lists:seq(0, N-1)).

terminate(normal, State) ->
	ok;
terminate(Msg, State) -> 
	%io:format("MSG:~n"),
	ok.
child_spec(WID) -> #{id => WID,
                 start => {mr_mapper, start, []},
                 restart => transient,
                 shutdown => 5000,
                 type => worker,
                 modules => [mr_mapper]}.
%%% Identify the mapper process by random
find_worker(Processes) ->
  case random:uniform(length(Processes)) of
    0 ->
      find_worker(Processes);
    N ->
      lists:nth(N, Processes)
  end.
