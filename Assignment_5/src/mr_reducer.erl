-module(mr_reducer).
-behaviour(gen_server).
-export([start/0]).
-export([init/1, terminate/2, 
	handle_call/3, handle_cast/2, 
	handle_info/2, code_change/3]).

start() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

terminate(normal, State) ->
	io:format("child created"),
	ok.

init([Initial, ReduceFun]) ->
	io:format("Reducer init~n"),
	{ok,{Initial, ReduceFun, dict:new()}}.

handle_info(_,_) -> undefined.
handle_call(collect,_From, {Acc, ReduceFun, ValueDict}) ->
	%io:format("Collect message caught!~n"),
	Res = lists:map(fun({K,V})-> {K,V} end,dict:to_list(ValueDict)),
	%io:format("Accumulator:~p~n", [Acc]),
	%io:format("Result sent without keys:~p~n", [Res]),
	{reply, Res,{Acc, ReduceFun, dict:new()}}.

handle_cast({reduce, {K, V}},{Initial, ReduceFun, ValueDict}) -> 
	%io:format("Begin reduce with:~p,~p~n",[K,V]),
	Guard1 = dict:is_key(K, ValueDict),

	if 
		Guard1 =:= false ->
			Acc = Initial,
			Res = ReduceFun(V, Acc),
			%io:format("Reduce result:~p~n",[Res]),
			NewVD = dict:store(K, Res, ValueDict);
		Guard1 ->
			Acc = dict:fetch(K, ValueDict),
			%io:format("K,V,Acc:~p,~p,~p~n",[K, V, Acc]),
			Res = ReduceFun(V, Acc),
			%io:format("Reduce result:~p~n",[Res]),
			NewVD = dict:update(K, fun(Acc) -> ReduceFun(V, Acc) end, ValueDict)
	end,
	%Acc = case get(K, ValueDict) of
	%		Current_acc -> Current_acc;
	%		_ -> Initial
	%	  end,
	%Res = ReduceFun(V, Acc),
	%io:format("Reduce result:~p~n",[Res]),
	%NewVD = dict:update(K, Res, Res,ValueDict),
	%put(K, ReduceFun(V, Acc)),
	{noreply, {Initial, ReduceFun, NewVD}};
	
handle_cast(crash_bitch,State) ->
	%io:format("Received crash"),
	throw(craash),
	{noreply, State}.

code_change(_,_,_) -> undefined.
