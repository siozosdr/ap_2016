-module(mr_mapper).
-behaviour(gen_server).
-export([start/0]).
-export([init/1, terminate/2, 
	handle_call/3, handle_cast/2, 
	handle_info/2, code_change/3]).

start() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

terminate(normal, State) ->
	io:format("child created"),
	ok.

init([ReducerPids, MapFun]) ->
	{ok, {ReducerPids, MapFun}}.

handle_info(Msg,_) -> 
	io:format("Uncaught Info: ~p~n", [Msg]).

handle_call(Msg, _From, _State) ->
	io:format("Uncaught Call: ~p~n", [Msg]).

handle_cast({map, Data}, {ReducerPids, MapFun}) -> 
	IntermediateResults = MapFun(Data),
	%io:format("Map function produce: ~p~n", [IntermediateResults]),
	lists:foreach(
		fun(Arg) ->
			case Arg of
				{K, V} ->
					Reducer_proc = 
						find_reducer(ReducerPids, K),
					gen_server:cast(Reducer_proc, {reduce, {K, V}});
				V -> 
					Reducer_proc = 
						find_reducer(ReducerPids, V),
					gen_server:cast(Reducer_proc, {reduce, {0, V}})
			end
		end, [IntermediateResults]),
	{noreply, {ReducerPids, MapFun}}.

code_change(_,_,_) -> undefined.


%%========= Internal functions
%%% Identify the reducer process by 
%%%   using the hashcode of the key
find_reducer(Processes, Key) ->
  Index = erlang:phash(Key, length(Processes)),
  lists:nth(Index, Processes).
