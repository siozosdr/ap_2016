-module(part_two).
-export([task1/0]).
task1() ->
	{_,Tracks} = read_mxm:from_file("mxm_dataset_test.txt"),
	{_, MR} = mr:start(),
	M_func = fun(Track) ->
		{_, _, WordCounts} = read_mxm:parse_track(Track),
		WordCounts
	end,
	R_func = fun(WordCounts, Acc) ->
		WordCounts ++ Acc
	end,

	{ok, Results} = mr:job(MR,
							10,
							M_func,
							{R_func, multi},
							[],
							Tracks),
	%io:format("results:~p~n",[Results]),
	M2_func = fun({_, Count}) -> 
		Count 
	end,
	R2_func = fun(Count, Acc) -> 
		io:format("Count,Acc:~p~p~n", [Count, Acc]),
		Count + Acc 
	end, 
	{ok, Sum} = mr:job(MR, 
						4,
						M2_func,
						{R2_func, single},
						0,
						Results),																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												
	mr:stop(MR),
	Sum.