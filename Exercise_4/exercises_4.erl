-module(exercises_4).
-compile(export_all).

start() -> spawn(fun () -> loop() end).


reverse(Pid, String) ->
	request_reply(Pid, {reverse,String}).

request_reply(Pid, Request) ->
	Pid ! {self(), Request},
	receive
		{Pid, {ok, Response}} -> Response
	end.

loop() ->
	receive
		{From, {reverse, String}} -> 
			From ! {self(), {ok, reverse_string(String)}},
		loop()
	end.

reverse_string([]) -> [];
reverse_string([H|T]) -> reverse_string(T)++[H].