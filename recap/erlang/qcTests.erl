-module(qcTests).
-compile(export_all).
-include_lib("eqc/include/eqc.hrl").
del_test() ->
	% delete element in the middle of list
	lists:delete(2, [1,2,3]) =:= [1,3],
	lists:delete(2, [2,1,3]) =:= [1,3],
	% delete element from list where it isn't
	lists:delete(2,[]) =:= [],
	lists:delete(2, [4,56]) =:= [4,56],
	% delete duplicate element	
	lists:delete(2, [2,1,2,3]) =:= [1,2,3],
	lists:delete(2, [1,2,3,2]) =:= [1,3,2].

prop_delete_0() ->
	?FORALL({X, Xs}, {int(), list(int())},
		not lists:member(X, lists:delete(X, Xs))).

prop_member_probability() ->
	?FORALL({X, Xs}, {int(), list(int())},
		collect(lists:member(X, Xs), true)).

test_delete_0() ->
	eqc:quickCheck(prop_delete_0()).

test_delete_0_more() ->
	eqc:quickCheck(eqc:numTests(2000, prop_delete_0())).

%prop_list_classification() ->
%	?FORALL({X, Xs}, {int(), list(int())},
%		collect(occurs(X, Xs), true)).

% qcTests:quickCheck(qcTests:numtests(2000, qcTests:prop_delete_1())).
prop_delete_1() ->
	?FORALL({X,Xs}, {int(), list(int())},
		?IMPLIES(lists:member(X, Xs),
			not lists:member(X, lists:delete(X, Xs)))).

prop_delete_2() ->
	fails(prop_delete_1()).

prop_delete_3() ->
	?FORALL({Xs,X,Ys}, {list(int()), int(), list(int())},
		?IMPLIES(not lists:member(X, Xs),
			Xs ++ Ys =:= lists:delete(X, Xs ++ [X] ++ Ys))).

dict() ->
	dict_2().

dict_0() ->
	?LAZY(
		oneof([dict:new(),
			?LET({K,V,D}, {key(), value(), dict_0()},
				dict:store(K,V,D))])
		).

no_duplicates(Lst) ->
	length(Lst) =:= length(lists:usort(Lst)).

prop_unique_keys() ->
	?FORALL(D, dict(),
		no_duplicates(dict:fetch_keys(D))).

dict_1() ->
	?LAZY(oneof([{call, dict, new, []},
		?LET(D, dict_1(),
			{call, dict, store,[key(), value(), D]})])
	).
key() ->
	% either key is an atom or an int
	oneof([atom(), int()]).

value() ->
	oneof([atom()]).

atom() ->
	elements([a,b,c,d, ken]).

prop_measure() ->
	?FORALL(D, dict(),
		collect(length(dict:fetch_keys(eval(D))), true)).

dict_2() ->
	?LAZY(
		frequency([{1, {call, dict, new, []}},
					{4, ?}]))