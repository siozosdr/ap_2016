-module(erl_test).
%-export([greet/2, add/2, hello/0, greet_add_add_two/1]).
-compile(export_all).
add(A,B) ->
    A+B.
hello() -> 
    io:format("Hello world!~n").
greet_add_add_two(X) ->
    hello(),
    add(X,2).

greet(male, Name) ->
    io:format("Hello, Mr. ~s", [Name]);
greet(female, Name) ->
    io:format("Hello, Mrs ~s", [Name]);
greet(_, Name) ->
    io:format("Hello, ~s", [Name]).
head([H | _]) -> H.
second([_, X|_]) -> X.

same(X,X) -> true;
same(_,_) -> false.
