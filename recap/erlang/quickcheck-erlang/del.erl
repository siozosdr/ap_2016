%%% @author Ken Friis Larsen <kflarsen@diku.dk>

-module(del).
-include_lib("eqc/include/eqc.hrl").
-compile(export_all).



del_test() ->
    [1, 3] = lists:delete(2, [1,2,3]),
    [1,3] = lists:delete(2, [2,1,3]),
    % delete element from list where it isn't
    [] = lists:delete(2, []),
    [4, 56] = lists:delete(2, [4,56]),

    % delete duplicate element
%    lists:delete(2, [2,1,2,3]) =:= [2,1,3],
%    lists:delete(2, [2,1,2,3]) =:= [1,3],
    [1,2,3] = lists:delete(2, [2,1,2,3]).



prop_delete_0() ->
  ?FORALL({X, Xs}, {int(), list(int())},
    not lists:member(X, lists:delete(X, Xs))).


% Checking this property for 100 random values and lists it might actually
% pass.
test_delete_0() ->
  quickcheck(prop_delete_0()).


% However, rerunning the property a few more times will reveal a problem:
test_delete_0_more() ->
  quickcheck(numtests(2000,prop_delete_0())).



prop_member_probability() ->
  ?FORALL({X, Xs}, {int(), list(int())},
    collect(lists:member(X, Xs), true)).

occurs(X, Xs) ->
    lists:foldl(fun(Y, Sum) ->
                        if X =:= Y -> Sum + 1; 
                           X =/= Y -> Sum
                        end end, 0, Xs).

prop_list_classification() ->
    ?FORALL({X, Xs}, {int(), list(int())},
            collect(occurs(X, Xs), true)).


prop_delete_1() ->
  ?FORALL({X, Xs}, {int(), list(int())},
  ?IMPLIES(lists:member(X, Xs),
    not lists:member(X, lists:delete(X, Xs)))).

prop_delete_2() ->
  fails(prop_delete_1()).


prop_delete_3() ->
  ?FORALL({Xs, X, Ys}, {list(int()), int(), list(int())},
  ?IMPLIES(not lists:member(X, Xs),
    (lists:delete(X, Xs ++ [X] ++ Ys) =:=
 	  Xs ++ Ys))).

