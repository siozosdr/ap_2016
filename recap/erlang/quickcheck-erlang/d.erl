-module(d).
-include_lib("eqc/include/eqc.hrl").
-compile(export_all).


no_duplicates(Lst) ->
    length(Lst) =:= length(lists:usort(Lst)).

prop_unique_keys() ->
    ?FORALL(D,dict(),
	    no_duplicates(dict:fetch_keys(eval(D)))).

dict() ->
    dict_2().

dict_0() ->
    ?LAZY(
       oneof([dict:new(),
	      ?LET({K,V,D},{key(), value(), dict_0()},
		   dict:store(K,V,D))])
      ).

dict_1() ->
    ?LAZY(
       oneof([{call,dict,new,[]},
	      ?LET(D, dict_1(),
               {call,dict,store,[key(),value(),D]})])
      ).

dict_2() ->
    ?LAZY(
       frequency([{1,{call,dict,new,[]}},
                  {4,?LET(D, dict_2(),
                          {call,dict,store,[key(),value(),D]})}])
      ).




key() ->
    oneof([atom(), int()]).

value() ->
    oneof([atom()]).

atom() ->
    elements([a,b,c,d, ken]).


prop_measure() ->
    ?FORALL(D,dict(),
	    collect(length(dict:fetch_keys(eval(D))),true)).

model(Dict) ->
    dict:to_list(Dict).

model_store(K,V,L) ->
    [{K,V}|L].

prop_store() ->
    ?FORALL({K,V,D},
	    {key(),value(),dict()},
       begin
           Dict = eval(D),
           model(dict:store(K,V,Dict))
                 =:=
           model_store(K,V,model(Dict))
       end).



%% %% Fixed version of the model, find the four differences
%% model(Dict) ->
%%     lists:sort(dict:to_list(Dict)).

%% model_store(K,V,L) ->
%%     L1 = proplists:delete(K,L),
%%     lists:sort([{K,V}|L1]).

%% prop_store() ->
%%     ?FORALL({K,V,D},
%% 	    {key(),value(),dict()},
%%        begin
%%            Dict = eval(D),
%%            equals(model(dict:store(K,V,Dict))
%%                  ,
%%            model_store(K,V,model(Dict)))
%%        end).
