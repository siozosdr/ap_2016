-module(trees).
-export([sampletree/0, contains/2]).

sampletree() -> 
    {node, 6, {node, 3, leaf, leaf}, 
              {node, 9, leaf, leaf}}.

contains(_, leaf) -> false;
contains(Key, {node, K, Left, Right}) ->
    if
        Key =:= K -> true;
        Key < K   -> contains(Key, Left);
        Key > K   -> contains(Key, Right)
    end.
