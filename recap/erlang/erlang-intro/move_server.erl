
-module(move_server).
-export([start/0,move/3]).


%%% API

start() -> spawn(fun () -> loop() end).

move(Pid, Dir, Pos) -> 
    request_reply(Pid, {move, Dir, Pos}).


%%% Internal implementation

request_reply(Pid, Request) ->
    Pid ! {self(), Request},
    receive
        {Pid, Response} -> Response
    end.

loop() ->
    receive
        {From, {move, north, {X, Y}}} -> 
            From ! {self(), {X, Y+1}};
        {From, {move, west, {X, Y}}} -> 
            From !  {self(), {X-1, Y}},
            loop();
        {From, Other} ->
            From ! {self(), {error, Other}},
            loop()
    end.


% ordinary function definition, just here for reference

old_move(north, {X, Y}) -> {X, Y+1};
old_move(west, {X, Y}) -> {X-1, Y}.

