-module(problems).
%-compile(export_all).
-export([rpn/1, rpn_test/0, main/1]).

rpn(L) when is_list(L) ->
	[Res] = lists:foldl(fun rpn/2, [], string:tokens(L, " ")),
	Res.

rpn("+", [N1,N2|S]) -> [N2+N1|S];
rpn("-", [N1,N2|S]) -> [N2-N1|S];
rpn("*", [N1,N2|S]) -> [N2*N1|S];
rpn("/", [N1,N2|S]) -> [N2/N1|S];
rpn("^", [N1,N2|S]) -> [math:pow(N2,N1)|S];
rpn("ln", [N|S])    -> [math:log(N)|S];
rpn("log10", [N|S]) -> [math:log10(N)|S];
rpn(X, Stack) -> [read(X)|Stack].

read(N) ->
	case string:to_float(N) of
		{error, no_float} -> list_to_integer(N);
		{F, _} -> F
	end.
rpn_test() ->
	5 = rpn("2 3 +"),
	87 = rpn("90 3 -"),
	-4 = rpn("10 4 3 + 2 * -"),
	-2.0 = rpn("10 4 3 + 2 * - 2 /"),
	ok = try
		rpn("90 34 12 33 55 66 + * - +")
	catch
		error:{badmatch,[_|_]} -> ok
	end,
	4037 = rpn("90 34 12 33 55 66 + * - + -"),
	8.0 = rpn("2 3 ^"),
	true = math:sqrt(2) == rpn("2 0.5 ^"),
	true = math:log(2.7) == rpn("2.7 ln"),
	true = math:log10(2.7) == rpn("2.7 log10"),
	%50 = rpn("10 10 10 20 sum"),
	%10.0 = rpn("10 10 10 20 sum 5 /"),
	%1000.0 = rpn("10 10 20 0.5 prod"),
	ok.

main([FileName]) ->
	{ok, Bin} = file:read_file(FileName),
	io:format("~p,~n",[optimal_path(parse_map(Bin))]),
	erlang:halt().

%% Picks the best of all paths, woo!
optimal_path(Map) ->
	{A, B} = lists:foldl(fun shortest_step/2, {{0,[]},{0,[]}}, Map),
	{_Dist, Path} = if hd(element(2,A)) =/= {x,0} -> A;
					   hd(element(2,B)) =/= {x,0} -> B
					end,
					lists:reverse(Path).
%% actual problem solving
%% change triples of the form {A,B,X}
%% where A,B,X are distances and a,b,x are possible paths
%% to the form {DistanceSum, PathList}.
shortest_step({A,B,X}, {{DistA,PathA}, {DistB,PathB}}) ->
	OptA1 = {DistA + A, [{a,A}|PathA]},
	OptA2 = {DistB + B + X, [{x,X}, {b,B}|PathB]},
	OptB1 = {DistB + B, [{b,B}|PathB]},
	OptB2 = {DistA + A + X, [{x,X}, {a,A}|PathA]},
	{erlang:min(OptA1, OptA2), erlang:min(OptB1, OptB2)}.
parse_map(Bin) when is_binary(Bin) ->
	parse_map(binary_to_list(Bin));
parse_map(Str) when is_list(Str) ->
	Values = [list_to_integer(X) || X <- string:tokens(Str, "\r\n\t ")],
	group_vals(Values, []).
group_vals([], Acc) ->
	lists:reverse(Acc);
group_vals([A,B,X|Rest], Acc) ->
	group_vals(Rest, [{A,B,X} |Acc]).