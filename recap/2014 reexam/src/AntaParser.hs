module AntaParser
       ( ParseError
       , parseString
       , parseFile
       )
       where

import Parser.Impl(
	  parseString,
	  parseFile
	)
import Text.Parsec.Error