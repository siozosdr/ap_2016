module AntaInterpreter
  ( runProg
  , Error (..)
  ) where

import Interpreter.Impl
  ( runProg
  , Error (..)
  )
