module Interpreter.Impl where

import AntaAST

-- You might need the following imports
import Control.Monad
import qualified Data.Map as Map
import Data.Map(Map)

-- ^ Any runtime error.  You may add more constructors to this type
-- (or remove the existing ones) if you want.  Just make sure it is
-- still an instance of 'Show' and 'Eq'.
data Error = Error String
             deriving (Show, Eq)

type Env = Map Name Value
type Primitive = Value -> Value -> AntaM Value
type PEnv = Map FunName Primitive
type Context = (Env, PEnv)

initialContext :: Context
initialContext = (Map.empty, initialPEnv)
  where initialPEnv =
          Map.fromList [ ("==", equalsF)
                       , ("in", undefined)
                       , ("not in", undefined)
                       , ("+", plusF)
                       , ("-", minusF)
                       , ("*", timesF)
                       , ("//", divisionF)
                       , ("%", moduloF)
                       ]

data Hole = Hole
newtype AntaM a = AntaM {runAntaM :: Context -> Either Error (a, Env)}

instance Functor AntaM where
  fmap f m = m >>= \a -> return (f a)

instance Applicative AntaM where
  pure = return
  -- (Just *) (Just 3) = (Just 3*)
  (<*>) = ap

instance Monad AntaM where
  -- return :: a -> Antam a 
  return x = AntaM $ \(env, _) -> Right (x, env)
  -- (>>=) :: AntaM a -> (a -> AntaM b) -> AntaM b  
  -- f :: a -> AntaM b
  -- m :: AntaM a
  -- runAntaM f :: Context -> Either Error (A, Env)
  m >>= f = AntaM $ \(env, pEnv) -> do
    case runAntaM m (env, pEnv) of
      (Right (a, newEnv)) -> runAntaM (f a) (newEnv, pEnv)
      (Left (Error msg)) -> fail msg
  -- fail :: String -> AntaM a
  fail s = AntaM $ \_ -> Left $ Error s

equalsF :: Primitive
equalsF (IntVal x) (IntVal y) = 
  case x==y of
    true -> return TrueVal
    false -> return FalseVal
equalsF TrueVal TrueVal = return TrueVal
equalsF FalseVal FalseVal = return TrueVal
equalsF TrueVal FalseVal = return FalseVal
equalsF FalseVal TrueVal = return FalseVal
equalsF (List v1) (List v2) = 
  case v1 == v2 of
    true -> return TrueVal
    false -> return FalseVal
equalsF _ _= fail ("Need parameters of same type.")

plusF :: Primitive
plusF (IntVal x) (IntVal y) = return $ IntVal (x+y)
plusF _ _ = fail ("Both parameters must be operators.")

minusF :: Primitive
minusF (IntVal x) (IntVal y) = return $ IntVal (x-y)
minusF _ _ = fail ("Both parameters must be integers.")

timesF :: Primitive
timesF (IntVal x) (IntVal y) = return $ IntVal (x*y)
timesF _ _ = fail ("Both parameters must be integers.")

divisionF :: Primitive
divisionF (IntVal x) (IntVal 0) = fail ("Division by zero, not allowed.")
divisionF (IntVal x) (IntVal y) = return $ IntVal (x `quot` y)
divisionF _ _ = fail ("Both arguments must be integers.")

moduloF :: Primitive
moduloF (IntVal x) (IntVal 0) = fail ("Division by zero, not allowed.")
moduloF (IntVal x) (IntVal y) = return $ IntVal (x `mod` y)
moduloF _ _ = fail ("Both arguments must be integers.")

getEnv :: AntaM Env
getEnv = AntaM $ \(env, _) -> Right (env, env)

setEnv :: Env -> AntaM ()
setEnv e = AntaM $ \_ -> Right ((), e)

modify :: (Env -> Env) -> AntaM ()
modify f = do
  e <- getEnv
  setEnv (f e)

updateEnv :: Name -> Value -> AntaM ()
updateEnv name val = do
  oldEnv <- getEnv
  let newEnv = Map.insert name val oldEnv
  setEnv newEnv

getVar :: Name -> AntaM Value
getVar name = AntaM $ \(env, _) ->
  case Map.lookup name env of
    Nothing -> fail ("No variable with name: " ++ name)
    (Just v) -> Right (v, env)

getFunction :: FunName -> AntaM Primitive
getFunction name = AntaM $ \(env, pEnv) ->
  case Map.lookup name pEnv of
    Nothing -> fail ("No function with name: " ++ name)
    (Just f) -> Right (f, env)

evalExpr :: Expr -> AntaM Value
evalExpr (IntConst i) = return $ IntVal i
evalExpr TrueConst = return TrueVal
evalExpr FalseConst = return FalseVal
evalExpr (Name name) = getVar name
evalExpr (Range a3) = 
  case a3 of
    A1 e1 -> do
      -- step is 1, start is 0
      (IntVal end) <- evalExpr e1
      let res = [(IntVal s) | s <- [0.. end],
                     s < end]
      return $ List res


    A2 e1 e2 -> do
      (IntVal start) <- evalExpr e1
      (IntVal end) <- evalExpr e2
      -- step is 1
      let res = [(IntVal s) | s <- [start+0,start+1..end],
                              s < end]
      return $ List res
    A3 e1 e2 e3 -> do
      (IntVal start) <- evalExpr e1
      (IntVal end) <- evalExpr e2
      (IntVal step) <- evalExpr e3
      if step == 0 
        then fail ("Step cannot be 0")
      else if step > 0 
        then do
          let res = [(IntVal s)|  i <- [0,1..end],
                                  s <- [start + i* step],
                                  s < end]
          return $ List res
      else do -- step <0
        let res = [(IntVal s)|  i <- [0,1..(-end)],
                                s <- [start + i* step],
                                s > end]
        return $ List res
evalExpr (Plus e1 e2) = do
  x <- evalExpr e1
  y <- evalExpr e2
  plusF x y
evalExpr (Minus e1 e2) = do
  x <- evalExpr e1
  y <- evalExpr e2
  minusF x y
evalExpr (Mult e1 e2) = do
  x <- evalExpr e1
  y <- evalExpr e2
  timesF x y
evalExpr (Div e1 e2) = do
  x <- evalExpr e1
  y <- evalExpr e2
  divisionF x y
evalExpr (Modulus e1 e2) = do
  x <- evalExpr e1
  y <- evalExpr e2
  moduloF x y
evalExpr (Equal e1 e2) = do
  x <- evalExpr e1
  y <- evalExpr e2
  equalsF x y
evalExpr (NotIn e1 e2) = do -- not working
  e <- evalExpr e1
  (List elist) <- evalExpr e2
  case e `elem` elist of
    true -> return TrueVal
    false -> return FalseVal
evalExpr (In e1 e2) = do -- not working
  e <- evalExpr e1
  (List elist) <- evalExpr e2
  case e `elem` elist of
    true -> return TrueVal
    false -> return FalseVal
evalExpr (ListComp (Simple es)) = do
  v <- mapM evalExpr es
  return $ List v
evalExpr (ListComp (ListFor e1 lf)) = evalListFor e1 lf 

evalListFor :: Expr -> ListFor -> AntaM Value
evalListFor e1 (n,e2, Nothing) = do
  evale2 <- evalExpr e2
  case evale2 of
    List l -> do
      result <- foldr(\h acc -> do
        updateEnv n h
        result <- evalExpr e1
        c <- acc
        let result' = result:c
        return $ result') (return $ []) l
      return $ List result

    otherwise -> return evale2

evalListFor e1 (n, e2, (Just li)) = do -- not working
  evale2 <- evalExpr e2
  case evale2 of
    List l -> do
      result <- foldr(\h acc -> do
        updateEnv n h
        eval <- evalListIter (Name n) li
        case  eval of
          TrueVal -> do
            result <- evalExpr e1
            c <- acc
            let result' = result:c
            return $ result'
          FalseVal -> do
            c <- acc
            return c
          other -> fail ("not implemented")
            ) (return $ []) l
      return $ List result

    otherwise -> return evale2

evalListIter :: Expr -> ListIter -> AntaM Value
evalListIter n (ListForIter lf) = evalListFor n lf
evalListIter e1 (ListIf e Nothing) = evalExpr e
evalListIter e1 (ListIf e (Just li)) = undefined

decl :: Decl -> AntaM ()
decl (name, expr) = do
  e <- evalExpr expr
  updateEnv name e

program :: Program -> AntaM ()
program decls = mapM_ decl decls

runProg :: Program -> Either Error Env
runProg prog = case runAntaM (program prog) initialContext of
  Left (Error msg) -> Left (Error msg)
  Right (_, env) -> Right env 
