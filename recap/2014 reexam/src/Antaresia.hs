-- | This module defines a simple command line interface for the Antaresia
-- interpreter.  If your solution is correct, this module should just
-- work.
module Main
       (main)
where
import AntaAST
import AntaParser
import AntaInterpreter

import Control.Monad(forM_)
import Data.List(intercalate)
import qualified Data.Map as Map
import System.Environment(getArgs)


-- | nice display of JavaScript values
nice :: Value -> String
nice (IntVal v) = show v
nice TrueVal = "true"
nice FalseVal = "false"

run :: Program -> IO ()
run p =
  case runProg p of
    Left e -> error $ show e
    Right res ->
      forM_ (Map.toList res) (\(n,v) -> putStrLn $ n ++ " = " ++ nice v)


main :: IO ()
main = do args <- getArgs
          case args of
            ["-i", file] -> do
              s <- readFile file
              run $ read s
            ["-p", file] -> do
              p <- parseFile file
              putStrLn $ show p
            [file] -> do
              pp <- parseFile file
              case pp of
                Left e -> error $ show e
                Right p -> run p
            _ ->
              error "Invalid arguments!"
