module Parser.Impl where

import AntaAST

import Control.Monad (void)
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Error
import Text.Parsec.String
import Text.Parsec.Combinator

keywords :: [String]
keywords = ["True", "False", "range", "for", "if", "in", "not"]

parseFile :: FilePath -> IO (Either ParseError Program)
parseFile path = parseString <$> readFile path

parseString :: String -> Either ParseError Program
parseString s = case parseme program s of
    Left err -> Left err
    Right result -> Right result

program :: Parser Program
program = decls []
decls :: Decls -> Parser Decls
decls ds = (do
    d <- decl
    decls (ds++[d]))
    <|> return ds
decl :: Parser Decl
decl = do
    n <- parseName
    void $ cToken '='
    e <- expr
    return (n, e)

exprs :: Parser Exprs
exprs = (do
    e <- expr
    commaExprs [e])
    <|> return []

commaExprs :: [Expr] -> Parser [Expr]
commaExprs es = (do
    void $ cToken ','
    e <- expr
    commaExprs (es ++ [e])) 
    <|> return es
expr :: Parser Expr
expr = do 
    e1 <- expr_1
    expr' e1

expr' :: Expr -> Parser Expr
expr' e1 = (do
    void $ sToken "in"
    e2 <- expr
    return $ In e1 e2
    )<|> (do
    void $ sToken "not"
    void $ sToken "in"
    e2 <- expr
    return $ NotIn e1 e2
    )<|> return e1

expr_1 :: Parser Expr
expr_1 = do
    e <- expr_2
    expr_1' e

expr_1' :: Expr -> Parser Expr
expr_1' e1 = (do
    void $ sToken "=="
    e2 <- expr_1
    return $ Equal e1 e2
    ) <|> 
    return e1
{-
--old versions with try
--the association is correct
expr :: Parser Expr
expr = try (do
    e1 <- expr_1
    void $ sToken "not"
    void $ sToken "in"
    e2 <- expr
    return $ NotIn e1 e2
    )<|> try(do
        e1 <- expr_1
        void $ sToken "in"
        e2 <- expr
        return $ In e1 e2
    )<|> expr_1

expr_1 :: Parser Expr
expr_1 = try (do
    e1 <- expr_2
    void $ sToken "=="
    e2 <- expr_1
    return $ Equal e1 e2
    )<|> expr_2
-}
{-
-- the assossiation is wrong here
expr :: Parser Expr
expr = do
    e1 <- expr_1
    expr' e1

expr' :: Expr -> Parser Expr
expr' e1 = try (do
    void $ sToken "not"
    void $ sToken "in" 
    e2 <- expr_1
    let e = NotIn e1 e2
    expr' e
    ) <|>
    try(do
        void $ sToken "in" 
        e2 <- expr_1
        let e = In e1 e2
        expr' e)
    <|> return e1

expr_1 :: Parser Expr
expr_1 = do
    e1 <- expr_2
    expr_1' e1

expr_1' :: Expr -> Parser Expr
expr_1' e1 = do
    void $ sToken "=="
    e2 <- expr_2
    let e = Equal e1 e2
    expr_1' e
    <|> return e1
-}
expr_2 :: Parser Expr
expr_2 = do
    e1 <- expr_3
    expr_2' e1

expr_2' :: Expr -> Parser Expr
expr_2' e1 = do
    op <- (cToken '+' <|> cToken '-')
    e2 <- expr_3
    case op of
        "+" -> expr_2' (Plus e1 e2)
        "-" -> expr_2' (Minus e1 e2)
        _ -> fail "not an operator"
    <|> return e1 

expr_3 :: Parser Expr
expr_3 = do
    e1 <- expr_4
    expr_3' e1

expr_3' :: Expr -> Parser Expr
expr_3' e1 = do
    op <- (cToken '*' <|> cToken '%' <|> sToken "//")
    e2 <- expr_4
    case op of
        "*" -> expr_3' (Mult e1 e2)
        "%" -> expr_3' (Modulus e1 e2)
        "//" -> expr_3' (Div e1 e2)
        _ -> fail "not an operator"
    <|> return e1

expr_4 :: Parser Expr
expr_4 =  (do
    void $ sToken "range"
    a <- between (cToken '(') (cToken ')') args3 
    return $ Range a) 
    <|> ( do
        l <- between (cToken '[') (cToken ']') listComp  
        return $ ListComp l)
    <|>  (do
         between (cToken '(') (cToken ')') expr
        )
    <|> (do
    i <- parseInteger
    return $ IntConst i
    ) <|> 
    (do
        void $ sToken "True"
        return TrueConst
    ) <|>
    (do
        void $ sToken "False"
        return FalseConst
    )
    <|>(do
        n <- parseName
        return $ Name n
    )
    
args3 :: Parser Args3
args3 = (do
    e <- expr
    args3' e)

args3' :: Expr -> Parser Args3
args3' e1= (do
    void $ cToken ','
    e2 <- expr
    args3'' e1 e2
    ) <|> return (A1 e1)

args3'' :: Expr -> Expr -> Parser Args3
args3'' e1 e2 = (do
    void $ cToken ','
    e3 <- expr
    return (A3 e1 e2 e3)
    ) <|> return (A2 e1 e2)    

listComp :: Parser ListComp
listComp = (do
        e <- expr
        lf <- listFor
        return $ ListFor e lf
        )<|> (do
        es <- exprs
        return $ Simple es)

listFor :: Parser ListFor
listFor = do
    void $ sToken "for"
    n <- parseName
    void $ sToken "in"
    e <- expr
    li <- listIter
    case li of
        Nothing -> return (n,e,Nothing)
        (Just res) -> return (n,e, Just res)

listIter :: Parser (Maybe ListIter)
listIter = (do
    void $ sToken "if"
    e <- expr
    li <- listIter
    return $ (Just $ ListIf e li)) 
    <|> (do
    l <- listFor
    return $ (Just $ ListForIter l))
    <|> return Nothing

parseName :: Parser String
parseName = do
    c <- letter
    rest <- many (alphaNum <|> char '_')
    let name = c:rest
    if name `elem` keywords then
        fail "Reserved variable name"
        else return name

parseInteger :: Parser Integer
parseInteger = do
    sign <- option "" (sToken "-")
    num <- many1 digit
    if length num > 8
    then
        fail "too big number"
    else
        case sign of
            "" -> return $ read num
            _ -> return $ read (sign++num)

sToken :: String -> Parser String
sToken s = try(spaces >> string s <* spaces) 

cToken :: Char -> Parser String
cToken c = try(spaces >> string [c] <* spaces)

parseme :: Parser a -> String -> Either ParseError a
parseme parser = parse parser ""
