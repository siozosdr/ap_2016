module MatchParser where
import Control.Monad (void, ap)
import Control.Applicative(Alternative((<|>), empty, many, some) )


newtype MatchParser a = MatchParser {runParser :: String -> Maybe (a, String)}

instance Functor MatchParser where
    fmap f m = m >>= \a -> return (f a)

instance Applicative MatchParser where
    pure = return
    (<*>) = ap

instance Monad MatchParser where
    return a = MatchParser $ \s -> Just (a, s)
    m >>= f = MatchParser $ \s -> do
        (a,s1) <- runParser m s
        runParser (f a) s1
    {-
    m >>= f = MatchParser $ \s ->
        case runParser m s of
            Just a -> runParser (f a) s 
            Nothing -> Nothing
    -}
    fail _ = MatchParser $ \_ -> Nothing

reject :: MatchParser a
reject = MatchParser $ \_ -> Nothing
get :: MatchParser String
get = MatchParser $ \s -> Just (s, s)

getC :: MatchParser Char
getC = MatchParser $ get'
    where 
        get' "" = Nothing
        get' (c:cs) = Just (c,cs)
char :: Char -> MatchParser Char
char c = do
    s <- getC
    if s == c then return c else reject

string :: String -> MatchParser String
string "" = return ""
string (c:cs) = do
    void $ char c
    void $ string cs
    return $ (c:cs)
eof :: MatchParser ()
eof = MatchParser $ eof'
    where
        eof' "" = return ((), "")
        eof' _ = Nothing

chars :: [Char] -> MatchParser String
chars cs = do
    c <- getC
    if c `elem` cs
        then return cs
        else reject

wildcard :: MatchParser ()
wildcard = void $ getC



parse :: MatchParser a -> String -> Maybe (a, String)
parse = runParser

instance Alternative MatchParser where
    -- empty :: MatchParser a
    empty = reject
    -- (<|>) :: MatchParser a -> MatchParser a -> MatchParser a
    p <|> q = MatchParser $ \s ->
        parse p s <|> parse q s