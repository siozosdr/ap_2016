import qualified Data.Map as M
type PhoneNumber = String
type Name = String
type PhoneBook = [(String, String)]

inPhoneBook :: Name -> PhoneNumber -> PhoneBook -> Bool
inPhoneBook name pNumber pBook = (name, pNumber) `elem` pBook

type IntMap v = M.Map Int v

data Either a b = Left a | Right b deriving (Eq, Ord, Read, Show)
