{-
    fmap :: (a -> b) -> f a -> f b
    fmap takes a function and a functor and applies
    the function inside the functor

    <*> :: f (a -> b) -> f a -> f b
    <*> takes a functor that has function in it and 
    another functor and "extracts" that function from
    the first functor and then maps it over the 
    second one

    instance Applicative Maybe where
        pure = Just
        Nothing <*> _ = Nothing
        (Just f) <*> something = fmap f something
-}  

myAction :: IO String
myAction = do
    a <- getLine
    b <- getLine
    return $ a ++ b

myAction2 :: IO String
myAction2 = (++) <$> getLine <*> getLine

{-
    sequenceA is used when we have a list of functions and we
    want to feed the same input to all of them and then view
    the list of results.
-}
sequenceA' :: (Applicative f) => [f a] -> f [a]
sequenceA' [] = pure []
sequenceA' (x:xs) = (:) <$> x <*> sequenceA' xs

newtype Pair' b a = Pair' {getPair :: (a,b)}

{-
    We can pattern match on types defined with newtype
-}
instance Functor (Pair' c) where
    fmap f (Pair' (x,y)) = Pair' (f x, y)
