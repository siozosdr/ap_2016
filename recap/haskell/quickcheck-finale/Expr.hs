{-
Example code produced for Advanced Programming lecture.

Main purpose is to show how to generate recursive types.
-}
import Test.QuickCheck
import Control.Monad (liftM, liftM2, ap)
import Control.Applicative

data Expr = Con Int
          | Add Expr Expr
     deriving (Eq, Show, Read, Ord)

eval :: Expr -> Int
eval (Con n) = n
eval (Add x y) = eval x + eval y


prop_com_add x y = eval (Add x y) == eval (Add y x)

-- Take 1: not limiting the recursion
expr =  oneof [ liftM Con arbitrary
              , Add <$> expr <*> expr
              ]


-- instance Arbitrary Expr where
--   arbitrary = expr


-- -- Take 2: using sized generators

--instance Arbitrary Expr where
--   arbitrary = expr2

expr2 = sized exprN
exprN 0 = liftM Con arbitrary
exprN n = oneof [ liftM Con arbitrary
                , liftM2 Add subexpr subexpr
                ]
  where subexpr = exprN (n `div` 2)


-- Take 3: Arbitrary instance with shrinking
instance Arbitrary Expr where
  arbitrary = expr2

  shrink (Add e1 e2) = [e1, e2]
  shrink _ = []

