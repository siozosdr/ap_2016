import Control.Monad

newtype Trace a = T (a, String) deriving (Show)

instance Functor Trace where
    fmap f m = m >>= \a -> return (f a)
    {-
    fmap f (T (a, str)) = T $ (f a, str)
    -}
instance Applicative Trace where
    pure = return
    (<*>) = ap
    {-
    (T (a, str)) <*> something = fmap a something
    -}
instance Monad Trace where
    -- (>>=) :: Trace a -> (a -> Trace b) -> Trace b
    (T (p,str)) >>= f = let (T (a, str')) = f p
                        in T (a, str++str')



    -- return :: a -> Trace a
    return x = T (x, "")

traceable :: String -> (t -> a) -> t -> Trace a
traceable name f = \x -> T(f x, name ++" called.")

{-
    let begin = traceable "beginning" (id)
    let beginVal = begin 1
    beginVal >>= (\x -> T (x*2, "Multiplied by 2"))
-}

doStuff = do
    let begin = traceable "beginning" (id)
    let beginVal = begin 1
    beginVal >>= (\x -> T(x*2, "multiplied by 2"))

--sqrtC, cbrtC :: Complex Float -> [Complex Float]

--sixthroot x = sqrt (cbrt x)
{-
sixthrootC :: Complex Float -> [Complex Float]
sixthrootC x = do
   cr <- cbrtC x
   sr <- sqrtC cr
   return sr
-}
newtype Multivalued a = MV [a]

data Complex = Complex Float
instance Functor Multivalued where
    fmap f m = m >>= \a -> return (f a)
instance Applicative Multivalued where
    pure = return 
    (<*>) = ap
instance Monad Multivalued where
    -- (>>=) :: Multivalued a -> (a -> Multivalued b) -> Multivalued b
    -- f :: a -> Multivalued b
    (MV p) >>= f = let nr = map f p
                       uw = map (\(MV x) -> x) nr
                       in MV $ concat $ uw

    -- return :: a -> Multivalued a
    return x = MV [x]

--sqrtC, cbrtC, sixthrootC :: Complex Float -> Multivalued (Complex Float)


