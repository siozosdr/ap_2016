{-
    If you have a value with a context, m a, how do you 
    apply to it a function that takes a normal a and 
    returns a value with a context? That is, how do you
    apply a function of type a -> m b to a value of 
    type m a? So essentially, we will want this function:
        (>>=) :: (Monad m) => m a -> (a -> m b) -> m b

    If we have a fancy value and a function that takes a 
    normal value but returns a fancy value, how do we feed 
    that fancy value into the function?   
-}

applyMaybe :: Maybe a -> (a -> Maybe b) -> Maybe b
applyMaybe Nothing _ = Nothing
applyMaybe (Just x) f = f x 

type Birds = Int
type Pole = (Birds, Birds)

landLeft :: Birds -> Pole -> Pole
landLeft x (left, right) = (left+x, right)

landRight :: Birds -> Pole -> Pole
landRight x (left, right) = (left,  right+x)

(-:) :: a -> (a -> b) -> b
x -: f = f x

landLeft' :: Birds -> Pole -> Maybe Pole
landLeft' x (left, right)
    | abs ((left + x) - right) < 4 = Just (left+x , right)
    | otherwise = Nothing

landRight' :: Birds -> Pole -> Maybe Pole
landRight' x (left, right)
    | abs (left - (right + x)) < 4 = Just (left,right+x)
    | otherwise = Nothing 

banana :: Pole -> Maybe Pole
banana _ = Nothing

{-
     do expressions are just different syntax for 
     chaining monadic values. 
-}
foo :: Maybe String  
foo = do  
    x <- Just 3
    y <- Just "!"  
    Just (show x ++ y)  

{-
    In a do expression, every line is a monadic value.
    To inspect its result, we use <-
-}
foo' :: Maybe Bool
foo' = do
    x <- Just 9
    Just (x>8)

routine :: Maybe Pole
routine = do
    start <- return (0,0)
    first <- landRight' 2 start
    second <- landLeft' 3 first
    landLeft' 1 second

listOfTuples :: [(Int, Char)]
listOfTuples = do
    n <- [1,2]
    ch <- ['a', 'b']
    return (n, ch)

class Monad m => MonadPlus m where
    mzero :: m a

    mplus :: m a -> m a -> m a

instance MonadPlus [] where
    mzero = []
    mplus = (++)

guard :: (MonadPlus m) => Bool -> m ()
guard True = return ()
guard False = mzero

sevensOnly :: [Int]
sevensOnly = do
    x <- [1..50]
    guard ('7' `elem` show x)
    return x

type KnightPos = (Int, Int)

moveKnight :: KnightPos -> [KnightPos]  
moveKnight (c,r) = do  
    (c',r') <- [(c+2,r-1),(c+2,r+1),(c-2,r-1),(c-2,r+1)  
               ,(c+1,r-2),(c+1,r+2),(c-1,r-2),(c-1,r+2)  
               ]  
    guard (c' `elem` [1..8] && r' `elem` [1..8])  
    return (c',r')  

moveKnight' :: KnightPos -> [KnightPos]  
moveKnight' (c,r) = filter onBoard  
    [(c+2,r-1),(c+2,r+1),(c-2,r-1),(c-2,r+1)  
    ,(c+1,r-2),(c+1,r+2),(c-1,r-2),(c-1,r+2)  
    ]  
    where onBoard (c,r) = c `elem` [1..8] && r `elem` [1..8] 

in3 :: KnightPos -> [KnightPos]
in3 start = do
    first <- moveKnight start
    second <- moveKnight first
    moveKnight second

canReachIn3 :: KnightPos -> KnightPos -> Bool
canReachIn3 start end = end `elem` in3 start

(<=<) :: (Monad m) => (b -> m c) -> (a -> m b) -> (a -> m c)  
f <=< g = (\x -> g x >>= f)  