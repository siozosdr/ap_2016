import Data.List

-- Solve reverse polish notation
solveRPN :: String -> Float
solveRPN = head . foldl foldingFunction [] . words
    where
        foldingFunction (x:y:ys) "*" = (x*y):ys
        foldingFunction (x:y:ys) "+" = (x+y):ys
        foldingFunction (x:y:ys) "-" = (y-x):ys
        foldingFunction (x:y:ys) "/" = (y/x):ys
        foldingFunction (x:y:ys) "^" = (y ** x):ys
        foldingFunction (x:xs) "ln" = log x:xs
        foldingFunction xs "sum" = [sum xs]
        foldingFunction xs numberString = read numberString:xs

-- Shortest path algorithm for road system
data Node = Node Road Road | EndNode Road
data Road = Road Int Node

data Section = Section {getA :: Int, getB :: Int, getC :: Int} deriving (Show)
type RoadSystem = [Section]

data Label = A | B | C deriving (Show)
type Path = [(Label, Int)]

-- roadsystem
heathrowToLondon :: RoadSystem
heathrowToLondon = [Section 50 10 30, Section 5 90 20, Section 40 2 25, Section 10 8 0]

-- calculate optimal path given a roadsystem
optimalPath :: RoadSystem -> Path
optimalPath roadSystem =
    let (bestAPath, bestBPath) = foldl roadStep ([],[]) roadSystem
    in if sum (map snd bestAPath) <= sum (map snd bestBPath)
        then reverse bestAPath
        else reverse bestBPath 

-- helper function of optimalPath to calculate shortest path
-- in each section of the roadsystem
roadStep :: (Path, Path) -> Section -> (Path, Path)
roadStep (pathA, pathB) (Section a b c) = 
    let priceA = sum $ map snd pathA
        priceB = sum $ map snd pathB
        forwardPricetoA = priceA + a
        forwardPricetoB = priceB + b
        crossPricetoA = priceB + b + c
        crossPricetoB = priceA + a + c
        newPathToA = if forwardPricetoA <= crossPricetoA
                        then (A,a):pathA
                        else (C,c):(B,b):pathB
        newPathtoB = if forwardPricetoB <= crossPricetoB
                        then (B,b):pathB
                        else (C,c):(A,a):pathA
    in (newPathToA, newPathtoB)

-- take groups of n from a list, used to make sections
-- for the roadsystem
groupsOf :: Int -> [a] -> [[a]]
groupsOf 0 _ = undefined
groupsOf _ [] = []
groupsOf n xs = take n xs : groupsOf n (drop n xs)

main = do
    contents <- getContents
    let threes = groupsOf 3 (map read $ lines contents)
        roadSystem = map (\[a,b,c] -> Section a b c) threes
        path = optimalPath roadSystem
        pathString = concat $ map (show . fst) path
        pathPrice = sum $ map snd path
    putStrLn $ "The best path to take is: " ++ pathString
    putStrLn $ "The price is: " ++ show pathPrice