doubleSmallNumber ::(Num a, Ord a) => a -> a
doubleSmallNumber x = if x > 100  
                        then x  
                        else x*2
lucky :: (Integral a) => a -> String
lucky 7 = "LUCKY NUMBER SEVEN!"
lucky _ = "Sorry, you're out of luck, pal!"

sayMe :: (Integral a) => a -> String  
sayMe 1 = "One!"  
sayMe 2 = "Two!"  
sayMe 3 = "Three!"  
sayMe 4 = "Four!"  
sayMe 5 = "Five!"  
sayMe _ = "Not between 1 and 5"  

factorial :: (Integral a) => a -> a
factorial 0  = 1
factorial n = n * factorial (n - 1)

add :: (Integral a) => a -> a -> a
add a b = a + b

type Pos = (Int, Int)
data Direction = North | South | West | East

move :: Direction -> Pos -> Pos
move North (x, y) = (x, y+1)
move South (x, y) = (x , y-1)
move West (x, y) = (x-1, y)
move East (x, y) = (x+1, y)

moves :: [Direction] -> Pos -> Pos 
moves [] p = p
moves (d:ds) p = moves ds (move d p)

data Nat = Zero | Succ Nat
    deriving (Eq, Show, Read, Ord)
addition :: Nat -> Nat -> Nat
addition a Zero = a
addition Zero a = a
addition (Succ a) b = Succ $ addition a b  

multiplication :: Nat -> Nat -> Nat
multiplication _ Zero = Zero
multiplication Zero _ = Zero
multiplication (Succ Zero) a = a
multiplication a (Succ Zero) = a
multiplication (Succ a) b = addition b (multiplication a b)

nat2int :: Num a => Nat -> a
nat2int Zero = 0
nat2int (Succ Zero) = 1
nat2int (Succ x) = 1 + (nat2int x)

int2nat :: (Eq a, Num a) => a -> Nat
int2nat 0 = Zero
int2nat 1 = Succ Zero
int2nat x = Succ $ int2nat (x-1)

data Figure = Point
            | Disc {radius :: Double}
            | Rectangle {width, height :: Double}

-- Every element in the left node has value <= Node Int
data Tree = Leaf | Node Int Tree Tree
    deriving (Eq, Show, Read, Ord)

insert :: Int -> Tree -> Tree
insert x Leaf = Node x Leaf Leaf
insert x (Node y t1 t2) 
    | x <= y = Node y (insert x t1) t2
    | otherwise = Node y t1 (insert x t2)
    
bmi :: (RealFloat a) => a -> a -> a
bmi w h 
    | final <= 18.5 = final
    | final <= 25.0 = final
    | final <= 30.0 = final
    | otherwise = final
    where final = w / h ^ 2

data Expr = Con Double
          | Add Expr Expr
          | Sub Expr Expr
          | Mul Expr Expr
          | Div Expr Expr
     deriving (Eq, Show, Read, Ord) 

value :: Expr -> Double
value (Con n) = n
value (Add x y) = value x + value y 
value (Sub x y) = value x - value y
value (Mul x y) = value x * value y
value (Div x y) = value x / value y

-- Morse code function
-- How do I define the alphabet as a data structure

-- Tic Tac Toe


-----------------------------------
take' :: (Num a, Ord a) => a -> [t] -> [t]
take' _ [] = []
take' n _ | n <= 0 = []
take' n (h:t) = h : take' (n-1) t 

reverse' :: [t] -> [t]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

zip' :: [t] -> [t1] -> [(t, t1)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x,y) : zip' xs ys

elem' :: Eq a => a -> [a] -> Bool
elem' _ [] = False
elem' x (h:t)
    | x == h = True
    | otherwise = elem' x t

quicksort' :: (Ord a) => [a] -> [a]
quicksort' [] = []
quicksort' (x:xs) = 
    let smallerSorted = quicksort'[a | a <- xs, a <= x]
        biggerSorted = quicksort'[a | a <-xs, a > x]
    in smallerSorted ++ [x] ++ biggerSorted

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ _ [] = []
zipWith' _ [] _ = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys    

flip' :: (a -> b -> c) -> b -> a -> c
flip' f = g
    where g x y = f y x

largestDivisible :: (Integral a) => a  
largestDivisible = head (filter p [100000,99999..])  
    where p x = x `mod` 3829 == 0  

chain :: (Integral a) => a -> [a]  
chain 1 = [1]  
chain n  
    | even n =  n:chain (n `div` 2)  
    | odd n  =  n:chain (n*3 + 1)

numLongChains :: Int
numLongChains = length(filter isLong (map chain [1..100]))
    where isLong x = length x > 15

numLongChains' :: Int
numLongChains' = length(filter (\xs -> length xs > 15) (map chain [1..100]))

numShortChains :: Int
numShortChains = length(filter isShort (map chain [1..100]))
    where isShort x = length x < 10

sum' :: (Num a) => [a] -> a
sum' xs = foldl (\acc x -> acc + x) 0 xs

sum'' :: (Num a) => [a] -> a
sum'' = foldl (+) 0

length' :: (Num a) => [a] -> a
length' xs = foldl (\acc _ -> acc + 1) 0 xs

average' :: (Num a, Fractional a) => [a] -> a
average' xs = (sum'' xs) / (length' xs)

elem'' :: (Eq a) => a -> [a] -> Bool
elem'' x ys = foldl (\acc y -> if x == y then True else acc) False ys

map' :: (a -> b) -> [a] -> [b]  
map' f xs = foldr (\x acc -> f x : acc) [] xs

-- map'' has the same result as map' in a reversed list
map'' :: (a -> b) -> [a] -> [b]
map'' f xs = foldl (\acc x -> f x : acc) [] xs

maximum' :: (Ord a) => [a] -> a
maximum' = foldl1 (\acc x -> if x > acc then x else acc)

reverse'' :: [a] -> [a]
reverse'' = foldl (\acc x -> x:acc) []

-- reverse''' is more expensive than reverse''
-- because of (++) operation
reverse''':: [a] -> [a]
reverse''' = foldr (\x acc -> acc ++ [x]) []

product' :: (Num a) => [a] -> a
product' = foldl1 (*)

-- Why does it not work with foldr1
filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldr (\x acc -> if p x then x:acc else acc) []

-- Why does it not work with foldr?
head'' :: [a] -> a
head'' = foldr1 (\x _ -> x)

-- Why does it not work with foldl?
last'' :: [a] -> a
last'' = foldl1 (\_ x -> x)

fn :: (Num a, Ord a, Floating a) => a -> a
fn = negate . tan . cos . max 50