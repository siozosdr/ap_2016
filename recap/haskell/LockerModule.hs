import qualified Data.Map as Map
data LockerState = Taken | Free deriving (Show, Eq)
type Code = String
type LockerMap = Map.Map Int (LockerState, Code)

lockerLookup :: Int -> LockerMap -> Either String Code 
lockerLookup lockerNumber m = 
    case Map.lookup lockerNumber m of
        Nothing -> Left $ "Locker number " ++ show lockerNumber ++ " doesn't exist."
        Just (state, code) -> if state Prelude./= Taken 
                              then Right code
                              else Left $ "Locker number " ++ show lockerNumber ++ " already taken."
takeLocker :: (Ord a, Show a) => a -> Map.Map a (LockerState, b) -> Either String b
takeLocker lockerNumber m =
    case Map.lookup lockerNumber m of
        Nothing -> Left $ "Locker number " ++ show lockerNumber ++ " does not exist."
        Just (Taken, _) -> Left $ "Locker already taken"
        Just (Free, code) -> Right code
lockers :: LockerMap  
lockers = Map.fromList   
    [(100,(Taken,"ZD39I"))  
    ,(101,(Free,"JAH3I"))  
    ,(103,(Free,"IQSA9"))  
    ,(105,(Free,"QOTSA"))  
    ,(109,(Taken,"893JJ"))  
    ,(110,(Taken,"99292"))  
    ]             

-- class: define new typeclasses                
-- Because == was defined in terms of /= and vice versa in 
-- the class declaration, we only had to overwrite one of 
-- them in the instance declaration.
class Eq' a where
    (==) :: a -> a -> Bool
    (/=) :: a -> a -> Bool
    x ==y = not (x Main./=y)
    x /=y = not (x Main.==y)

data TrafficLight = Red | Yellow | Green

-- instance: make our types instances of typeclasses (class)
instance Eq' TrafficLight where
    Red == Red = True
    Yellow == Yellow = True
    Green == Green = True
    _ == _ = False

instance Show TrafficLight where
    show Red = "Red light"
    show Yellow = "Yellow light"
    show Green = "Green light"

-- class constraints in class declarations are used for making
-- a typeclass a subclass of another typeclass and class 
-- constraints in instance declarations are used to express 
-- requirements about the contents of some type.
-- e.g. class (Eq a) => Num a where ...

-- e.g.
instance (Eq' m) => Eq' (Maybe m) where
    Just x == Just y = x Main.== y
    Nothing == Nothing = True
    _ == _ = False

