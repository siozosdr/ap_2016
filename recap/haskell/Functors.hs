import Data.Char
import Data.List
main :: IO()
main = do line <- getLine
          let line' = reverse line
          putStrLn $ line'

main' :: IO()
main' = do line <- fmap reverse getLine
           putStrLn $ line

main2 :: IO()
main2 = do line <- fmap (intersperse '-' . reverse . map toUpper) getLine
           putStrLn line

{-
    A type constructor has to take exactly one TYPE parameter so that
    it can be made an instance of Functor.

    fmap :: (a -> b) -> f a -> f b

    e.g. (->)
    fmap :: (a -> b) -> (r -> a) -> (r -> b)
    fmap takes a function from a to b (a -> b) and a function from
    r to a (r -> a) and returns a function from r to b (r -> b)

    We pipe the output of (r -> a) into the input of (a -> b) to 
    get a function (r -> b).. Function composition
-}

-- mapping of fmap over a list
-- can only accept lists as input
testfmap = map $ fmap (*3) (+2)
cd{-
    The type of fmap means that the function will work on any
    functor. If we use fmap (*3) on a list, the list's implementation
    for fmap will be chosen, which is just map. If we use it on Maybe
    it will apply (*3) to the value inside the Just, or if it is 
    Nothing, then it stays Nothing.
-}
-- using fmap to call the appriate function.
-- accepts any kind of input, if list is given,
-- then fmap calls map on the list.
testfmap2 :: (Functor f, Num a) => f a -> f a
testfmap2 = fmap (fmap (*3) (+2))

data CMaybe a = CNothing | CJust Int a deriving (Show)

instance Functor CMaybe where
    fmap f CNothing = CNothing
    fmap f (CJust counter x) = CJust (counter+1) (f x)

