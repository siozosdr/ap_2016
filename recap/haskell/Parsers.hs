import Control.Monad 
import Control.Applicative
{-
    Empty list denotes error, non-empty lists denote success
    In case of success, each result is a pair. First 
    component is a value of type a produced by parsing and
    processing a prefix of the argument string. Second
    component is the unparsed suffix of the argument string.
-}
newtype Parser a = Parser (String -> [(a,String)])

data Hole = Hole
item :: Parser Char
item = Parser (\cs -> case cs of
    "" -> []
    (c:cs') -> [(c,cs')])

parse :: Parser a -> String -> [(a, String)]
parse (Parser p) = p
instance Functor Parser where
    fmap = liftM

instance Applicative Parser where
    pure a = Parser (\cs -> [(a,cs)]) 
    (<*>) = ap
instance Monad Parser where
    -- f :: a -> Parser b
    m >>= f = Parser $ (\cs -> 
        concat [parse (f a) cs' |
            (a, cs') <- parse m cs])

p :: Parser (Char, Char)
p = do {c <- item; item; d<-item; return (c,d)}

instance MonadPlus Parser where
    mzero = Parser (\cs -> [])
    mplus p q = Parser (\cs -> parse p cs ++ parse q cs)

instance Alternative Parser where
    (<|>) = mplus
    empty = mzero

(+++) :: Parser a -> Parser a -> Parser a
p +++ q = Parser (\cs -> case parse (mplus p q) cs of
                            [] -> []
                            (x:xs) -> [x])

sat :: (Char -> Bool) -> Parser Char
sat p = do 
    c <- item
    if p c then return c else mzero

char :: Char -> Parser Char
char c = sat (c ==)

-- parse specific string
string :: String -> Parser String
string "" = return ""
string (c:cs) = do
    char c
    string cs
    return (c:cs)
-- parse repeated applications of a parser p
-- the many' combinator permits zero or more applications of p
many' :: Parser a -> Parser [a]
many' p = many1 p +++ return []
--many1 permits one or more applications of p
many1 :: Parser a -> Parser [a]
many1 p = do
    a <- p
    as <- many' p
    return (a:as)

-- parse repeated application of a parser p,
-- separated by applications of a parser sep
-- whose result values are thrown away
sepby :: Parser a -> Parser b -> Parser [a]
p `sepby` sep = (p `sepby1` sep) +++ return []

sepby1 :: Parser a -> Parser b -> Parser [a]
p `sepby1` sep = do
    a <- p
    as <- many (do 
        sep
        p)
    return (a:as)

{-  Parse repeated applications of a parser p, separated
    by applications of a parser op whose result value is an
    operator that is assumed to associate to the left, and
    which is used to combine results from the p parsers:
-}

chainl :: Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = (p `chainl1` op) +++ return a

chainl1 :: Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl1` op = do
    a <- p
    rest a
    where
        rest a = (do f <- op
                     b <- p
                     rest (f a b))+++ return a 
                  
