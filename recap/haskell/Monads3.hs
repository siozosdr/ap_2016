import Control.Monad
addStuff :: Int -> Int
addStuff = do
    a <- (*2)
    b <- (+10)
    return (a+b)

addStuff2 :: Int -> Int
addStuff2 x = let
    a = (*2) x
    b = (+10) x
    in a+b

data Hole = Hole
type Stack = [Int]

pop :: Stack -> (Int, Stack)
pop (x:xs) = (x,xs)

push :: Int -> Stack -> ((), Stack)
push x xs = ((), x:xs)

stackMainip :: Stack -> (Int, Stack)
stackMainip stack = let
    ((), newStack1) = push 3 stack
    (_, newStack2) = pop newStack1
    in pop newStack2

stackMainip' :: Stack -> (Int, Stack)
stackMainip' = do
    push 3
    pop
    pop
newtype State s a = State {runState :: s -> (a, s)}

instance Functor (State s) where
    fmap f m = m >>= \a -> return (f a)

instance Applicative (State s) where
    pure = return; (<*>) = ap {- from Control.Monad-}

instance Monad (State s) where
    return x = State $ \s -> (x,s)
    (State h) >>= f = State $ \s -> let (a, newState) = h s
                                        (State g) = f a
                                        in g newState
    {-
        bind can also be defined as
        h >>= f = State $ \s -> let (a, newState) = runState h s
                                    g = f a
                                    in runState g newState 
    -}

pop' :: State Stack Int
pop' = State $ \(x:xs) -> (x, xs)

push' :: Int -> State Stack Int
push' a = State $ \s -> (a, a:s)

stackManip :: State Stack Int
stackManip = State $ do
    push 3
    pop
    pop

stackStuff :: State Stack ()
stackStuff = State $ do
    (a, s) <- pop
    if a == 5
        then push 5
        else do
            push 3
            push 8
get :: State s s
get = State $ \s -> (s,s)

set :: s -> State s ()
set s = State $ \_ -> ((), s)

