module QCtests where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck as QC

data Tree a = Null | Fork a (Tree a) (Tree a)
    deriving (Eq,Show)

empty :: Tree a
empty = Null

invariant :: Ord a => Tree a -> Bool
invariant Null = True
invariant (Fork x l r) = smaller x l && smaller x r

smaller :: Ord a => a -> Tree a -> Bool
smaller _ Null = True
smaller x (Fork y l r) = x <=y && invariant (Fork y l r)

minElem :: Tree t -> t
minElem (Fork x _ _) = x

insert :: Ord a => a -> Tree a -> Tree a
insert x Null = Fork x Null Null
insert x (Fork y l r) = Fork (min x y) r (insert (max x y) l)

balanced :: Tree t -> Bool
balanced Null = True
balanced (Fork _ l r) = (d==0 || d==1) && balanced l && balanced r
    where d = weight r - weight l

weight :: Num a => Tree t -> a
weight Null = 0
weight (Fork _ l r) = 1 + weight l + weight r

--make :: [Integer] -> Tree Integer
--make ns = foldl (\h n -> insert n h) empty ns

prop_invariant :: [Op] -> Bool
prop_invariant ns = invariant (make ns)
prop_balanced :: [Op] -> Bool
prop_balanced ns = balanced (make ns)

deleteMin (Fork x l r) = merge l r

merge l Null = l
merge Null r = r
merge l r | minElem l <= minElem r = join l r
          | otherwise              = join r l

join (Fork x l r) h = Fork x r (merge l h)

data Op = Insert Integer | DeleteMin
    deriving Show
make ops = foldl op Null ops
    where 
        op h (Insert n) = insert n h
        op Null DeleteMin = Null
        op h DeleteMin = deleteMin h

-- QC generators
-- generate insertions twice as often as deletions
instance Arbitrary Op where
    arbitrary = 
        -- choose arbitrary integer, generate Insert containing it
        frequency [(2, do n<- arbitrary; return (Insert n)), 
        -- geerate a DeleteMin directly
                    (1, return DeleteMin)]

credits Null = 0
credits h@(Fork _ l r) =
    credits l + credits r + if good h then 0 else 1

good (Fork _ l r) = weight l <= weight r

prop_cost_insert n ops =
    cost_insert h <= 2* logBase 2.0 (weight h) + 1
    where h = make ops

prop_cost_insert n ops =
    cost_insert h + credits (insert n h)
    <=
    2*log (weight h) + 1 + credits h
    where h = make ops

prop_cost_deleteMin ops =
    h/=Null ==>
        cost_deleteMin h + credits (deleteMin h)
        <=
        2* logBase 2.0 (weight h) + credits h
    where h = make ops
------- QUICKCHECK TESTING------------
unitTests :: TestTree
unitTests = testGroup "Unit Tests"
    [

    ]
propertyTests :: TestTree
propertyTests = testGroup "QuickCheck"
    [
        QC.testProperty "prop_invariant" $
        prop_invariant,
        QC.testProperty "prop_balanced" $
        prop_balanced,
        QC.testProperty "prop_cost_insert" $
        prop_cost_insert,
        QC.testProperty "prop_cost_deleteMin" $
        prop_cost_deleteMin
    ]

tests :: TestTree
tests = testGroup "Tests" [propertyTests, unitTests]

main :: IO ()
main = defaultMain tests