module Paths_testProj (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/sokratis/Desktop/haskell/testProj/.stack-work/install/x86_64-linux/lts-6.15/7.10.3/bin"
libdir     = "/home/sokratis/Desktop/haskell/testProj/.stack-work/install/x86_64-linux/lts-6.15/7.10.3/lib/x86_64-linux-ghc-7.10.3/testProj-0.1.0.0-I59AqQ9kOf67f3cjiZqPVg"
datadir    = "/home/sokratis/Desktop/haskell/testProj/.stack-work/install/x86_64-linux/lts-6.15/7.10.3/share/x86_64-linux-ghc-7.10.3/testProj-0.1.0.0"
libexecdir = "/home/sokratis/Desktop/haskell/testProj/.stack-work/install/x86_64-linux/lts-6.15/7.10.3/libexec"
sysconfdir = "/home/sokratis/Desktop/haskell/testProj/.stack-work/install/x86_64-linux/lts-6.15/7.10.3/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "testProj_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "testProj_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "testProj_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "testProj_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "testProj_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
