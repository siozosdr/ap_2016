import Data.Monoid
import Control.Monad.Writer

{-
    the Writer monad is for values that have another 
    value attached that acts as a sort of log value. 
    Writer allows us to do computations while making 
    sure that all the log values are combined into 
    one log value that then gets attached to the result.
-}

isBigGang :: Int -> Bool
isBigGang x = x > 9

isBigGang2 :: Int -> (Bool, String)
isBigGang2 x = (x>9, "Compared gang size to 9.")

applyLog :: (a, [c]) -> (a -> (b, [c])) -> (b, [c])
applyLog (x, log) f = let (y, newLog) = f x in (y, log ++ newLog)

{-
applyLog' :: (a, String) -> (a -> (b, String)) -> (b, String)
applyLog' (x, log) f = do
    (y, newLog) <- f x
    (y, log ++ newLog)
    The above does not work at (y, newLog) <- f x 
    because the '<-' expects the result of fx to be in a context
    or a Monadic context.
-}

applyLog2 :: (Monoid m) => (a, m) -> (a -> (b, m)) -> (b, m)
applyLog2 (x , log) f = let (y , newLog) = f x in (y, log `mappend` newLog)

type Food = String
type Price = Sum Int

addDrink :: Food -> (Food, Price)
addDrink "beans" = ("milk", Sum 25)
addDrink "jerky" = ("beer", Sum 50)
addDrink _ = ("water", Sum 20) 

newtype Writer w a = Writer { runWriter :: (a, w) }
instance (Monoid w) => Monad (Writer w) where  
    return x = Writer (x, mempty)  
    (Writer (x,v)) >>= f = let (Writer (y, v')) = f x in Writer (y, v `mappend` v')  

logNumber :: Int -> Writer [String] Int
logNumber x = writer (x, ["Got number " ++ show x])

multWithLog :: Writer [String] Int
multWithLog = do
    a <- logNumber 3
    b <- logNumber 5        
    return a * b

multWithLog2 :: Writer [String] Int  
multWithLog2 = do  
    a <- logNumber 3  
    b <- logNumber 5  
    return (a*b)  


gcd' :: Int -> Int -> Int
gcd' a b
    | b == 0 = a
    | otherwise = gcd' b (a `mod` b)

newtype DiffList a = DiffList {getDiffList :: [a] -> [a]}

toDiffList :: [a] -> DiffList a
toDiffList x = DiffList (x++)

fromDiffList :: DiffList a -> [a]
fromDiffList (DiffList f) = f []

instance Monoid (DiffList a) where
    mempty = DiffList (\xs -> [] ++ xs)
    (DiffList f) `mappend` (DiffList g) = DiffList (\xs -> f (g xs))

finalCountDown :: Int -> WriterT (DiffList String) ()
finalCountDown 0 = do
    tell (toDiffList ["0"])
finalCountDown x = do
    finalCountDown (x-1)
    tell (toDiffList [show x])