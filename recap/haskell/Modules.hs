import Data.List (nub, tails)
import qualified Data.Map as M
import qualified Data.Set as S

numUniques :: (Eq a) => [a] -> Int  
numUniques = length . nub  

search :: (Eq a) => [a] -> [a] -> Bool
search n h =
    let nlen = length n
    in foldl (\acc x -> if take nlen x == n then True else acc ) False (tails h)

phoneBook :: [(String, String)]
phoneBook =   
    [("betty","555-2938"),("bonnie","452-2928"),("patsy","493-2928"),("lucille","205-2928"),("wendy","939-8282"),("penny","853-2492")]  

findKey :: (Eq k) => k -> [(k, v)] -> Maybe v
findKey _ [] = Nothing
--findKey key ((k,v):xs) 
--    | key == k = Just v
--    | otherwise = findKey key xs 
--findKey key xs = snd . head . filter (\(k,_) -> key == k) $ xs 

--findKey key ((k,v) : xs) = if key == k 
--    then Just v
--    else findKey key xs

findKey key xs = foldr (\(k, v) _ -> if k == key then Just v else Nothing) Nothing xs

fromList' :: (Ord k) => [(k,v)] -> M.Map k v
fromList' = foldr (\(k,v) acc -> M.insert k v acc) M.empty

text1 :: String
text1 = "I just had an anime dream. Anime... Reality... Are they so different?"  
text2 :: String
text2 = "The old man left his garbage can out and now his trash is all over my lawn!" 

data Person = Person { firstName :: String  
                     , lastName :: String  
                     , age :: Int  
                     , height :: Float  
                     , phoneNumber :: String  
                     , flavor :: String  
                     } deriving (Show) 

data Car = Car { company :: String
               , model :: String
               , year :: Int} deriving (Show)

data Car' a b c = Car' { company' :: a
                       , model' :: b
                       , year' :: c} deriving (Show)
tellCar :: Car -> String    
tellCar (Car {company = a, model = b, year = c}) = "This" ++ a ++ " " ++ b ++ " " ++ "was made in " ++ show c