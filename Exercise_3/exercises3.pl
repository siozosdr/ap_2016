% call helper function and get results
reverse_list1(List, Result) :-
	reverse_list_helper(List, [], Result).

% end case: give accumulator result to output result
reverse_list_helper([],Result,Result).
% append recurse with tail and head appened to the accumulator
reverse_list_helper([Head|Tail],Acc, Result) :-
	reverse_list_helper(Tail,[Head|Acc], Result).

reverselist2([], []).
reverselist2([H|T], R) :- 
	reverselist2(T, RevT),
	append(RevT, [H], R).

palindrome(List) :-
	reverse_list1(List, Reversed),
	List = Reversed.

duplicate([],[]).
duplicate(List, Result) :-
	duplicate_helper(List,[] ,Result).

duplicate_helper([], Acc, Result) :-
	reverse_list1(Acc, Result).
duplicate_helper([Head|Tail], Acc, Result) :-
	duplicate_helper(Tail, [Head, Head|Acc], Result).

duplicate2([],[]).
duplicate2([H|T], [H,H | Res]) :- duplicate2(T, Res).

compress(List, Result) :-
	compress_helper(List, [], Result).

compress_helper([Last], Acc, [Last |Acc]) :-
	reverse_list1([Last|Acc],Result).
compress_helper([Head1, Head2 |Tail], Acc, Result) :-
	Head1 \= Head2, !,
	compress_helper([Head2 |Tail], [Head1|Acc], Result).
compress_helper([Head, Head |Tail], Acc, Result) :-
	compress_helper([Head|Tail], Acc, Result).

cmp([H,H|T], Acc, Res) :- cmp([H|T], Acc, Res), !.
cmp([H|T], Acc,Res) :- cmp(T,[H|Acc], Res).
cmp([],L,R) :- reverse_list1(L,R), write(R), nl.

member1(_, []) :- fail.
member1(X, [Y |Tail]) :-
	X\=Y,!,
	member1(X, Tail).
member1(X, [X|_]).

append1([], List2, List2).
append1([Head |Tail], List2, [Head |Result]) :-
	append1(Tail, List2, Result).


sublist(SubList, List) :- 
	sublist_helper(SubList, [], List).

sublist_helper([], _, _) :- !.
sublist_helper(_, _, []) :- fail.
sublist_helper([Head|Tail],Acc, [Head2 | Tail2]) :-
	Head = Head2, !,
	%write("rec1: "), write([Head|Tail]), write(", "), write(Acc), write(", "), write([Head2 | Tail2]), nl,
	sublist_helper(Tail, [Head |Acc], Tail2).
sublist_helper([Head1 |Tail1], Acc, [Head2 |Tail2]) :-
	Head1 \= Head2,
	reverse_list1(Acc, NewAcc),
	append1(NewAcc, [Head1 | Tail1], NewSublist),
	%write("rec2: "), write([Head1|Tail1]), write(", "), write(Acc), write(", "), write(Tail2), nl,
	sublist_helper(NewSublist, [], Tail2).
