bigger(elephant, horse).
bigger(horse, donkey).
bigger(donkey, dog).
bigger(donkey, monkey).

is_bigger(X, Y) :- bigger(X, Y).
is_bigger(X, Y) :- bigger(X, Z), is_bigger(Z, Y).

is_smaller(X, Y) :- is_bigger(Y, X).
aunt(Aunt, Child) :- 
	sister(Aunt, Parent),
	parent(Parent, Child).
/*Comment*/
mortal(X) :- man(X).
man(sokratis). % subgoal comment

blond(X) :-
	father(Father, X),
	blond(Father),
	mother(Mother, X),
	blond(Mother).

a(B,B).
female(mary).
female(sandra).
female(juliet).
female(lisa).
male(peter).
male(paul).
male(dick).
male(bob).
male(harry).
parent(bob, lisa).
parent(bob, paul).
parent(bob, mary).
parent(juliet, lisa).
parent(juliet, paul).
parent(juliet, mary).
parent(peter, harry).
parent(lisa, harry).
parent(mary, dick).
parent(mary, sandra).

father(X, Y) :-
	male(X),
	parent(X,Y).
sister(X, Y) :-
	female(X),
	parent(Z, X),
	parent(Z, Y).
grandmother(X, Y) :-
	female(X),
	parent(X, Z),
	parent(Z, Y).
cousin(X,Y) :-
	parent(Z, X),
	parent(W, Y),
	parent(G, Z),
	parent(G, W),
	X\=Y,
	Z\=W.
brother(X, Y) :-
	parent(Z, X),
	parent(Z,X),
	male(X),
	X\=Y.


concat_lists([Elem | List1], List2, (Elem | List3)) :- 
	concat_lists(List1,List2,List3).
concat_lists([], List, List).

analyse_list([]) :-
	write('emptylist').
analyse_list([H|T]) :-
	write(H),nl,
	write(T).
membership(_, []) :- fail.
membership(X, [X|_]) :- !.
membership(X, [_|T]) :-
	membership(X,T).

not_membership(_, []) :- !.
not_membership(X,[X|_]) :- fail.
not_membership(X, [Y|T]) :-
	X \= Y,
	not_membership(X,T).

remove_duplicates([],_).
remove_duplicates([X|T], [X|Result]) :-
	not_membership(X, Result),
	remove_duplicates(T, Result).
remove_duplicates([X|T], Result) :-
	membership(X,Result),
	remove_duplicates(T, Result).

reverse_list(L,R):-accReverse(L,[],R).
accReverse([], R, R).
accReverse([X|T], A, R) :-
	accReverse(T, [X|A], R).

whoami([]).
whoami([_,_|Rest]):-
	whoami(Rest).

last1([H|T],Result) :-
	last1_(T, H, Result).
last1_([],X, X).
last1_([H|T], _, X) :-
	last1_(T, H, X).

distance((X1,Y1),(X2,Y2), Result) :-
	Result is ((X1-X2)**2 + (Y1-Y2)**2)**(1/2).

square(0, _).
square(N,X) :-
	M is N-1,
	line(N,X),
	square(M,X).

line(0,_) :- nl.
line(N,X) :-
	write(X),
	M is N-1,
	line(M, X).

fibonacci(0, 1).
fibonacci(1, 1).
fibonacci(F, Result) :-
	F > 1,
	F1 is F-1,
	F2 is F-2,
	fibonacci(F1,X1),
	fibonacci(F2,X2),
	Result is X1 + X2.
fib(0,0).
fib(1,1).
fib(F,N) :- 
    N>1,
    N1 is N-1,
    N2 is N-2,
    fib(F1,N1),
    fib(F2,N2),
    F is F1+F2.

element_at([],_,_).
element_at([_|T], Pos, Res) :-
	Pos > 0,
	NewPos is Pos-1,
	element_at(T, NewPos, Res).
element_at([Result|_], 0, Result).

mean(L, Res) :-
	sum1(L, S),
	length1(L, Len),
	Res is S/Len.

sum1([],0).
sum1([H|T], Sum2) :-
	sum1(T,Sum),
	Sum2 is Sum+H.

length1([],0).
length1([_|T], Res2) :-
	length1(T, Res),
	Res2 is Res+1.

range(_, _, []).
range(Start,End, [Start|Result]) :-
	NewStart is Start+1,
	NewStart =< End,
	range(NewStart, End, Result).

permutation1([], []).
permutation1(List, [Element| Permutation]) :-
	select(Element, List, Rest),
	permutation1(Rest, Permutation).

% terminal case
remove_duplicates1([],[]).

/*
	succeeds if head member of tail
		and recusion succeeds
*/
remove_duplicates1([Head|Tail], Result) :-
	membership(Head,Tail), !,
	remove_duplicates1(Tail, Result).

remove_duplicates1([Head|Tail], [Head|Result]) :-
	remove_duplicates1(Tail, Result).

add_element(Element, List, Result) :-
	membership(Element, List), !,
	Result = List.
add_element(Element, List, [Element | List]).