module MatchParser where

import Data.Char as Char
import Control.Applicative
  ( Alternative((<|>), empty, many, some) )

import Control.Monad ( void )

newtype MatchParser a = MatchParser {
  runParser :: String -> Maybe (a, String)
}

instance Functor MatchParser where
  fmap f m = m >>= \a -> return (f a)

instance Applicative MatchParser where
  pure = return
  df <*> dx = df >>= \f -> dx >>= return . f

instance Monad MatchParser where
  -- return :: a -> MatchParser a
  return a = MatchParser $ \ s -> Just (a, s)

  -- (>>=) :: MatchParser a
  --          -> (a -> MatchParser b)
  --          -> MatchParser b 
  m >>= f = MatchParser $ \s -> do
    (a, s') <- runParser m s
    runParser (f a) s'

  -- fail :: String -> MatchParser a
  fail _ = MatchParser $ \ _ -> Nothing

getc :: MatchParser Char
getc = MatchParser getc'
  where getc' "" = Nothing
        getc' (x:xs) = Just (x, xs)

reject :: MatchParser a
reject = MatchParser $ \ _ -> Nothing

char :: Char -> MatchParser Char
char c = do
  c' <- getc
  if c == c' then return c else reject

string :: String -> MatchParser String
string "" = return ""
string (c:cs) = do
  void $ char c
  void $ string cs
  return (c:cs)

chars :: [Char] -> MatchParser Char
chars cs = do
  c <- getc
  if c `elem` cs then return c else reject

wild :: MatchParser ()
wild = void $ getc

parse :: MatchParser a -> String -> Maybe (a, String)
parse = runParser

instance Alternative MatchParser where
  -- empty :: a -> MatchParser a
  empty = reject

  -- (<|>) :: MatchParser a -> MatchParser a
  --          -> MatchParser a
  p <|> q = MatchParser $ \cs ->
    parse p cs <|> parse q cs

satisfy :: (Char -> Bool) -> MatchParser Char
satisfy p = do
  c <- getc
  if p c then return c else reject

option :: a -> MatchParser a -> MatchParser a
option v p = p <|> return v

choice :: [MatchParser a] -> MatchParser a
choice [] = reject
choice (p:ps) = p <|> choice ps

-- parser to match between parenthesis
-- >> throws away the value from the left  side
-- <* throws away the value from the right side
between :: MatchParser open -> MatchParser close 
           -> MatchParser a -> MatchParser a
between open close p = (void $ open) >> p <* (void $ close)

keywords :: [String]
keywords = ["if", "then", "else"]

varName :: MatchParser String
varName = do
  cs <- some $ chars ['a'..'z']
  if cs `elem` keywords then reject else return cs

ntimes :: Word -> MatchParser a -> MatchParser [a]
ntimes 0 _ = reject
ntimes 1 p =
  do v <- p; return [v]
ntimes n p = do
  v <- p
  vs <- ntimes (n-1) p
  return (v:vs)
{-
  
-}
lenString :: MatchParser String
lenString = do
  cn <- chars ['1'..'9']
  cns <- many $ chars ['0'..'9']
  let n = read (cn:cns) -- See report.pdf
  count n (chars ['a'..'z'])
-- same as ntimes
count :: Integer -> MatchParser Char -> MatchParser String
count 0 _ = reject
count 1 p = 
  do v <- p; return [v]
count n p = do
  v <- p
  vs <- count (n-1) p
  return (v:vs)
get :: MatchParser String
get = MatchParser $ \ s -> Just (s, s)

eol :: MatchParser ()
eol = do
  s <- get
  case s of
    ""  -> return ()
    _   -> reject

-- 1.1
ichar :: Char -> MatchParser Char
ichar c = do
  c' <- getc
  if Char.toLower c' == Char.toLower c then 
    return c' 
    else reject

istring :: String -> MatchParser String
istring "" = return ""
istring (c:cs) = do
  void $ ichar c
  void $ istring cs
  return (c:cs)

myparser1 :: MatchParser String
myparser1 = do
  istring "haskell"


myparser2 :: MatchParser String
myparser2 = do
  s1 <- myparser1
  void $ getc
  void $ many $ char ' '
  s2 <- istring "prolog"
  void $ getc
  void $ many $ char ' '
  s3 <- istring "erlang"
  void $ many $ char ' '
  return (s1++s2++s3)

--1.2 Character classes
parser1_4 :: MatchParser Char
parser1_4 = do
  cs <- chars ['a'..'z']
  return cs

parser1_5 :: MatchParser Char
parser1_5 = do
  cs1 <- chars ['a'..'z'] <|> chars ['A'..'Z'] <|> chars ['0'..'9']
  void $ many $ char ' '
  return (cs1)

parser1_6 :: MatchParser String
parser1_6 = do
  s1 <- chars['a'..'z']
  s2 <- chars['A'..'Z']
  s3 <- chars['0'..'9']
  return (s1:[s2]++[s3])

parser1_7 :: MatchParser String
parser1_7 = do
  c1 <- parser1_4
  c2 <- parser1_4
  c3 <- parser1_4
  c4 <- chars ['0'..'9']
  c5 <- chars ['0'..'9']
  c6 <- chars ['0'..'9']
  return ([c1,c2,c3,c4,c5,c6])

-- 1.3 Wildcard
parser1_8 :: MatchParser Char
parser1_8 = do
  c <-getc
  return c

parser1_9 :: MatchParser String
parser1_9 = do
  c1 <- parser1_8
  c2 <- parser1_8
  return (c1:[c2])

parser1_10 :: MatchParser String
parser1_10 = do
  c1 <- parser1_8
  c2 <- parser1_4
  c3 <- parser1_8
  return (c1:[c2]++[c3])

-- 1.4 Choice
parser1_11 :: MatchParser Char
parser1_11 = do
  c1 <- char 'a' <|> char 'b'
  return c1

parser1_12 :: MatchParser Char
parser1_12 = parser1_5

parser1_13 :: MatchParser String
parser1_13 = do
  c <- chars ['a','b']
  s <- many $ char c
  let res = c:s
  return res

parser1_14 :: MatchParser String
parser1_14 = do 
  s <- many $ char 'a'
  return s

parser1_15 :: MatchParser String
parser1_15 = do
  s <- many $ chars ['a'..'z']
  return s

parser1_16 :: MatchParser String
parser1_16 = do
  s <- many getc
  return s

parser1_18 :: String -> MatchParser String
parser1_18 s = 
  case s of
    "" -> return ""
    _ -> do
      res <- many $ string s
      return $ concat res

parser1_19 :: [Char] -> MatchParser Char
parser1_19 cs = do
  c <- chars cs
  return c

parser1_20 :: MatchParser String
parser1_20 = do
  cs <- some $ char 'a' 
  return cs

parser1_21 :: MatchParser String
parser1_21 = do
  cs <- some $ chars ['a'..'z']
  return cs

parser1_23 :: MatchParser String
parser1_23 = do
  cs <- some $ getc
  return cs

parser1_24 :: MatchParser Char
parser1_24 = do
  res <- option 'x' (char 'a')
  return res

parser1_25 :: MatchParser String
parser1_25 = do
  res <- option "" (many $ chars ['a'..'z'])
  return res

parser1_27 :: MatchParser String
parser1_27 = do
  s <- option "" (ntimes 3 (char 'a'))
  return s

parser1_28 :: MatchParser String
parser1_28 = do
  s1 <- ntimes 3 (char 'a')
  c2 <- char 'a'
  c3 <- char 'a'
  return $ s1++[c2]++[c3]

parser1_29 :: MatchParser String
parser1_29 = do 
  s1 <- ntimes 3 (char 'a')
  s2 <- many $ char 'a'
  return $ s1 ++ s2

parser1_30 :: MatchParser String
parser1_30 = undefined

parser1_31 :: MatchParser String
parser1_31 = do
  c1 <- char 'a' <|> char 'b'
  c2 <- char 'c' <|> char 'd'
  return [c1,c2]