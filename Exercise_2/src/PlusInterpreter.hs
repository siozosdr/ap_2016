module PlusInterpreter (
	ParserError,
	parseString) where

import ExprParser
import PlusAst

data ParserError a = NoParse | Ambiguity [a]
	deriving (Eq, Show)
parseString :: String -> Either (ParseError Expr) Expr
parseString s = 
	case fullParse lPlus s of
		[] -> Left $ NoParse
		[a] -> Right $ a
		as -> Left $ Ambiguity as
eval :: Expr -> Int
eval (Con n) = n
eval (Add e1 e2) = (eval e1) + (eval e2)