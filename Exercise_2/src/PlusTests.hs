module Tests where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck as QC

import Control.Applicative ( Alternative( (<|>), empty, many ) )
import PlusAst
import PlusParser
import PlusInterpreter
import MatchParser

unitTests :: TestTree
unitTests = testGroup "Unit Tests"
  [ testCase "5 + 6 = 6 + 5" $
      fmap eval (parseString "5+6") @?=
        fmap eval (parseString "6+5")
    
  ]
data TestCase = TestCase { 
  caseE :: Expr, 
  caseV :: Int 
  } deriving (Eq, Show)

instance Arbitrary TestCase where
  arbitrary = do
    n <- arbitrary
    return $ TestCase (Con n) n
  --shrink :: TestCase -> [TestCase]


properties :: TestTree
properties = testGroup "QuickCheck"
  [ QC.testProperty "Fancy property" $
      \e -> eval (caseE t) == (caseV t) 
  ]

tests :: TestTree
tests = testGroup "Tests" [properties, unitTests]

main :: IO ()
main = defaultMain tests
