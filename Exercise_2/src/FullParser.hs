module FullParser where

get :: Parser String 
get = Parser $ \s -> Just (s, s)

getc :: Parser Char
getc = undefined
newtype Parser a = Parser {
	runParser :: String -> Maybe (a, String) 
}
fullParse :: Parser a -> String -> [a]
fullParse = runParser