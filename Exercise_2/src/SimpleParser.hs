module SimpleParser where

import Data.Char as Char
import Control.Applicative
  ( Alternative((<|>), empty, many, some) )

import Control.Monad ( void )

newtype SimpleParser a = SimpleParser {
  runParser :: String -> Maybe (a, String)
}

instance Functor SimpleParser where
  fmap f m = m >>= \a -> return (f a)

instance Applicative SimpleParser where
  pure = return
  df <*> dx = df >>= \f -> dx >>= return . f

instance Monad SimpleParser where
  -- return :: a -> SimpleParser a
  return a = SimpleParser $ \ s -> Just (a, s)

  -- (>>=) :: SimpleParser a
  --          -> (a -> SimpleParser b)
  --          -> SimpleParser b 
  m >>= f = SimpleParser $ \s -> do
    (a, s') <- runParser m s
    runParser (f a) s'

  -- fail :: String -> SimpleParser a
  fail _ = SimpleParser $ \ _ -> Nothing

getc :: SimpleParser Char
getc = SimpleParser getc'
  where getc' "" = Nothing
        getc' (x:xs) = Just (x, xs)

reject :: SimpleParser a
reject = SimpleParser $ \ _ -> Nothing

char :: Char -> SimpleParser Char
char c = do
  c' <- getc
  if c == c' then return c else reject

string :: String -> SimpleParser String
string "" = return ""
string (c:cs) = do
  void $ char c
  void $ string cs
  return (c:cs)

chars :: [Char] -> SimpleParser Char
chars cs = do
  c <- getc
  if c `elem` cs then return c else reject

wild :: SimpleParser ()
wild = void $ getc

parse :: SimpleParser a -> String -> Maybe (a, String)
parse = runParser

instance Alternative SimpleParser where
  -- empty :: a -> SimpleParser a
  empty = reject

  -- (<|>) :: SimpleParser a -> SimpleParser a
  --          -> SimpleParser a
  p <|> q = SimpleParser $ \cs ->
    parse p cs <|> parse q cs

satisfy :: (Char -> Bool) -> SimpleParser Char
satisfy p = do
  c <- getc
  if p c then return c else reject

space :: SimpleParser Char
space = satisfy Char.isSpace

spaces :: SimpleParser String
spaces = many space
token :: SimpleParser a -> SimpleParser a
token p = space >> p

stoken :: String -> SimpleParser ()
stoken = void . token . string
option :: a -> SimpleParser a -> SimpleParser a
option v p = p <|> return v

choice :: [SimpleParser a] -> SimpleParser a
choice [] = reject
choice (p:ps) = p <|> choice ps

-- parser to match between parenthesis
-- >> throws away the value from the left  side
-- <* throws away the value from the right side
between :: SimpleParser open -> SimpleParser close 
           -> SimpleParser a -> SimpleParser a
between open close p = (void $ open) >> p <* (void $ close)

keywords :: [String]
keywords = ["if", "then", "else"]

varName :: SimpleParser String
varName = do
  cs <- some $ chars ['a'..'z']
  if cs `elem` keywords then reject else return cs

ntimes :: Word -> SimpleParser a -> SimpleParser [a]
ntimes 0 _ = reject
ntimes 1 p =
  do v <- p; return [v]
ntimes n p = do
  v <- p
  vs <- ntimes (n-1) p
  return (v:vs)
{-
  
-}
lenString :: SimpleParser String
lenString = do
  cn <- chars ['1'..'9']
  cns <- many $ chars ['0'..'9']
  let n = read (cn:cns) -- See report.pdf
  count n (chars ['a'..'z'])
-- same as ntimes
count :: Integer -> SimpleParser Char -> SimpleParser String
count 0 _ = reject
count 1 p = 
  do v <- p; return [v]
count n p = do
  v <- p
  vs <- count (n-1) p
  return (v:vs)
get :: SimpleParser String
get = SimpleParser $ \ s -> Just (s, s)

eol :: SimpleParser ()
eol = do
  s <- get
  case s of
    ""  -> return ()
    _   -> reject

-- 1.1
ichar :: Char -> SimpleParser Char
ichar c = do
  c' <- getc
  if Char.toLower c' == Char.toLower c then 
    return c' 
    else reject

istring :: String -> SimpleParser String
istring "" = return ""
istring (c:cs) = do
  void $ ichar c
  void $ istring cs
  return (c:cs)

myparser1 :: SimpleParser String
myparser1 = do
  istring "haskell"


myparser2 :: SimpleParser String
myparser2 = do
  s1 <- myparser1
  void $ getc
  void $ many $ char ' '
  s2 <- istring "prolog"
  void $ getc
  void $ many $ char ' '
  s3 <- istring "erlang"
  void $ many $ char ' '
  return (s1++s2++s3)

--1.2 Character classes
parser1_4 :: SimpleParser Char
parser1_4 = do
  cs <- chars ['a'..'z']
  return cs

parser1_5 :: SimpleParser Char
parser1_5 = do
  cs1 <- chars ['a'..'z'] <|> chars ['A'..'Z'] <|> chars ['0'..'9']
  void $ many $ char ' '
  return (cs1)

parser1_6 :: SimpleParser String
parser1_6 = do
  s1 <- chars['a'..'z']
  s2 <- chars['A'..'Z']
  s3 <- chars['0'..'9']
  return (s1:[s2]++[s3])

parser1_7 :: SimpleParser String
parser1_7 = do
  c1 <- parser1_4
  c2 <- parser1_4
  c3 <- parser1_4
  c4 <- chars ['0'..'9']
  c5 <- chars ['0'..'9']
  c6 <- chars ['0'..'9']
  return ([c1,c2,c3,c4,c5,c6])

-- 1.3 Wildcard
parser1_8 :: SimpleParser Char
parser1_8 = do
  c <-getc
  return c

parser1_9 :: SimpleParser String
parser1_9 = do
  c1 <- parser1_8
  c2 <- parser1_8
  return (c1:[c2])

parser1_10 :: SimpleParser String
parser1_10 = do
  c1 <- parser1_8
  c2 <- parser1_4
  c3 <- parser1_8
  return (c1:[c2]++[c3])

-- 1.4 Choice
parser1_11 :: SimpleParser Char
parser1_11 = do
  c1 <- char 'a' <|> char 'b'
  return c1

parser1_12 :: SimpleParser Char
parser1_12 = parser1_5

parser1_13 :: SimpleParser String
parser1_13 = do
  c <- many $ char 'a' <|> char 'b'
  return c

parser1_14 :: SimpleParser String
parser1_14 = do 
  s <- many $ char 'a'
  return s

parser1_15 :: SimpleParser String
parser1_15 = do
  s <- many $ chars ['a'..'z']
  return s

parser1_16 :: SimpleParser String
parser1_16 = do
  s <- many getc
  return s