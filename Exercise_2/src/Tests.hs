module Tests where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck as QC

import Control.Applicative ( Alternative( (<|>), empty, many, some ) )

import MatchParser

unitTests :: TestTree
unitTests = testGroup "Unit Tests"
  [ testCase "The reason char returns Char" $
      (parse (many $ chars "ab") "abab") @?=
        (Just ("abab", "")),
    testCase "The reason ichar returns Char" $
      (parse (many $ ichar 'a') "A") @?=
        (Just ("A","")),
    testCase "Testing istring function" $
      (parse (istring "haskell") "hAskElL") @?=
        (Just ("haskell", "")),
    testCase "Testing myparser1" $
      (parse myparser1 "HAsKell") @?=
        (Just ("haskell", "")),
    testCase "Testing parser1_4" $
      (parse parser1_4 "a") @?=
        (Just ('a',"")),
    testCase "Testing parser1_4" $
      (parse parser1_4 "ab") @?=
        (Just ('a',"b")),
    testCase "Testing parser1_5" $
      (parse parser1_5 "2") @?=
        (Just ('2',"")),
    testCase "Testing parser1_7" $
      (parse parser1_7 "dnb823") @?=
        (Just ("dnb823","")),
    testCase "Testing parser1_9" $
      (parse parser1_9 "ab") @?=
        (Just ("ab","")),
    testCase "Testing parser1_10" $
      (parse parser1_10 "ab3") @?=
        (Just ("ab3","")),
    testCase "Testing parser1_13" $
      (parse parser1_13 "aaaaa") @?=
        (Just ("aaaaa","")),
    testCase "Testing parser1_13.2" $
      (parse parser1_13 "bbb") @?=
        (Just ("bbb","")),
    testCase "Testing parser1_13.3" $
      (parse parser1_13 "bbbaa") @?=
        (Just ("bbb","aa")),
    testCase "Testing parser 1.18" $
      (parse (parser1_18 "haskell") "haskellhaskell") @?=
        (Just ("haskellhaskell","")),
    testCase "Testing parser 1_20" $
      (parse parser1_20 "aaa") @?=
        (Just ("aaa", "")),
    testCase "Testing parser 1_20.2" $
      (parse parser1_20 "a") @?=
        (Just ("a", "")),
    testCase "Testing parser 1_20.3" $
      (parse parser1_20 "ab") @?=
        (Just ("a", "b")),
    testCase "Testing parser 1_22" $
      (parse parser1_23 "abc") @?=
        (Just ("abc", "")),
    testCase "Testing parser 1_22.2" $
      (parse parser1_23 "ab@#") @?=
        (Just ("ab@#", "")),
    testCase "Testin parser 1_24" $
      (parse parser1_24 "ab") @?=
        (Just ('a', "b")),
    testCase "Testing parser 1.25" $
      (parse parser1_25 "haskelltest") @?=
        (Just ("haskelltest","")),
    testCase "Testing parser 1.25.2" $
      (parse parser1_25 "haskelltest123") @?=
        (Just ("haskelltest","123")),
    testCase "Testing parser 1.25" $
      (parse parser1_25 "123haskelltest") @?=
        (Just ("","123haskelltest")),
    testCase "Testing parser 1.27" $
      (parse parser1_27 "aaa") @?=
        (Just ("aaa","")),
    testCase "Testing parser 1.27" $
      (parse parser1_27 "abaab") @?=
        (Just ("","abaab")),
    testCase "Testing parser 1.29" $
      (parse parser1_29 "aaaaa") @?=
        (Just ("aaaaa","")),
    testCase "Testing parser 1.29.2" $
      (parse parser1_29 "aa") @?=
        (Nothing),
    testCase "Testing parser 1.29.3" $
      (parse parser1_29 "aaabb") @?=
        (Just ("aaa", "bb"))
  ]

properties :: TestTree
properties = testGroup "QuickCheck"
  [ QC.testProperty "Fancy property" $
      \(s, v) -> parse (chars s) v ==
        parse (foldl ((<|>)) empty (map char s)) v,
    QC.testProperty "Testing istring property" $
      \(s,v) -> parse (istring s) v ==
        parse (mapM ichar s) v,
    QC.testProperty "Testing myparser1" $
      \v -> parse myparser1 v ==
        parse (istring "haskell") v,
    QC.testProperty "Testing parser1_4" $
      \v -> parse parser1_4 v ==
        parse (chars ['a'..'z']) v,
    {-QC.testProperty "Testing parser1_13" $
      \v -> parse parser1_13 v ==
        parse (many $ char 'a' <|> char 'b') v,-}
    QC.testProperty "Testing property 1.17" $
      \v -> parse (parser1_16) v ==
        parse (many getc) v,
    {-QC.testProperty "Testing property 1.18" $
      \(s, v) -> parse (parser1_18 s) v ==
        parse (some $ string s) v,-}
    QC.testProperty "Testing property 1.19" $
      \v -> parse (parser1_19 ['a'..'d']) v ==
        parse (chars ['a'..'d']) v,
    QC.testProperty "Testing property 1.21" $
      \v -> parse parser1_21 v ==
        parse (some $ chars ['a'..'z']) v,
    QC.testProperty "Testing property 1.23" $
      \v -> parse parser1_23 v ==
        parse (some $ getc) v
  ]

tests :: TestTree
tests = testGroup "Tests" [properties, unitTests]

main :: IO ()
main = defaultMain tests
