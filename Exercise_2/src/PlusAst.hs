module PlusAst where

data Expr = Con Int
		  | Add Expr Expr
		  | Mul Expr Expr