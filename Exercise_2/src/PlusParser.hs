module PlusParser where

import Data.Char as Char
import SimpleParser
import PlusAst
import Control.Monad (void)
import Control.Applicative
  ( Alternative((<|>), empty, many, some) )

pNum :: SimpleParser Expr
pNum = token $ do
    c <- chars ['1'..'9']
    cs <- many $ chars ['0'..'9']
    let s = c:cs
    if length s > 4 
        then reject 
        else return $ Con (read s)
stoken :: String -> SimpleParser ()
stoken c = void $ string c
space :: SimpleParser Char
space = satisfy Char.isSpace

spaces :: SimpleParser String
spaces = many space

token :: SimpleParser a -> SimpleParser a
token p = spaces >> p

lPlus :: SimpleParser Expr
lPlus = lPlus' >>= lPlus''
    where 
        lPlus' :: SimpleParser Expr
        lPlus' = pNum
        lPlus'' :: Expr -> SimpleParser Expr
        lPlus'' e1 = (do
            stoken "+"
            e2 <- lPlus'
            lPlus'' (Add e1 e2)) <|> return e1

rPlus :: SimpleParser Expr
rPlus = rPlus' >>= rPlus''
    where
        rPlus' :: SimpleParser Expr
        rPlus' = pNum
        rPlus'' :: Expr -> SimpleParser Expr
        rPlus'' e1 = (do
            stoken "+"
            e2 <- rPlus
            return (Add e1 e2)) <|> return e1