-module(exercise_5_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() -> 
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).
%start_link() ->
%	spawn(supervisor, start_link, [{local, supervisor},?MODULE, []]).

%init(_Args) ->
%	SupFlags = #{strategy => one_for_one, intensity => 1, period => 5},
%    ChildSpecs = [#{id => exercise_5,
%                    start => {exercise_5, start_link, []},
%                    restart => permanent,
%                    shutdown => brutal_kill,
%                    type => worker,
%                    modules => [exercise_5]}],
%    {ok, {SupFlags, ChildSpecs}}.

init([]) ->
    Shop = {exercise_5, {exercise_5, start_link, []},
                permanent, brutal_kill, worker, [exercise_5]},
    Shop1 = {exercise_5_1, {exercise_5, start_link, []},
                permanent, brutal_kill, worker, [exercise_5]},
    Children = [Shop, Shop1],
    RestartStrategy = {one_for_one, 1, 5},
    {ok, {RestartStrategy, Children}}.