-module(exercise_5).
-behaviour(gen_server).
-export([start_link/0, show/0, 
	add_item/2, buy/2, 
	close/0, return/1]).
-export([init/1, terminate/2, 
	handle_call/3, handle_cast/2, 
	handle_info/2, code_change/3]).


start_link() ->
     gen_server:start_link({local, ?MODULE},?MODULE, [], []).
%start_link() ->
%	spawn(gen_server, start_link, [{local, shop},?MODULE, [], []]).

init([]) -> 
	%erlang:process_flag(trap_exit, true),
	io:format("Shop has started (~w)~n", [self()]),
	{ok, dict:new()}.

show() ->
	gen_server:call(shop, show).

add_item(Item, Price) ->
	gen_server:call(shop, {add, Item, Price}).

buy(Item, Money) ->
	gen_server:call(shop, {buy, Item, Money}).

return(Item) ->
	gen_server:cast(shop, {return, Item}).

close() ->
	gen_server:cast(shop, close).

handle_call({buy, Item, Money}, _From, Groceries) ->
	Res1 = dict:is_empty(Groceries),
	Res2 = dict:is_key(Item, Groceries),
	if 
		Res1 ->
			{reply, out_of_stock, Groceries};
	   	Res2 ->
		   	{Price, Number} = dict:fetch(Item, Groceries),
		   	if 
		   		Price =< Money ->
			   		if 
			   			Number-1 >=0 ->
			   				Change = Money - Price,
			   				NewGroceries = dict:update(Item, fun({_Price, Number}) -> {Price, Number-1} end, Groceries),
			   		   		{reply, {ok, Item, Change}, NewGroceries};
			   		   	true ->
			   		   		{reply, out_of_stock, Groceries}
			   		end;
		   		
		   	   	true ->
		   			{reply, {not_enough_money, Money, price_is, Price}, Groceries}
		   	end
	end;

handle_call(show, _From, Groceries) ->
	Items = [Key  || Key <- dict:fetch_keys(Groceries)],
	{reply, Items, Groceries};

handle_call({add, Item, Price}, _From, Groceries) ->
	NewGroceries = dict:update(Item, fun({_Price, Number}) -> {Price, Number+1} end,{Price, 1}, Groceries),
	{reply, ok, NewGroceries}.
	

handle_cast({return, Item}, Groceries) ->
	NewGroceries = dict:update(Item, fun({Price, Number}) -> {Price, Number+1} end, {0,1}, Groceries),
	{noreply, NewGroceries};

handle_cast(close, Groceries) ->
	{stop, normal, ok, Groceries}.

terminate(normal, _Groceries) ->
	io:format("Shop: terminating. ~n"),
	ok.
handle_info(out_of_stock, Groceries) ->
	{noreply, Groceries};

handle_info({not_enough_money, _Money, price_is, _Price}, Groceries) ->
	{noreply, Groceries};
handle_info(Msg, Groceries) ->
	{noreply, Groceries}.

code_change(_OldVsn, Groceries, _Extra) ->
	{ok, Groceries}.