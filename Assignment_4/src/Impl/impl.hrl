-compile(export_all).
%%-------------------API--------------------
% loop arguments: 
% 	list for messages
%	dict of {PIDs, Clients}
%	list of filters installed
start() -> 
	Returned_Server = spawn(fun () ->
		process_flag(trap_exit,true),
		Server = spawn_link(fun() -> server_loop([], dict:new(), []) end),
		supervisor(Server)
	end),
	{ok, Returned_Server}.
	

connect(Server, Nick) ->
	request_reply_ref(Server, {connect, Nick}).

chat(Server, Cont) ->
	sendWithId(Server, {chat, Cont}).

history(Server) ->
	request_reply_ref(Server, history).

filter(Server, Method, P) ->
	request_reply_ref(Server, {filter, {Method, P}}).

plunk(Server, Nick) ->
	filter(Server, compose, fun(Cont) -> plunk_function(Nick, Cont) end).
censor(Server, Words) -> 
	filter(Server, compose, fun(Cont) -> censor_function(Words, Cont) end).
%------------Internal functions------------

supervisor(Server) -> 
	receive
		{'EXIT', Server, Reason} ->
			io:format("~p exited because of ~p~n", [Server,Reason]),
			New_Server = spawn_link(fun() -> server_loop([], dict:new(), []) end),
			supervisor(New_Server);
		{Client_Pid, Ref, Msg} -> 
			async(Server,{Client_Pid, Ref, Msg}),
			supervisor(Server);
		{Client_Pid, {chat, Msg}} ->
			async(Server, {Client_Pid, {chat, Msg}}),
			supervisor(Server)
	end.

request_reply_ref(Pid, Request) ->
	Ref = make_ref(),
	Pid ! {self(), Ref, Request},
	receive
		{Ref, Response} -> Response;
		{Ref, error, Response} ->
			{Ref,{server_error, Response}};
		Msg -> Msg
	after 3000 ->
		throw(request_timeout)
	end.

sendWithId(Pid, Cont) -> Pid ! {self(), Cont}.

async(Pid, Cont) -> Pid ! Cont.

server_loop(Messages, Clients, Filters) ->
	receive
		{From, Ref, {connect, Nick}} -> 
			Nicknames = [N || {_, {N, _}} <- dict:to_list(Clients)],
			case lists:member(Nick, Nicknames) of
				true -> 
					From ! {Ref, {error, Nick, is_taken}},
					server_loop(Messages, Clients, Filters);
				false -> 
					From ! {Ref, {ok, Ref}},
					server_loop(Messages,dict:store(From, {Nick,Ref}, Clients), Filters)
			end;
		{From, {chat, Msg}} -> 
			case dict:is_key(From, Clients) of
				true ->	
					{Nickname,_} = dict:fetch(From, Clients),
					broadcast(Clients, Msg, Nickname, Filters);
				false -> nothing
			end,
			server_loop([Msg |Messages], Clients, Filters);
		{From, Ref, history} ->
			case Messages of
				[] -> LatestMessages = [];
				_ ->LatestMessages = lists:sublist(Messages, 42)
			end,
			From ! {Ref, LatestMessages},
			server_loop(Messages, Clients, Filters);

		{From, Ref, {filter, {Method, P}}} ->			
			try
				case Method of
					compose -> 
						New_Filters = [P|Filters];
					replace ->
						New_Filters = [P]
				end,
				From ! {Ref, {ok, New_Filters}},
				server_loop(Messages, Clients, New_Filters)
			catch
				error:Reason -> 
					From ! {Ref, error, {unknown_method,Reason}},
					server_loop(Messages, Clients, Filters)		
			end
		
	end.
broadcast(Clients, Msg, Sender,Filters) ->
	% apply server filters to message, before broadcasting to all clients
	case apply_filters(Filters, {Msg, Sender}) of
		true -> 
			% map over the dict of clients to get their Pids and broadcast message
			dict:map(fun (Pid, {_,Ref}) -> async(Pid,{Ref, {Sender, Msg}}) end, Clients);
		false -> nothing
	end.
apply_filters(Filters, Cont) ->
	% map over the list of filters and apply the 
	Result = lists:map(fun(F) -> F(Cont) end,Filters),
	lists:all(fun(R) -> R =:= true end, Result).

% check if the Sender has the same name as the Nickname
plunk_function(Nickname, {_, Sender}) -> 
	if 
		Nickname =:= Sender -> false;
		Nickname =/= Sender ->  true
	end.

% check if any of the Words is a substring of Msg
censor_function(Words, {Msg, _}) -> 
	Result = lists:map(fun(Word) -> string:str(Msg, Word) end, Words),
	lists:all(fun(R) -> R =:= 0 end, Result).