-compile(export_all).
%-------------Client implementation for testing----------------
% State of clieant is (Ref, Messages), 
% where Messages are the messages the client received
% from the Server and Ref, which is unique for each client.
request_reply(Pid, Request) ->
	Pid ! {self(), Request},
	receive
		{Pid, Response} -> Response;
		{Pid, error, Response} ->
			{server_error, Response}
	after 3000 ->
		throw(request_timeout)
	end.

% spawn a new client process to use for testing
client_spawn() -> 
	spawn(fun () -> client_loop(nothing,[]) end).

% Takes as arguments, the Pid of the Client that we want
% to send a chat message to the Server
client_chat(Client, Server, Msg) -> 
	async(Client, {chat, Server, Msg}).

% Takes as arguments, the Pid of the client that we want
% to connect to the Server with the nickname Nick
client_connect(Client ,Server, Nick) -> 
	request_reply(Client, {connect, Server, Nick}).

% Take as argument the Pid of the Client to return 
% all the mesages it received
client_history(Client) ->
	request_reply(Client, history).

% Client loop, listens for chat, connect, broadcasted messages
% and history
client_loop(Ref, Messages) ->
	receive
		{chat, Server, Msg} -> 
			chat(Server,Msg),
			client_loop(Ref, Messages);
		{From, {connect, Server, Nick}} -> 
			case connect(Server, Nick) of
				{server_error, {Nick, is_taken}} -> 
					NewReference = Ref,
					From ! {self(), error, {Nick, is_taken}};
				{ok, Id} -> 
					NewReference = Id,
					From ! {self(), {ok, Id}};
				Msg->
					NewReference = Ref,
					io:format("DUMMY:~p  ~n", [Msg])
			end,
			client_loop(NewReference, Messages);
		{From, {ok, Id}} -> 
			From ! {ok, Id},
			client_loop(Ref, Messages);
		{Ref, {_, Msg}} ->
			client_loop(Ref, [Msg | Messages]);
		{From, history} ->
			case Messages of
				[] -> ReturnValue = [];
				_ -> ReturnValue = Messages
			end,
			From ! {self(), ReturnValue},
			client_loop(Ref, Messages)
	end.