% Advanced Programming
% Assignment 4: Chat with Erlang
% Filip Strycko: fnb946
% Sokratis Siozos - Drosos: dnb823

-module(erc).
-export([start/0,connect/2,chat/2,history/1, filter/3, plunk/2, censor/2]).
-include("Impl/impl.hrl").