module SubsParser (
    parseString,
    parseFile,
    ParseError
  ) where

import Parser.Impl (
    parseString,
    parseFile
  )
import Text.Parsec.Error