module Parser.Tests where

import Parser.Impl
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck as QC

import Control.Applicative (Alternative((<|>), empty, many, some))
import SubsAst
import Parser.Impl 

unitTests :: TestTree
unitTests = testGroup "Unit Tests"
    [ testCase "Testing ident parser.1" $
        (parseme parseIdent "_x0") @?=
            (Right "_x0"),
      testCase "Testing ident parser.2" $
        (parseme parseIdent "___") @?=
            (Right "___"),
      testCase "Testing ident parser.3" $
        (parseme parseIdent "variable") @?=
            (Right "variable"),
      testCase "Testing ident parser.4" $
        (parseme parseIdent "FOO_3bar_") @?=
            (Right "FOO_3bar_"),
      testCase "Testing parseNumber.1" $
        (parseme parseNumber "123") @?=
            (Right 123),
      testCase "Testing parseNumber.2" $
        (parseme parseNumber "  123") @?= 
            (Right 123),
      testCase "Testing parseNumber.3" $
        (parseme parseNumber "75432960") @?=
            (Right 75432960),
      testCase "Testing parseNumber.4" $
        (parseme parseNumber "  4562") @?=
            (Right 4562),
      testCase "Testing string parser.1" $
        (parseme subsstring "'foo'") @?=
            (Right ("foo")),
      testCase "Testing string parser.2" $
        (parseme subsstring "'ab'") @?=
            (Right ("ab")),
      testCase "Testing afterIdent parser.1" $
        (parseme (afterIdent "i") ".foo(1)") @?=
            (Right (Call "i.foo" [Number 1])),
      testCase "Testing afterIdent parser.2" $
        (parseme (afterIdent "i") ".foo(1,2)") @?=
            (Right (Call "i.foo" [Number 1, Number 2])),
      testCase "Testing expr1 parser.1" $
        (parseme expr1 "2") @?=
            (Right $ Number 2),
      testCase "Testing expr1 parser.2" $
        (parseme expr1 "true") @?=
            (Right TrueConst),
      testCase "Testing expr1 parser.3" $
        (parseme expr1 "false") @?= 
            (Right FalseConst),
      testCase "Testing expr1 parser.4" $
        (parseme expr1 "undefined") @?=
            (Right Undefined),
      testCase "Testing expr1 parser.5" $
        (parseme expr1 "x = 2") @?=
            (Right (Assign "x" (Number 2))),
      testCase "Testing expr1 parser.6" $
        (parseme expr1 "x=123+2") @?=
            (Right (Assign "x" (Call "+" [Number 123,Number 2]))),
      testCase "Testing expr1 parser.7 - recursion" $
        (parseme expr1 "x=1-2-3") @?=
            (Right (Assign "x" (Call "-" [Call "-" [Number 1,Number 2],Number 3]))),
      testCase "Testing expr1 parser.8" $
        (parseme expr1 "x=123+2") @?=
            (Right (Assign "x" (Call "+" [Number 123,Number 2]))),
      --- Black box tests
      testCase "Testing parseString squares.1" $
        (parseString "var squares = [ for (x of xs) x * x ];") @?=
            (Right (Prog [VarDecl "squares" (Just (Compr ("x",Var "xs",Nothing) (Call "*" [Var "x",Var "x"])))])),
      testCase "Testing parseString hundred.2" $
        (parseString "var hundred = [ for (i of [0]) for (x of xs)for (y of xs) i = i + 1 ];") @?=
            (Right (Prog [VarDecl "hundred" (Just (Compr ("i",Array [Number 0],Just (ArrayForCompr ("x",Var "xs",Just (ArrayForCompr ("y",Var "xs",Nothing))))) (Assign "i" (Call "+" [Var "i",Number 1]))))])),
      testCase "Testing parseString many_a.3" $
        (parseString "var many_a = [ for (x of xs) for (y of xs) 'a' ];") @?=
            (Right (Prog [VarDecl "many_a" (Just (Compr ("x",Var "xs",Just (ArrayForCompr ("y",Var "xs",Nothing))) (String "a")))])),
      testCase "Testing parseString evens" $
        (parseString "var evens = [ for (x of xs) if (x % 2 === 0) x ];") @?=
            (Right (Prog [VarDecl "evens" (Just (Compr ("x",Var "xs",Just (ArrayIf (Call "===" [Call "%" [Var "x",Number 2],Number 0]) Nothing)) (Var "x")))])),
      testCase "Testing operator precedences with parseString.1" $
        (parseString "var x = 1+2+3;") @?=
          (Right (Prog [VarDecl "x" (Just (Call "+" [Call "+" [Number 1,Number 2], Number 3]))])),
      testCase "Testing operator precedences with parseString.2" $
        (parseString "var x = 2+3+4*5;") @?=
            (Right (Prog [VarDecl "x" (Just (Call "+" [Call "+" [Number 2,Number 3],Call "*" [Number 4,Number 5]]))])),
      testCase "Testing operator precedences with parseString.3" $
        (parseString "var x = 1*(3+2);") @?=
          (Right (Prog [VarDecl "x" (Just (Call "*" [Number 1,Call "+" [Number 3,Number 2]]))])),
      testCase "Testing operator precedences with parseString.4" $
        (parseString "var x = (1%4) *(3+2);") @?=
          (Right (Prog [VarDecl "x" (Just (Call "*" [Call "%" [Number 1,Number 4],Call "+" [Number 3,Number 2]]))])),
      testCase "Testing comma constructor with parseString.5" $
        (parseString "var x= [1,2,3];") @?=
          (Right (Prog [VarDecl "x" (Just (Array [Number 1,Number 2,Number 3]))])),
      testCase "Testing comma constructor with parseString.6" $
        (parseString "var x= (1,2,3);") @?=
          (Right (Prog [VarDecl "x" (Just (Comma (Comma (Number 1) (Number 2)) (Number 3)))]))

    ]

propertyTests :: TestTree
propertyTests = testGroup "QuickCheck"
    [

    ]

tests :: TestTree
tests = testGroup "Tests" [propertyTests, unitTests]

main :: IO ()
main = defaultMain tests