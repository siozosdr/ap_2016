module Parser.Impl where

import SubsAst

import Control.Monad (void)

import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Error
import Text.Parsec.String
import Text.Parsec.Combinator


keywords :: [String]
keywords = ["if", "var", "true", "false", "undefined", "for", "of"]

parseProgram :: Parser Program
parseProgram = parseStms []

parseStms:: [Stm] ->Parser Program
parseStms a=
    try(do
        stm <- parseStm
        void $ cToken ';'
        parseStms (a++[stm])
    ) <|>
    (do
        stm <- parseStm
        void $ cToken ';'
        return $ Prog (a++[stm])
    )


parseStm::Parser Stm
parseStm =
    try(do
        void $ sToken "var "
        i<- parseIdent
        parseAssignOpt i

        )<|>
    try(do
        e <- expr
        return $ ExprAsStm e
        )


parseAssignOpt:: Ident -> Parser Stm
parseAssignOpt ident = do
            void $ cToken '='
            e <- expr1
            return $ VarDecl ident (Just e)
            <|> return (VarDecl ident Nothing)
        

expr1 :: Parser Expr
expr1 = do
    e1 <- expr1_1
    expr1' e1

expr1' :: Expr -> Parser Expr
expr1' e1 = do
    op <- sToken "==="
    e2 <- expr1_1
    let e = Call op [e1,e2]
    expr1' e
    --return (Call op [e1,e3]))
    <|> return e1

expr1_1 :: Parser Expr
expr1_1 = do
    e <- expr1_2
    expr1_1' e

expr1_1' :: Expr -> Parser Expr
expr1_1' e1 = (do
    op <- cToken '<'
    e2 <- expr1_2
    let e = Call op [e1,e2]
    expr1_1' e)
    <|> return e1

expr1_2 :: Parser Expr
expr1_2 = do
    e <- expr1_3
    expr1_2' e

expr1_2' :: Expr -> Parser Expr
expr1_2' e1 = (do
    op <- cToken '+' <|> cToken '-'
    e2 <- expr1_3
    let e = Call op [e1, e2]
    expr1_2' e)
    <|> return e1

expr1_3 :: Parser Expr
expr1_3 = do
    e <- expr1_4
    expr1_3' e

expr1_3' :: Expr -> Parser Expr
expr1_3' e1 = (do
    op <- cToken '*' <|> cToken '%'
    e2 <- expr1_4
    let e = Call op [e1,e2]
    expr1_3' e)
    <|> return e1

expr1_4 :: Parser Expr
expr1_4 = try(do
    n <- parseNumber
    return $ Number n
    )<|>
    try (do
        s <- subsstring
        return $ String s)<|>
    try (do
        void $ sToken "true"
        return TrueConst) <|>
    try (do
            void $ sToken "false"
            return FalseConst) <|>
    try (do
           void $ sToken "undefined"
           return Undefined) <|>
    try (do
            i <- parseIdent
            afterIdent i)<|>
    try listCompr<|>
    try listOfExprs<|>
    try(do
           void $ cToken '('
           e <- expr
           void $ cToken ')'
           return e)<|>
    try(do
            i <- parseIdent
            return $ Var i)

expr :: Parser Expr
expr = do
    e <- expr1
    expr' e

expr' :: Expr -> Parser Expr
expr' e1 = try(do
    void $ cToken ','
    e2 <- expr
    case e2 of
        (Comma e3 e3') -> expr' (Comma (Comma e1 e3) e3')
        _ -> expr' (Comma e1 e2) )
    <|> return e1

parseFor :: Parser String
parseFor = do
    void $ sToken "for"
    void $ cToken '('
    ident <- parseIdent
    void $ sToken "of"
    return ident

listCompr :: Parser Expr
listCompr = do
    void $ cToken '['
    ident <- parseFor
    exprEnt1 <- expr1
    void $ cToken ')'
    arrayCompr <- parseArrayCompr
    case arrayCompr of
        Nothing -> do
            exprEnt2 <- expr1
            void $ cToken ']'
            return $ Compr (ident, exprEnt1, Nothing) exprEnt2
        (Just a) -> do
            exprEnt2 <- expr1
            void $ cToken ']'
            return $ Compr (ident, exprEnt1, Just a) exprEnt2

parseArrayCompr :: Parser (Maybe ArrayCompr)
parseArrayCompr = try(do
        void $ sToken "if"
        e <- between (cToken '(') (cToken ')') expr1
        compr <- parseArrayCompr
        case compr of
            Nothing -> return $ Just $ ArrayIf e Nothing
            (Just a) -> return $ Just $ ArrayIf e (Just a)

    ) <|> (do
        ident <- parseFor
        e <- expr1
        void $ cToken ')'
        compr <- parseArrayCompr
        case compr of
            Nothing -> return $ Just $ ArrayForCompr (ident, e, Nothing)
            (Just a) -> return $ Just $ ArrayForCompr (ident, e, Just a)
    )<|> return Nothing


listOfExprs:: Parser Expr
listOfExprs = do
    void $ cToken '['
    e <- parseExprs
    void $ cToken ']'
    return e

afterIdent :: Ident -> Parser Expr
afterIdent i =
    try(do void $ cToken '='
           ee <- expr1
           return $ Assign i ee
        ) <|>
        funCall i

--------TODO: empty parantheses case
funCall :: Ident -> Parser Expr
funCall ident= try (do void $ char '.'
                       i <- parseIdent
                       funCall (ident++"."++i))
                       --return $ Call (ident) [f]) 
                <|> try(do
                        void $ cToken '('
                        (Array middle) <- parseExprs
                        void $ cToken ')'
                        return $ Call ident middle
                )<|> try(do 
                    void $ char '.'
                    i <- parseIdent
                    void $ cToken '('
                    void $ cToken ')'
                    return $ Call (ident++"."++i) []
                ) <|> (do
                    void $ cToken '('
                    void $ cToken ')'
                    return $ Call ident []

                    )

parseExprs :: Parser Expr
parseExprs = do
        e1 <- expr1
        x<-commaExprs e1
        return $ Array x
commaExprs:: Expr -> Parser [Expr]
commaExprs e1 = try(do
        void $ cToken ','
        e2 <- expr1
        s2 <- commaExprs e2
        return (e1:s2))<|>
    try(do
        void $ cToken ','
        e2 <- expr1
        return (e1: [e2])) <|>
        return [e1]

parseIdent:: Parser Ident
parseIdent = do
    c <- char '_' <|> letter
    rest <- many (alphaNum <|> char '_')
    let ident = c:rest
    if ident `elem` keywords then
        fail "Reserved variable name"
        else return ident

subsstring :: Parser String
subsstring = do
    s <- between open close middle
    if s `elem` keywords
        then fail "Reserved variable name"
        else return s
    where
        open = char '\''
        middle = many (alphaNum <|> char '!' 
            <|> char '@' <|> char '#' <|> char '$' <|> char ' ')
        close = char '\''

-- tries parsing - sign in case of negative
-- returns correct number based on case of sign
parseNumber :: Parser Int
parseNumber = do
    sign <- option "" (sToken "-")
    num <- many1 digit
    if length num > 8
    then
        fail "too big number"
    else
        case sign of
            "" -> return $ read num
            _ -> return $ read (sign++num)

parseFloat :: Parser Double
parseFloat = do
    number <- parseNumber
    o <- option "" (string ".")
    case o of
        "." -> do
            d1<- digit
            d2<- digit
            return $ read $ show number++"."++[d1,d2]
        _ -> fail "not a number"

-- eliminate spaces when parsing operators with 1 character
cToken :: Char -> Parser String
cToken c = try (spaces >> string [c] <* spaces)

-- eliminate spaces when parsing operators with more
-- than 1 characters
sToken :: String -> Parser String
sToken s = try (spaces >> string s <* spaces)

--helper
parseme :: Parser a -> String -> Either ParseError a
parseme parser = parse parser ""

parseString :: String -> Either ParseError Program
parseString s = case parseme parseProgram s of
    Left err -> Left err
    Right result -> Right result

parseFile :: FilePath -> IO (Either ParseError Program)
parseFile path = parseString <$> readFile path