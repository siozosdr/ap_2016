module Curves (
  Point,
  Curve,
  point,
  pointX,
  pointY,
  curve,
  connect,
  rotate,
  translate,
  reflect,
  bbox,
  width,
  height,
  toList,
  normalize
) where

import Text.Printf


instance Eq Point where
  Point (x,y) == Point (a,b) = (abs (x-a) < 0.01) && (abs (y-b) < 0.01)

instance Show Point where
    show (Point (x,y)) = "Point " ++ show x' ++ " " ++ show y'
        where   x' = fromIntegral (round (x * 1000) :: Integer) / 1000
                y' = fromIntegral (round (y * 1000) :: Integer) / 1000  

data Point = Point (Double, Double)
  --deriving Show

type Curve = [Point]

point :: (Double, Double) -> Point
point (x,y) = Point (x,y)

pointX :: Point -> Double
pointX point = x
 where Point (x,_) =  point

pointY :: Point -> Double
pointY point = y
 where Point (_,y) = point

curve :: Point -> [Point] -> Curve
--curve p [] = error "Curve has to consist from at least two points"
curve p c = p:c

connect :: Curve -> Curve -> Curve
connect c1 c2 = c1 ++ c2


rotatePoint :: Double -> Point -> Point
rotatePoint degree point = Point (x', y')
 where x' = x* cos radians - y*(sin radians)
       y' = y*(cos radians) + x*(sin radians)
       radians = degree*pi/180
       Point (x,y) = point

rotate :: Curve -> Double -> Curve
rotate curve degree= map (rotatePoint degree) curve

translatePoint :: Point -> Point -> Point
translatePoint point tranPoint = Point (x+a, y+b)
  where Point (x,y) = point
        Point (a,b) = tranPoint

translate :: Curve -> Point -> Curve
translate [] _ = []
translate curve tranPoint = map (translatePoint vector) curve
  where vector = Point (tranPointX-startPointX, tranPointY-startPointY)
        Point (startPointX, startPointY):rest = curve
        Point (tranPointX, tranPointY) = tranPoint


data Line = Vertical Double | Horizontal Double

reflectPoint :: Line -> Point -> Point
reflectPoint (Horizontal y) point = newPoint
  where Point (px, py) = point
        change = y-py
        newPoint = Point (px, y+change)
reflectPoint (Vertical x) point = newPoint
  where Point (px, py) = point
        change = x-px
        newPoint = Point (x+change, py)



reflect :: Curve -> Line -> Curve
reflect curve line = map (reflectPoint line) curve

maxY:: Curve -> Double
maxY [(Point (x,y))] = y
maxY ((Point (x1,y1)):(Point (x2,y2)):cs)
  | y1>y2 = maxY ((Point (x1,y1)):cs)
  | otherwise = maxY ((Point (x2,y2)):cs)

maxX:: Curve -> Double
maxX [(Point (x,y))] = x
maxX ((Point (x1,y1)):(Point (x2,y2)):cs)
  | x1>x2 = maxX ((Point (x1,y1)):cs)
  | otherwise = maxX ((Point (x2,y2)):cs)

minY:: Curve -> Double
minY [(Point (x,y))] = y
minY ((Point (x1,y1)):(Point (x2,y2)):cs)
  | y1<y2 = minY ((Point (x1,y1)):cs)
  | otherwise = minY ((Point (x2,y2)):cs)

minX:: Curve -> Double
minX [(Point (x,y))] = x
minX ((Point (x1,y1)):(Point (x2,y2)):cs)
  | x1<x2 = minX ((Point (x1,y1)):cs)
  | otherwise = minX ((Point (x2,y2)):cs)



 
bbox :: Curve -> (Point, Point)
bbox curve = (Point (minX curve, minY curve), Point(maxX curve, maxY curve))



--check!
width :: Curve -> Double
width curve = abs(minp-maxp)
 where minp = minY curve
       maxp = maxY curve

height :: Curve -> Double
height curve = abs(minp-maxp)
 where minp = minX curve
       maxp = maxX curve


toList :: Curve -> [Point]
toList c = c

normalize :: Curve -> Curve
normalize curve = map (translatePoint (Point (-x, -y))) curve
  where (Point (x, y), _) = bbox curve

toSVGCurve:: Curve -> String
toSVGCurve [(Point (_,_))] = ""
toSVGCurve curve = curveString ++ (toSVGCurve ((Point (x2,y2)):ps) )
  where points = toList curve 
        ((Point (x1,y1)):(Point (x2,y2)):ps) = points
        curveString = "<line style=\"stroke-width: 2px; stroke: black; fill:white\" x1='"++(printf "%.2f" x1)++"' x2='"++(printf "%.2f" x2)++"' y1='"++(printf "%.2f" y1)++"' y2='"++(printf "%.2f" y2)++"' />"


toSVG :: Curve -> String
toSVG curve = header++curves++footer
  where wstring = printf "%.0f" (width curve)
        hstring = printf "%.0f" (height curve)
        header = "<svg xmlns='http://www.w3.org/2000/svg' width='"++wstring++"px' height='"++hstring++"px' version='1.1'>\n<g>"
        footer = "</g></svg>"
        curves = toSVGCurve curve

toFile :: Curve -> FilePath -> IO ()
toFile curve path = writeFile path $ toSVG curve


hilbert :: Curve -> Curve
hilbert c = c0 `connect` c1 `connect` c2 `connect` c3
   where  w = width c
          h = height c
          p = 6

          ch = reflect c $ Vertical 0

          c0 = ch `rotate` (-90) `translate` point (w+p+w, h+p+h)
          c1 = c `translate` point (w+p+w, h)
          c2 = c
          c3 = ch `rotate` 90 `translate` point (0, h+p)

hlbrt = hilbert $ hilbert $ hilbert $ hilbert $ curve (point (0,0)) []

--TESTCURVE
p1 = point (123.4234,222.22)
p2 = point (199.999,522.222)
p3 = point (200,500)
p4 = point (0,600)
p5 = point (-100,700)
p6 = point (-200,500)

testCurve1 = curve p1 [p2,p3]
testCurve2 = curve p4 [p5,p6]

testCurve3 = connect testCurve1 testCurve2
rotated = rotate testCurve3 90

translatedCurve = translate testCurve1 (Point (10,10))

ref1 = reflectPoint (Horizontal 4) (Point (5,2))
ref2 = reflectPoint (Vertical 4) (Point (5,2))

normTC3 = bbox $ normalize testCurve3

