module CurvesTest where

import Curves

-- Create curves to work with
c1 = Curve (point (-2,2)) (map point [(-1,1), (0,0)])
c2 = Curve (point (0,1)) (map point [(1,2), (2,3)])

-- Test point function
testPoint :: Bool
testPoint = pointX (point (1,2.5)) == 1 && pointY (point (1,2.5)) == 2.5

-- Test for 'curve' function
testCurve :: Bool
testCurve = ch1 == a1 && ch2 == a2
    where 
        ch1 = curve (point (2,3)) [point (3,3)]
        a1 = Curve (point (2,3)) [point (3,3)]
        ch2 = curve (point (2,3)) []
        a2 = Curve (point (2,3)) []

-- Test for equality
testEq :: Bool
testEq = point (0,0) == point (0.009, 0.009) && point (0,0) /= point (0.01, 0.01)

-- Test for 'connect' function
testConnect :: Bool
testConnect = ch1 == a1
    where
        ch1 = connect c1 c2
        a1 = Curve (point (-2,2)) (map point [(-1,1), (0,0), (0,1), (1,2), (2,3)])

-- Test for 'rotate' function
testRotate :: Bool
testRotate = ch1 == a1 && ch2 == a2
    where
        ch1 = rotate c1 32
        a1 = Curve (point (-2.756, 0.636)) (map point [(-1.378, 0.318), (0,0)])
        ch2 = rotate c1 (-30)
        a2 = Curve (point (-0.732, 2.732)) (map point [(-0.366, 1.366), (0.0, 0.0)])

-- Test for 'translate' function
testTranslate :: Bool
testTranslate = ch1 == a1 && ch2 == a2
    where ch1 = translate c1 (point (1,1))
          a1 = Curve (point (1,1)) (map point [(2,0), (3,-1)])
          ch2 = translate c1 (point (-3,2))
          a2 = Curve (point (-3,2)) (map point [(-2,1), (-1,0)])

-- Test for 'reflect' function
testReflect :: Bool
testReflect = ch1 == a1 && ch2 == a2
    where
        ch1 = reflect c1 (Horizontal 2)
        a1 = Curve (point (-2,2)) (map point [(-1,3), (0,4)])
        ch2 = reflect c1 (Vertical 2)
        a2 = Curve (point (6,2)) (map point [(5,1), (4,0)])

-- Test for 'normalize function'
testNormalize :: Bool
testNormalize = ch1 == a1 && ch2 == a2
    where 
        ch1 = normalize c1
        a1 = Curve (point (2,2)) (map point [(1,1), (0,0)])
        ch2 = normalize c2
        a2 = Curve (point (0,1)) (map point [(1,2), (2,3)])

-- Test for 'toList' function
testToList :: Bool
testToList = ch == a
    where 
        ch = toList c1
        a = map point [(-2,2), (-1,1), (0,0)]

-- Test for 'bbox' function
testBBox :: Bool
testBBox = ch == a
    where
        ch = bbox c1
        a = (point(-2, 0), point (0,2))

-- Test for 'width' function
testWidth :: Bool
testWidth = ch == a
    where
        ch = width c1
        a = 2.0

-- Test for 'height' function
testHeight :: Bool
testHeight = ch == a
    where 
        ch = height c1
        a = 2.0

testHilbert = toFile (hilbert $ hilbert $ hilbert $ hilbert $ curve (point (0,0)) []) "tosvg/hilbert.svg"  

runTests :: IO ()
runTests = do
    print testPoint
    print testCurve
    print testEq
    print testConnect
    print testRotate
    print testTranslate
    print testReflect
    print testNormalize
    print testToList
    print testBBox
    print testWidth
    print testHeight
    testHilbert
    print "Hilbert curves saved in 'tosvg' directory."