module Curves( 
    Line(Horizontal, Vertical),
    Curve(Curve),
    Point,
    point,
    pointX,
    pointY,
    curve,
    connect,
    rotate,
    translate,
    reflect,
    bbox,
    width,
    height,
    toList,
    normalize,
    toSVG,
    toFile,
    hilbert
    ) where


import Text.Printf

instance Eq Point where
  Point (x,y) == Point (a,b) = (abs(x-a) < 0.01 ) && (abs(y-b) < 0.01)

instance Eq Curve where
  Curve p1 pl1 == Curve p2 pl2 = (p1==p2) && (pl1 == pl2)
  
data Point = Point (Double, Double)
data Curve = Curve Point [Point]

-- customize Show to round up to 3 decimals when showing Points
-- mainly for debugging
instance Show Point where
    show (Point (x,y)) = "Point " ++ show x' ++ " " ++ show y'
        where   x' = fromIntegral (round (x * 1000) :: Integer) / 1000
                y' = fromIntegral (round (y * 1000) :: Integer) / 1000  

point :: (Double, Double) -> Point
point (x, y) = Point (x,y)

pointX :: Point -> Double
pointX (Point (x, _)) = x 

pointY :: Point -> Double
pointY (Point (_, y)) = y

curve :: Point -> [Point] -> Curve
curve p ps = Curve p ps

connect :: Curve -> Curve -> Curve
connect (Curve sp1 pl1) (Curve sp2 pl2) = Curve sp1 (pl1 ++ ([sp2] ++ pl2))

rotate :: Curve -> Double -> Curve
rotate (Curve sp pl) deg = Curve (rotatePoint deg sp)(map(rotatePoint deg) pl)

-- helper function for rotate
-- rotates a single point based on a double d
rotatePoint :: Double -> Point -> Point
rotatePoint deg (Point (x, y)) = Point (x', y')
  -- convertion of degrees to radians
  where 
    r  = deg * pi / 180
    x' = x * cos r - y * sin r
    y' = y * cos r + x * sin r 

translate :: Curve -> Point -> Curve
--translate (Curve []) _ = (Curve [])
translate (Curve (Point (sx, sy)) pl) (Point (px, py)) = Curve (translatePoint tv (Point (sx, sy))) (map(translatePoint tv) pl)
  where 
    --translation vector
    tv = Point ((px - sx), (py - sy))

-- helper function for translate, translates a single point
-- tx,ty is the translation vector found by subtracting the starting point
-- from the translation point
translatePoint :: Point -> Point -> Point
translatePoint (Point (tx, ty)) (Point (x, y)) = Point ((x+tx), (y+ty))


data Line = Vertical Double | Horizontal Double
reflect :: Curve -> Line -> Curve
reflect (Curve sp pl) l = Curve (reflectPoint l sp)(map(reflectPoint l) pl)

--helper function for reflect
reflectPoint :: Line -> Point -> Point
reflectPoint (Vertical d) (Point (x, y)) = Point ((x - 2*(x - d)), y)
reflectPoint (Horizontal d) (Point (x, y)) = Point (x, (y - 2*(y - d)))

bbox :: Curve -> (Point, Point)
bbox (Curve p pl) = (minbbox pl p, maxbbox pl p)
  where
    --helper function to find point with minimum coordinates
    minbbox :: [Point] -> Point -> Point
    minbbox pl' _ = foldl (flip minPoint) p pl'
      where 
        minPoint :: Point -> Point -> Point
        minPoint (Point (x1, y1)) (Point (x2, y2)) = Point ((minCoord x1 x2), (minCoord y1 y2))
          where
            minCoord :: Double -> Double -> Double
            minCoord x y = if x > y then y else x
    maxbbox :: [Point] -> Point -> Point
    maxbbox pl'' _ = foldl (flip maxPoint) p pl''
      where
        -- helper function to find point with maximum corrdinates
        maxPoint :: Point -> Point -> Point
        maxPoint (Point (x1, y1)) (Point (x2, y2)) = Point ((maxCoord x1 x2), (maxCoord y1 y2))
          where
            maxCoord :: Double -> Double -> Double
            maxCoord x y = if x > y then x else y
width :: Curve -> Double
width c = abs(x1 - x2)
    where
        (Point (x1, _), Point (x2, _)) = bbox c

height :: Curve -> Double
height c = abs(y1 - y2)
    where
        (Point (_, y1), Point (_, y2)) = bbox c

toList :: Curve -> [Point]
toList (Curve p pl) = p:pl

normalize :: Curve -> Curve
normalize (Curve p pl) = Curve (normalizePoint p) (map normalizePoint pl)

normalizePoint :: Point -> Point
normalizePoint (Point (x,y)) = Point $ (abs x, abs y)

toSVG :: Curve -> String
toSVG c = let
  w = (ceiling . width  $ c) :: Integer
  h = (ceiling . height  $ c) :: Integer
  header = printf "<svg xmlns = \"http://www.w3.org/2000/svg\" \n\t width=\"%dpx\" height=\"%dpx\" version=\"1.1\"> \n<g>\n" w h
  listOfPoints = toList c
  linesSVG = listToLines Nothing listOfPoints
  footer = printf "</g> \n</svg>"
  in header ++ linesSVG ++ footer
    where
      -- helper function to produce the lines
      -- Use of Maybe Point type to ensure that the first argument of
      -- the function will always have either Just one Point or 
      -- no Points, so that we will always be able to have pairs of
      -- points to print.
      listToLines :: Maybe Point -> [Point] -> String
      listToLines _ [] = ""
      listToLines Nothing (ph:pl) = listToLines (Just ph) pl
      listToLines (Just (Point (x, y))) (Point (hx, hy): pl) = printf "<line style=\"stroke-width: 2px; stroke: black; fill:white\" \n\t x1=\"%.2f\" x2=\"%.2f\" y1=\"%.2f\" y2=\"%.2f\" /> \n" x hx y hy ++ listToLines (Just (Point (hx, hy))) pl

toFile :: Curve -> FilePath -> IO ()
toFile c f = writeFile f (toSVG c) 


hilbert :: Curve -> Curve
hilbert c = c0 `connect` c1 `connect` c2 `connect` c3
   where  w = width c
          h = height c
          p = 6

          ch = reflect c $ Vertical 0

          c0 = ch `rotate` (-90) `translate` point (w+p+w, h+p+h)
          c1 = c `translate` point (w+p+w, h)
          c2 = c
          c3 = ch `rotate` 90 `translate` point (0, h+p)
