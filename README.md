# README #

This repo contains the assignments took in the Advanced Programming course in the MSc. in Computer Science program of DIKU. All the implementations are in Haskell, Prolog and Erlang.

## Assignment 0 ##
The objective of this assignment is to gain hands-on programming experience with Haskell.

The goal is to implement a library, Curves, for working with piecewise linear curves in the plane (based on an idea by Hansen and Rischel, who in turn got the idea from Maarten M. Fokkinga).

Implementation in Hakell

## Assignment 1 ##
This assignment is about implementing an interpreter for a conservative subset of Mozilla’s
JavaScript implementation, which we will call SubScript.
It is not part of this assignment to implement a parser for SubScript. The task is to
implement a SubScript interpreter, which given a SubScript abstract syntax tree either
yields a value or an error.

Implementation in Haskell

## Assignment 2 ##

This assignment is about writing a parser for the language SubScript.

Implementation in Haskell

## Assignment 3 ##
The objective of this assignment is to gain hands-on declarative programming experience with Prolog. 
This assignment is about analysing the social relations in a social network called “FacedIn”.

## Assignment 4 ##
This assignment is about implementing a relay chat server in Erlang (ERC). A relay chat server consists of a central server that clients can connect to, and then send messages which are relayed (broadcast) to other connected clients. An often seen problem with relay chat servers is that when the number of users goes up, various mechanisms are needed for reducing the noise (the number of messages) in the network, the most basic one is the notion of filters.

The server will keep track of which clients are connected, and for each client, possibly, a filter. When a client sends a message, it is relayed to other connected clients. Each client is registered with a nickname.

A message is a pair {Nick, Cont} where Nick is the nickname of the client who sent the message, and Cont is the content of the message.

## Assignment 5 ##
The objective of this assignment is to gain hands-on programming experience with Erlang and to get an understanding of how to implement map-reduce algorithms.

The goal is to implement a simple map-reduce library, using the OTP framework. Then use the map-reduce library to process the musiXmatch dataset (mxm), the official collection of lyrics from the Million Song Dataset.

## Exam ##
The objective of the exam is to implement a parser and an interpreter for APMake. 
In addition, it is required that a build-system called "Pirate" alternative to Ninja. It is about
making a library that takes care of handling concurrent command invocations and orchestrating the execution of build plans.