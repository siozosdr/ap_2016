import Prelude hiding (lookup)
import Data.Map(fromList,lookup)
import Data.List.Split

letCode =   
    [("A",".-")  
    ,("B","-...")  
    ,("C","-.-.")  
    ,("D","-..")  
    ,("E",".")
    ,("F","..-.")
    ,("G","--.")
    ,("H","....")
    ,("I","..")
    ,("J",".---")
    ,("K","-.-")
    ,("L",".-..")
    ,("M","--")
    ,("N","-.")
    ,("O","---")
    ,("P",".--.")
    ,("Q","--.-")
    ,("R",".-.")
    ,("S","...")
    ,("T","-")
    ,("U","..-")
    ,("V","...-")
    ,("W",".--")
    ,("X","-..-")
    ,("Y","-.--")
    ,("Z","--..")  
    ]  

codeLet = Prelude.map (\(x,y) -> (y,x)) letCode

letCodeMap = fromList letCode
codeLetMap = fromList codeLet

toDecode = "...|---|..-.|..|.-"



getMorse :: String -> Maybe String
getMorse letter = lookup letter letCodeMap

getLetter :: String -> Maybe String
getLetter code = lookup code codeLetMap

unwrap:: Maybe String -> String
unwrap (Just x) = x
unwrap Nothing = error "not maybe"

translateDecode :: String -> [Maybe String]
translateDecode x =  map getLetter $ splitOn "|" x

writeout :: [Maybe String]-> String
writeout x = foldl (++) "" $ map unwrap x


decode = writeout.translateDecode

--decode x =  Prelude.map getLetter (split (=='|') toDecode)
