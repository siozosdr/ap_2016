


g([person(ralf, [susan, ken]),
 person(ken, [ralf]),
 person(susan, [reed, jessica, jen, ralf,filip]),
 person(reed, [tony, jessica]),
 person(jessica, [jen]),
 person(tony, []),
 person(jen, [susan, jessica, tony, filip]),
%mine
 person(filip,[jen,susan])
 ]).


/*
FaceIn network.
Filip Strycko: fnb946
Sokratis Siozos-Drosos: dnb823

Note: numbers in parentheses next 
to variables at the comment
 mean the number of parameter for 
 specific function (e.g. follows(G,X,Y) - "G (1), X (2), Y (3)").
 */


eq(X,X).

is_member(X,[X]).
is_member(X, [H|T]):- 
	eq(X,H);
	is_member(X,T).

%isNot_member(X, []).
%isNot_member(X, [H|T]) :-
	
% True, if Y (param 3) is in friendlist of X (param 2).
follows(G,X,Y):- 
	is_member(person(X,Friends),G),
	is_member(Y,Friends).

% True, if X (param 2) follows Y (param 3) and Y follows X.
buddies(G,X,Y):-
	follows(G,X,Y),
	follows(G,Y,X).

% True, if X (param 2) is buddy with everybody in a list (3).
buddiesList(_,_,[]).
buddiesList(G,X,[H|T]):-
	buddies(G,X,H),
	buddiesList(G,X,T).

has_duplicates([H|T]):-
	is_member(H,T)


% True, if everybody in a list are buddies.
clique(_, []).
clique(G, [H|T]):-
	buddiesList(G, H, T),
	clique(G, T).

% Result (3) is a list of people without X (2) in network G (1).
peopleExcept(G,X,Result):-
	listOfPeople(G,Friends),
	select_(X,Friends,Result).


%  Result (2) is a list of names of people in network G (1).
listOfPeople([],[]).
listOfPeople([person(Name,_)|Tail],[Name|List]):-
	listOfPeople(Tail,List).


% selects and element from a list and returns
% a new list without the element.
select_(E, [E|T], T).
select_(E, [Y|T], [Y|Result]) :-
	select_(E,T, Result).

% True, if X (2) transitively admires Z (3) in graph G (1).
admires(G,X,Z):- follows(G,X,Z).
admires(G,X,Z):-
	select_(person(X,F),G,NG),
	% TransP gets all the possible values (friends) of list F.
	is_member(TransP, F),
	admires(NG,TransP,Z).

% True, if X (2) is transitively admiring everyone in graph G (1) without X.
admirer(G, X):-
	peopleExcept(G,X,People),
	admirerHelper(G,X,People).

% True, if X (2) admires everyone in list L (3).
admirerHelper(_,_,[]).
admirerHelper(G,X, [H|RPeople]):-
	admires(G,X,H),
	admirerHelper(G,X,RPeople).

% True, if X (2) is transitively admired by everyone in graph G (1) without X.
idol(G, X):-
	peopleExcept(G, X, People),
	idolHelper(G,X,People).

% True, if X (2) is transitively admired by everyone in list L (3)
idolHelper(_,_,[]).
idolHelper(G,X, [H|RPeople]):-
	admires(G,H,X),
	idolHelper(G, X, RPeople).






