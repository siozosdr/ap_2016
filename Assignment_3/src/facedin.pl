/*
FaceIn network.
Filip Strycko: fnb946
Sokratis Siozos-Drosos: dnb823

Note: numbers in parentheses next 
to variables at the comments
 mean the number of parameter for the
 specific function (e.g. follows(G,X,Y) - "G (1), X (2), Y (3)").
 */

%%---------a------------------
% True, if X (param 2) follows Y (param 3) and Y follows X.
buddies(G,X,Y):-
	follows(G,X,Y),
	follows(G,Y,X).

%%---------b------------------

%checks if X is buddy with everybody from the List
buddiesWithAll(G, X, [Y]):-
	buddies(G, X, Y).
buddiesWithAll(G, X, [Y | FL]):-
	buddies(G, X, Y),
	select_(person(Y,_),G,NG),
	buddiesWithAll(NG, X, FL).


%if just two people in a clique, they are buddies
clique(G, [H1,H2]) :-
	buddies(G,H1,H2).	


clique(G, [H|T]) :-
	% Checks if H is friend with everybody in T
	buddiesWithAll(G,H,T),
	% Selecting person appearing as the head of the clique
	% from G and creating new NG without the person. 
	select_(person(H,_),G,NG),
	% Recursion with the new NG and clique list, both without head H.
	clique(NG, T).



%%---------c------------------
% True, if X (2) is transitively admiring everyone in graph G (1) without X.
admirer(G, X):-
	peopleExcept(G,X,People),
	admirerHelper(G,X,People).

% True, if X (2) transitively admires Z (3) in graph G (1).
admires(G,X,Z):- follows(G,X,Z).
admires(G,X,Z):-
	select_(person(X,F),G,NG),
	% TransP gets all the possible values (friends) of list F.
	is_member(TransP, F),
	admires(NG,TransP,Z).

% True, if X (2) admires everyone in list L (3).
admirerHelper(_,_,[]).
admirerHelper(G,X, [H|RPeople]):-
	admires(G,X,H),
	admirerHelper(G,X,RPeople).

%%---------d------------------
% True, if X (2) is transitively admired by everyone in graph G (1) without X.
idol(G, X):-
	peopleExcept(G, X, People),
	idolHelper(G,X,People).

% True, if X (2) is transitively admired by everyone in list L (3)
idolHelper(_,_,[]).
idolHelper(G,X, [H|RPeople]):-
	admires(G,H,X),
	idolHelper(G, X, RPeople).

%%---------e------------------
% Base case true, becasue there is a path from person X to person X
ispath(_, X, X, [X]).

% right arrow
ispath(G, X, Y, [X,'->',TransP|P]):- 
	select_(person(X,F),G,NG),
	% TransP is all friends of X
	is_member(TransP, F),
	ispath(NG, TransP, Y, [TransP|P]).

%left arrow
ispath(G, X, Y, [X,'<-',TransP|P]):- 
	% TransP is all possible followers of X
	follows(G,TransP,X),
	select_(person(X,_),G,NG),
	ispath(NG, TransP, Y, [TransP|P]).

/*
	-----------------Helper functions-----------
*/

% Result (3) is a list of people without X (2) in network G (1).
peopleExcept(G,X,Result):-
	listOfPeople(G,Friends),
	select_(X,Friends,Result).

%  Result (2) is a list of names of people in network G (1).
listOfPeople([],[]).
listOfPeople([person(Name,_)|Tail],[Name|List]):-
	listOfPeople(Tail,List).

% selects and element from a list and returns
% a new list without the element.
select_(E, [E|T], T).
select_(E, [Y|T], [Y|Result]) :-
	select_(E,T, Result).
% True, if Y (param 3) is in friendlist of X (param 2).
follows(G,X,Y):- 
	is_member(person(X,Friends),G),
	is_member(Y,Friends).

is_member(X, [_|T]):- 
	is_member(X,T).
is_member(X,[X|_]).