:- include('facedin.pl').

g0([person(ralf, [susan, ken]),
 person(ken, [ralf]),
 person(susan, [reed, jessica, jen, ralf,filip]),
 person(reed, [tony, jessica]),
 person(jessica, [jen]),
 person(tony, []),
 person(jen, [susan, jessica, tony, filip]),
 person(filip,[jen,susan])
 ]).

testgraph([
  person(filip, [sokratis, susan]),
  person(sokratis, [filip, susan]),
  person(susan, [jen]),
  person(jen, [])
]).

%% a star graph has one person that is on everyone's friendlist,
%% and this one person has everyone on his friendlist.
stargraph([
  person(person1, [starperson, person2]),
  person(person2, [starperson, person3]),
  person(person3, [starperson, person4]),
  person(person4, [starperson, person5]),
  person(person5, [starperson, person1]),
  person(starperson, [person1, person2, person3, person4, person5])
]).

%% Testing goodfriends function
t0(_):-
  testgraph(G),
  buddies(G, filip, sokratis).

%% Testing clique
t1(_):-
  testgraph(G),
  clique(G, [filip, sokratis]).

%% Testing wannabe
t2(_):-
  testgraph(G),
  admirer(G, filip).

%% Testing idol
t3(_):-
  testgraph(G),
  idol(G, jen).

t4(_):-
  testgraph(G),
  ispath(G, filip, jen, [filip, ->, sokratis, ->, susan, ->, jen]).

t5(_) :-
	stargraph(G),
	buddies(G, person1, starperson),
	buddies(G, person2, starperson),
	buddies(G, person2, starperson),
	buddies(G, person3, starperson),
	buddies(G, person4, starperson),
	buddies(G, person5, starperson).
t6(_) :-
	stargraph(G),
	clique(G, [starperson, person2]).

t7(_) :-
	stargraph(G),
	admirer(G, person1).
t8(_) :-
	stargraph(G),
	admirer(G, person2).
t9(_) :-
	stargraph(G),
	admirer(G, person5).
t10(_):-
	g0(G),
	admirer(G, ken).
t11(_) :-
	g0(G),
	not(admirer(G, tony)).
t12(_) :-
	g0(G),
	not(idol(G, ralf)).
t13(_) :-
	g0(G),
	idol(G, tony).
t14(_) :-
	g0(G),
	ispath(G, ken, reed, [ken, '->', ralf, '->', susan, '->', reed]).
t15(_) :-
	g0(G),
	ispath(G, ralf, jen, [ralf, '->', susan, '<-', jen]).
t16(_) :-
	stargraph(G),
	ispath(G, person1, person5, [person1, '->', person2, '->', person3, '->', person4, '->', person5]).
t17(_) :-
	testgraph(G),
	not(ispath(G, jen, filip, [jen, '->', susan, '->', filip])).
t18(_) :-
	g0(G),
	not(ispath(G, jen, tony, [jen, '<-', tony])).
t19(_) :-
	g0(G),
	clique(G, [ken, ralf]).
t20(_) :-
	g0(G),
	clique(G, [ralf, susan]).
t21(_) :-
	g0(G),
	clique(G, [susan, jen]).
t22(_) :-
	g0(G),
	not(clique(G, [ken, ralf, reed])).
testrunner(_):-
  t0(asdf),
  t1(asdf),
  t2(asdf),
  t3(asdf),
  t4(adsf),
  t5(weqr),
  t6(qwer),
  t7(wert),
  t8(qwert),
  t9(qwer),
  t10(qwet),
  t11(qwert),
  t12(qwe),
  t13(qwe),
  t14(qwetry),
  t15(qwerf),
  t16(sdfsd),
  t17(qwe),
  t18(asd),
  t19(asd),
  t20(asd),
  t21(asd),
  t22(asd),!.
