/*
	Assignment 3 : Analysing Facedin
	Filip Strycko : fnb946
	Sokratis Siozos - Drosos : dnb823

*/

g0([person(ralf, [susan, ken]),
 person(ken, [ralf]),
 person(susan, [reed, jessica, jen, ralf, sokratis]),
 person(reed, [tony, jessica]),
 person(jessica, [jen]),
 person(tony, []),
 person(jen, [susan, jessica, tony, sokratis]),
 person(sokratis, [jen, susan])]).

buddies(G, X, Y) :-
	getPerson(G, X, PersonX),
	getPerson(G, Y, PersonY),
	hasfriend(PersonX, Y),
	hasfriend(PersonY, X).

clique(_, []).
clique(G, [H|T]):-
	buddiesWith(G, H, T),
	clique(G, T).

admires(G,X,Z):- follows(G,X,Z).
admires(G,X,Z):-
	select_(person(X,F),G,NG),
	is_member(TransP, F),
	admires(NG,TransP,Z).


admirer(G, X):-
	friendsInGraphExcept(G,X,People),
	admirerHelper(G,X,People).

admirerHelper(G,X, [H|RPeople]):-
	admires(G,X,H);
	admirerHelper(G,X,RPeople).
/*
 admirer(G, Admirer) :-
 	friendsInGraphExcept(G, Admirer, Friends),
 	admirer_helper(G, Admirer, Friends).
*/
 idol(G, Idol) :-
 	friendsInGraphExcept(G, Idol, Friends),
 	idol_helper(G, Idol, Friends).
/*
	No person occurs more than once in the list
	e.g.Susan to jen path: 
		[susan,->,reed,->,tony,<-,jen]
*/
 ispath(G, X, Y, P).

 /*
	-----------------Helper functions-------------------
 */
idol_helper(_, _, []).
idol_helper(G, Idol, [Friend |RestOfFriends]) :-
	% check if there are only idol åpaths to that idol from that friend
	idol_path(G, Idol, Friend, _),
	% call the helper which checks
	idol_helper(G, Idol, RestOfFriends).

idol_path(_, X, X, [X]).
idol_path(G, X, Y, [X, '<-', A |Path]) :-
	getPerson(G, A, PersonA),
	hasfriend(PersonA, X),
	select_(PersonA, G, NewG),
	idol_path(NewG, A, Y, [A |Path]).

/*
	if there are no more friends to check, we return true
	because in this closed worlds it holds true.
*/
admirer_helper(_, _, []).
admirer_helper(G, Admirer, [Friend |RestOfFriends]) :-
	% find path from admirer to friend
	admirer_path(G, Admirer, Friend, _),
	%repeat
	admirer_helper(G, Admirer, RestOfFriends).

/*
	admirer_path finds only path to the right, meaning that
	it only needs to find friends of friends.
*/
admirer_path(_, X, X, [X]).
admirer_path(G, X, Y, [X, '->', A | Path]) :-
	getPerson(G, X, PersonX),
	hasfriend(PersonX, A),
	select_(PersonX, G, NewG),
	admirer_path(NewG, A, Y, [A|P]).

/*
	return list of friends from the graph G
	withouth friend X.
*/
friendsInGraphExcept(G, X, Result) :-
	friendsInGraph(G, Friends),
	select_(X, Friends, Result).

% return all the friends in the graph
friendsInGraph([], []).
friendsInGraph([person(Name, _) | Tail], [Name |Result]) :-
	friendsInGraph(Tail, Result).
% check if friend is buddy with all the friends
buddiesWith(_, _, []).
buddiesWith(G, FriendOne, [FriendTwo |FriendList]) :-
	buddies(G, FriendOne, FriendTwo),
	buddiesWith(G, FriendOne, FriendList).

% Get person from graph
getPerson([person(Name, Friends)| T], Name, person(Name, Friends)).
getPerson([_|T], Name, Result) :-
	getPerson(T, Name, Result).

follows(G,X,Y):- 
	getPerson(G, X, PersonOne),
	hasfriend(PersonOne,Y).

% checks for friend Y in friendlist of X
hasfriend(person(X, Friends), Y) :-
	is_member(Y,Friends).

is_member(X, [X]).
is_member(X, [H |T]) :-
	eq(X, H);
	is_member(X, T).

% selects an element from a list and returns
% a new list without the element
select_(E, [E |T], T).
select_(E, [K |T], [K|Result]) :-
	select_(E, T, Result).
% returns true if list has at least 2 elements
atLeastTwoElements([_,_|_]).

eq(X,X).