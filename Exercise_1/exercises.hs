import Control.Monad

--f :: Float -> Float
--f x = x

--ft :: Float -> (Float, String)
--ft f = (f, "")

newtype Trace a = T (a, String)
 deriving (Show)

instance Functor Trace where
   fmap f m = m >>= \a -> return $ f a

-- f:: a -> a
--fmap f (T (a,str)) = T $ (f a, str)
--fmap f (T (a, str)) = let x' = f a
--in (T (x', str)) 

instance Applicative Trace where
    pure = return
    --x1 = T (3, "asd") -- *
    --x2 = T (3+, "something else") -- * -> *

    --(T (a, str)) <*> something = fmap a something
    --(Just f) <*> something = fmap f something
    (<*>) = ap


instance Monad Trace where
    --(>>=) :: Trace a -> (a -> Trace b) -> Trace b
    (T (p,str)) >>= f = let (T (a,str')) = f p
                        in T (a, str++str')
    
    --return :: a -> Trace a
    return x = T (x,"")



traceable :: String -> (t -> a) -> t -> Trace a
traceable name f = \x -> T(f x, name ++" called.")

doStuff = do
    let begin = traceable "beginning w 0" (id)
    let beginVal = begin 1
    beginVal >>= (\x -> T (x*2, "multiplied by two"))
    --x
    -- QUESTION: why does "print "



--MULTIVALUED FUNCTIONS
data Complex = Complex Float

newtype Multivalued a = MV [a]

instance Applicative Multivalued where
    pure = return
    (<*>) = ap

instance Functor Multivalued where
    fmap f m = m >>= \a -> return $ f a

instance Monad Multivalued where
    -- (>>=) :: Multivalued a -> (a -> Multivalued b) -> Multivalued b

    --f :: a -> Multivalued b

    (MV p) >>= f = let nr = map f p
                       uw = map (\(MV x) -> x) nr
                       in MV $ concat $ uw
-- return :: a -> Multivalued a
    return x = MV [x]
                       -- COOL!!
--    (MV xs) >>= f = do
--                  let x1 = map f xs
--                  let uw = map (\(MV x) -> x) x1
--                  return $ concat $ concat uw




                  --[MV [1], MV [2]] -> MV [1,2]
    	--let nr = map f xs
          --          in nr

    

--sqrtC, cbrtC, sixthrootC :: Complex Float -> Multivalued (Complex Float)



--[1,2,3] >>= \n -> [sqrtC, cbrtC, sithrootC] >>= \ch -> return (n,ch)

