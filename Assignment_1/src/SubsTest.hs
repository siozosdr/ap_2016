module SubsTest where

import SubsAst
import SubsInterpreter

import Control.Monad(forM_)
import Data.List(intercalate)
import qualified Data.Map as Map
import System.Environment(getArgs)



-- | nice display of JavaScript values
nice :: Value -> String
nice (IntVal v) = show v
nice TrueVal = "true"
nice FalseVal = "false"
nice (StringVal s) = show s
nice UndefinedVal = "undefined"
nice (ArrayVal vs) = "["++ intercalate ", " (map nice vs) ++"]"



-- Create variable declarations
var1 = VarDecl "x" (Just (Number 42))
var2 = VarDecl "z" (Just (Var "x"))
var3 = VarDecl "a" (Just (Call "+" [Var "x", Number 2]))
var4 = VarDecl "myArray" (Just (Array [Number 3, Number 31]))

var22 = VarDecl "w" (Nothing)
var44 = VarDecl "myArray1" (Just (Array [Number 3, Number 31, String "xasd"]))
var444 = VarDecl "myArray2" (Just (Array [Number 3, Number 31, Var "x"]))

var6 = VarDecl "ss" (Just(Call "===" [Var "x",Var "z"]))
var66 = VarDecl "ss" (Just(Call "===" [Var "x",Var "z", String "asd"]))
var666 = VarDecl "ss" (Just(Call "===" [Var "x"]))


var7 = VarDecl "ss" (Just(Call "<" [Var "x",Number 49]))
var77 = VarDecl "ss" (Just(Call "<" [Var "x",String "asd"]))
var777 = VarDecl "ss" (Just(Call "<" [Var "x"]))
var7777 = VarDecl "ss" (Just(Call "<" [Var "x",Var "z", Var "a"]))


var8 = VarDecl "ss" (Just(Call "-" [Var "x",Var "z", Var "a"]))
var88 = VarDecl "ss" (Just(Call "-" [Var "x",String "asd"]))
var888 = VarDecl "ss" (Just(Call "-" [Var "x"]))

var9 = VarDecl "ss" (Just(Call "+" [Var "x",Var "z", Var "a"]))
var99 = VarDecl "ss" (Just(Call "+" [Var "x",Var "z", String "asd"]))
var999 = VarDecl "ss" (Just(Call "+" [Var "x"]))

var10 = VarDecl "ss" (Just(Call "*" [Var "x",Var "z", Var "a"]))
var100 = VarDecl "ss" (Just(Call "*" [Var "x",String "z"]))
var1000 = VarDecl "ss" (Just(Call "*" [Var "x"]))

var11 = VarDecl "ss" (Just(Call "%" [Number 12, Number 33, Number 7]))
var111 = VarDecl "ss" (Just(Call "%" [Number 12, Number 33, String "asd"]))
var1111 = VarDecl "ss" (Just(Call "%" [Number 12]))

var_comp = VarDecl "squares" (Just (Compr ("x",Var "xs", Nothing) (Call "*" [Var "x",Var "x"])))
var_xs = VarDecl "xs" (Just (Array [Number 0, Number 1, Number 2, Number 3]))

-- Test programs

prog1_g = [var1, var2, var3, var4] -- variables
prog1_b1 = [var1, var2, var3, var22, var4] -- variables
prog1_b2 = [var1, var2, var3, var4,var44] -- variables
prog1_b3 = [var1, var2, var3, var4, var444] -- variables

prog2_g = [var1, var2, var3,var6] -- equality 
prog2_b1 = [var1, var2, var3,var6, var66] -- equality 
prog2_b2 = [var1, var2, var3,var6, var666] -- equality


prog3_g = [var1, var2, var3, var7] -- ordering
prog3_b1 = [var1, var2, var3, var7, var77] -- ordering
prog3_b2 = [var1, var2, var3, var7, var777] -- ordering
prog3_b3 = [var1, var2, var3, var7, var7777] -- ordering


prog4_g = [var1, var2, var3, var8]
prog4_b1 = [var1, var2, var3, var8, var88]
prog4_b2 = [var1, var2, var3, var8, var888]

prog5_g = [var1, var2, var3, var9]
prog5_b1 = [var1, var2, var3, var99]
prog5_b2 = [var1, var2, var3, var999]


prog6_g = [var1, var2, var3, var10]
prog6_b1 = [var1, var2, var3, var100]
prog6_b2 = [var1, var2, var3, var1000]


prog7_g = [var1, var2, var3, var11]
prog7_b1 = [var1, var2, var3, var111]
prog7_b2 = [var1, var2, var3, var1111]

prog10_g = [var_xs,var_comp]

--- type of testcases  [(program, expected_result, [(exp_variable_name, exp_var_value)])]
tests1 = [(prog1_g, True, [("x",42), ("z",42), ("a",44), ("myArray",0)]),
          (prog4_g, True, [("x",42), ("z",42), ("a",44),("ss",-44)]),
          (prog5_g, True, [("x",42), ("z",42), ("a",44),("ss",128)]),
          (prog6_g, True, [("x",42), ("z",42), ("a",44),("ss",77616)]),
          (prog7_g, True, [("x",42), ("z",42), ("a",44),("ss",5)]),
          (prog10_g, True, [("squares",0)])

          ]
tests2 = [(prog2_g, True,[("ss", True)]), (prog3_g, True, [("ss",True)])  ]--,
--, (prog2_b1, False),(prog2_b2, False),
--(prog3_g, True),-- (prog3_b1, False),(prog3_b2, False),(prog3_b3, False),
--(prog4_g, True),-- (prog4_b1, False),(prog4_b2, False),
--(prog5_g, True),-- (prog5_b1, False),(prog5_b2, False),
--(prog6_g, True),-- (prog6_b1, False),(prog6_b2, False),
--(prog7_g, True)]-- (prog7_b1, False),(prog7_b2, False)

-- ]
--(prog1_b1, False,[("x",42)]),(prog1_b2, False,[("x",42)])]--,(prog1_b3, False),[("x",42)]]--,
get1st (a,_,_) = a
get2nd (_,a,_) = a
get3rd (_,_,a) = a

testProg test success result = 
    case runProg (Prog test) of
        Left e -> putStrLn $ if not success then "true" else "false"
        Right res -> forM_ (Map.toList res) (\(n,v) -> do
            let resultMap = Map.fromList result
            case v of
                (IntVal b) -> case Map.lookup n resultMap of
                    Nothing -> putStrLn "false1"
                    (Just a)-> putStrLn $ show $ a==b
                (ArrayVal ((IntVal x1):(IntVal x2):[]))-> putStrLn $ show $ (x1==3 && x2==31)
                (ArrayVal ((IntVal x1):(IntVal x2):(IntVal x3):[]))-> putStrLn $ show $ (x1==0 && x2==1 && x3 == 2)
                (ArrayVal ((IntVal x1):(IntVal x2):(IntVal x3):(IntVal x4):[]))-> putStrLn $ show $ (x1==0 && x2==1 && x3 == 4 && x4 == 9)
                
                otherwise -> putStrLn "not relevant result"                    
            )
            
            
testProg1 test success result = 
    case runProg (Prog test) of
        Left e -> putStrLn $ if not success then "true" else "false"
        Right res -> forM_ (Map.toList res) (\(n,v) -> do
            let resultMap = Map.fromList result
            case v of
                TrueVal -> case Map.lookup n resultMap of
                    Nothing -> putStrLn "false2"
                    (Just a)-> putStrLn $ show $ a==True
                FalseVal -> case Map.lookup n resultMap of
                    Nothing -> putStrLn "false3"
                    (Just a)-> putStrLn $ show $ a==False
                otherwise -> putStrLn "not relevant result"   
            )
            

            
                

--putStrLn $ n ++ " = " ++ nice v)
doTests:: IO ()
doTests = do
    mapM_ (\tpl -> testProg (get1st tpl) (get2nd tpl) (get3rd tpl)) tests1 --test assigned Int and Array values
    mapM_ (\tpl -> testProg1 (get1st tpl) (get2nd tpl) (get3rd tpl)) tests2 -- test assigned Bool values

