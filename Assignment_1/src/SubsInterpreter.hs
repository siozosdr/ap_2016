module SubsInterpreter
       ( runProg
       , Error (..)
       , Value(..)
       )
       where

import SubsAst

-- You might need the following imports
import Control.Monad
import qualified Data.Map as Map
import qualified Data.List as List
import Data.Map(Map)

data Hole = Hole

-- | A value is either an integer, the special constant undefined,
--   true, false, a string, or an array of values.
-- Expressions are evaluated to values.
data Value = IntVal Int
           | UndefinedVal
           | TrueVal | FalseVal
           | StringVal String
           | ArrayVal [Value]
           deriving (Eq, Show)

-- ^ Any runtime error.  You may add more constructors to this type
-- (or remove the existing ones) if you want.  Just make sure it is
-- still an instance of 'Show' and 'Eq'.
data Error = Error String
             deriving (Show, Eq)

type Env = Map Ident Value
type Primitive = [Value] -> SubsM Value
type PEnv = Map FunName Primitive
type Context = (Env, PEnv)

initialContext :: Context
initialContext = (Map.empty, initialPEnv)
  where initialPEnv =
          Map.fromList [ ("===", equalF)
                       , ("<", compareF)
                       , ("+", plusF)
                       , ("*", timesF)
                       , ("-", minusF)
                       , ("%", moduloF)
                       , ("Array.new", arrayNew)
                       ]

newtype SubsM a = SubsM {runSubsM :: Context -> Either Error (a, Env)}

instance Functor SubsM where
  fmap f m = m >>= \a -> return (f a)


instance Applicative SubsM where
  pure = return
  (<*>) = ap

instance Monad SubsM where
  -- return :: a -> SubsM a
  return x = SubsM $ \(env, _) -> Right (x, env)
  -- (>>=) :: SubsM a -> (a -> SubsM b) -> SubsM b
  (SubsM s) >>= f = SubsM $ \(env, pEnv) -> 
    case s (env, pEnv) of
      Left (Error message) -> fail message
      Right (result, newEnv) -> runSubsM (f result) (newEnv, pEnv)
  -- fail :: String -> SubsM a
  fail s = SubsM $ \_ -> Left $ Error s

minusF :: Primitive
minusF [x] = fail "(-) needs two arguments"
minusF (IntVal x:xs) = return $ foldl (\(IntVal acc) (IntVal x) -> 
  IntVal (acc - x)) (IntVal x) xs
minusF _ = fail "Both arguments of (-) must be integers"

timesF :: Primitive
timesF [x] = fail "(*) needs two arguments"
timesF (x:xs) = return $ foldl(\(IntVal acc) (IntVal x) -> 
  IntVal (acc * x)) x xs
timesF _ = fail "Both arguments of (*) must be integers"

moduloF :: Primitive
moduloF [x] = fail "(%) needs two arguments"
moduloF (x:xs) = return $ foldl (\(IntVal acc) (IntVal x) -> 
  IntVal (acc `mod` x)) x xs
moduloF _ = fail "Both arguments of (mod) must be integers"

compareF:: Primitive
compareF (StringVal s1:[StringVal s2])
  | s1 < s2 = return TrueVal
  | otherwise = return FalseVal

compareF (IntVal s1:[IntVal s2])
  | s1 < s2 = return TrueVal
  | otherwise = return FalseVal

compareF _ = fail "You need exactly two arguments of same type to compare"

equalF:: Primitive
equalF (s1:[s2])
  | s1 == s2 = return TrueVal
  | otherwise = return FalseVal
equalF _ = fail "You need exactly two arguments of same type to compare"

plusF :: Primitive
plusF [x1] = return x1
plusF (UndefinedVal:_) = fail "can not use Undefined with (+) "
plusF (TrueVal:_) = fail "can not use Bool with (+) "
plusF (FalseVal:_) = fail "can not use Bool with (+) "
plusF (StringVal str:xs) = do
  rest <- plusF xs
  case rest of
    (IntVal intRest) -> return $ StringVal (str++show intRest)
    (StringVal strRest) -> return $ StringVal (str++strRest)
  

plusF (IntVal int:xs) = do
  rest <- plusF xs
  case rest of
    (IntVal intRest) -> return $ IntVal (int+intRest)
    (StringVal strRest) -> return $ StringVal (show int++strRest)

arrayNew :: Primitive
arrayNew [IntVal n] | n > 0 = return $ ArrayVal(replicate n UndefinedVal)
arrayNew _ = fail "Array.new called with wrong number of arguments"

getEnv :: SubsM Env
getEnv = SubsM (\(env, pEnv)-> Right (env, env) )

setEnv :: Env -> SubsM ()
setEnv env = SubsM (\_ -> Right ((), env))

modify :: (Env -> Env) -> SubsM ()
modify f = do
  env <- getEnv
  setEnv $ f env

updateEnv :: Ident -> Value -> SubsM ()
updateEnv name val = do
  oldEnv <- getEnv
  let newEnv = Map.insert name val oldEnv
  setEnv newEnv
getVar :: Ident -> SubsM Value
getVar name = do
  env <- getEnv
  case Map.lookup name env of
    Nothing -> fail ("Cannot find variable: " ++ show name)
    Just v -> return v
getFunction :: FunName -> SubsM Primitive
getFunction name = SubsM $ \(env, pEnv) -> 
  case Map.lookup name pEnv of
    Nothing -> fail ("Cannot find function: " ++ show name)
    Just f -> Right (f, env)

evalExpr :: Expr -> SubsM Value
evalExpr (Number i) = return (IntVal i)
evalExpr (String s) = return (StringVal s)
evalExpr Undefined = return UndefinedVal
evalExpr TrueConst = return TrueVal
evalExpr FalseConst = return FalseVal
evalExpr (Array a) = 
  do
    v <- mapM evalExpr a
    return (ArrayVal v)
evalExpr (Var name) = getVar name

evalExpr (Call fname e_array) = 
  do 
    e <- mapM evalExpr e_array
    f <- getFunction fname
    let e1 = e
    f e1

evalExpr (Assign name e) = do
  expr <- evalExpr e
  updateEnv name expr
  return expr

evalExpr (Comma e1 e2) = do
  _ <- evalExpr e1
  evalExpr e2


-- Top level function of list compr. 
evalExpr (Compr arrayFor expr) = do
  -- evaluate all the arrayFor. It returns 
  localStatements <- evalArrayFor arrayFor

  -- get just vars (x = [1,2,3]) from the statements
  let vars = filter (\a ->
        case a of
          VarDecl name (Just expr) -> True
          _ -> False
        ) localStatements

  -- get just conditions from the statements ( x % 2 == 0 )
  let conds = filter (\a ->
        case a of
          ExprAsStm expr -> True
          _ -> False
        ) localStatements

  -- evaluate the compr with vars, conditions and final expression 
  vals <- evalCompr vars conds expr
  return $ ArrayVal vals

-- takes conditions (ExprAsStm) and evaliates them. Returns final Bool (conjunction between bools)
evalConds :: [Stm] -> SubsM Bool
evalConds stms = do
  let unwrapped = map (\(ExprAsStm x)-> x) stms
  res <- mapM evalExpr unwrapped
  let transformed = map (\x ->
        case x of
        TrueVal -> True
        FalseVal -> False 
        ) res
  return $ and transformed


-- takes vars, conditions and expression and returns final list compr
evalCompr :: [Stm] -> [Stm] -> Expr -> SubsM [Value]
-- border case, just one var
evalCompr [var] conds expr = do
  let VarDecl name (Just exprs) = var
  evalEs <- evalExpr exprs
  case evalEs of
    -- if var consists of array ( x = [1,2,3])
    ArrayVal a -> do
      let results = map (\x -> do
              updateEnv name x
              conds <- evalConds conds
              if conds then do
                evE <- evalExpr expr
                return $ Just evE
              else
                return Nothing
            ) a
      unwrappedResults <- sequence results
      let filtered = filter (\a ->
            case a of
              Just x -> True
              _ -> False
              ) unwrappedResults
      let justs = map (\(Just x) -> x) filtered
      return $ justs
    -- if var consists of string ( x = "abc")
    StringVal s -> do
      let results = map (\x -> do
              updateEnv name (StringVal [x])
              conds <- evalConds conds
              if conds then do
                evE <- evalExpr expr
                return $ Just evE
              else
                return Nothing
            ) s
      unwrappedResults <- sequence results
      let filtered = filter (\a ->
            case a of
              Just x -> True
              _ -> False
              ) unwrappedResults
      let justs = map (\(Just x) -> x) filtered
      return $ justs
    otherwise -> fail "Compr value has to be sort of an array"

-- if more vars
evalCompr (var:vs) conds expr = do
  let VarDecl name (Just exprs) = var
  evalEs <- evalExpr exprs
  case evalEs of
    -- if var consists of array ( x = [1,2,3])
    ArrayVal a -> do
      let results = map (\x -> do
              -- take the element from an array
              -- update its value in the environment 
              updateEnv name x
              -- evaluate the following vars (..y = [4,5,6]). recursion.
              evE <- evalCompr vs conds expr
              return evE
            ) a
      unwrapped <- sequence results
      let flatten = concat unwrapped
      return $ flatten
    -- if var consists of string ( x = "abc")
    StringVal s -> do
      let results = map (\x -> do
              updateEnv name (StringVal [x])
              evE <- evalCompr vs conds expr
              return evE
            ) s
      unwrapped <- sequence results
      let flatten = concat unwrapped
      return $ flatten
    otherwise -> fail "Compr value has to be an array"


-- Collects all the statements from ArrayFor. That is, VarDecl-s (x = [1,2,3]) and ExprAsStm-s ( x % 2 == 0 )
--                                                "vars"--^              conditions ---^
evalArrayFor:: ArrayFor -> SubsM [Stm]
-- if Nothing more (end case)
evalArrayFor (name, e, Nothing) = do
  let stm = VarDecl name (Just e)
  return [stm]

-- if there is something more (call evalArrayCompr)
evalArrayFor (name, e, (Just arrayCompr)) = do
  let stm = VarDecl name (Just e)
  -- collect all the rest
  listOfStm <- evalArrayCompr arrayCompr
  return (stm:listOfStm)


--collecting all the statements in the list compr
evalArrayCompr :: ArrayCompr -> SubsM [Stm]

-- end case for ArrayIf
evalArrayCompr (ArrayIf e Nothing) = do
  return [ExprAsStm e]

-- ArrayIf recursion
evalArrayCompr (ArrayIf e (Just arrayCompr)) = do
  lstOfStm <- evalArrayCompr arrayCompr
  return $ (ExprAsStm e):lstOfStm 

-- ArrayForCompr recursion
evalArrayCompr (ArrayForCompr arrayFor) = do
  lstOfStm <- evalArrayFor arrayFor
  return $ lstOfStm

evalHelper :: Expr -> [Expr]
evalHelper (Array (h:hs)) = hs


stm :: Stm -> SubsM ()
stm (VarDecl name (Just e)) =
  do 
    v <- evalExpr e
    updateEnv name v
stm (VarDecl x Nothing) = 
    fail ("syntax error: Nothing to assign to " ++ show x)

stm (ExprAsStm e) = do 
  evalExpr e
  return ()

program :: Program -> SubsM ()
program (Prog prog) = mapM_ stm prog

runProg :: Program -> Either Error Env
runProg prog = 
  case runSubsM (program prog) initialContext of
    Left (Error message) -> Left (Error message)
    Right (_, env) -> Right env


runX arrFor =
  case runSubsM (evalExpr arrFor) initialContext of
  Left (Error message) -> Left (Error message)
  Right (x, env) -> Right x
