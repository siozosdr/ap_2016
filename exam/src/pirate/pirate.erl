%-
%    AP Exam November 2016
%    Sokratis Siozos-Drosos (dnb823)
%
-module(pirate).
-behavior(gen_server).
-export([go_on_account/0, register_simple/3, register_command/4, mission/2, ahoy/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, code_change/3, terminate/2]).

% =========== API ==================
go_on_account() ->
    gen_server:start(?MODULE, [], []).

register_simple(Pirate, Cmd, Fun) ->
    % register Fun as the Cmd::atom
    gen_server:cast(Pirate, {register, Cmd, Fun}).
  
register_command(_, _, _, _) ->
    undefined.

mission(Pirate, TreasureMap) ->
	io:format("pirate \n"),
    gen_server:call(Pirate, {mission, TreasureMap}).

ahoy(MissionRef) ->
    undefined.

% ------------ Callback functions ------------
handle_info(Info,State) -> {stop, Info, {dict:new(), dict:new()}}.
code_change(_,_,_) -> undefined.
terminate(Reason,State) -> ok.
init(_) ->
	io:format("Pirate init\n"),
	{ok, {dict:new(), dict:new()}}.

handle_cast({register, Cmd, Fun}, {CmdDict, MissionDict}) ->
	Newdict = dict:store(Cmd, Fun, CmdDict),
	{no_reply, {Newdict, MissionDict}}.


handle_call({mission, {cmd, Cmd, Arg}}, _From, {CmdDict, MissionDict}) ->
	case dict:is_key(Cmd, CmdDict) of
		true ->	Fun = dict:fetch(Cmd, CmdDict),
				MissionRef = make_ref(),
				NewMissionDict = dict:store(MissionRef, ongoing, MissionDict),
				Result = Fun(Arg),
				NewMissionDict = dict:store(MissionRef, success, MissionDict),
				{reply, {ok, Result}, {CmdDict, NewMissionDict}};
		false -> 
				{reply, {error, undefinedCommand}, {CmdDict, MissionDict}}
	end;
handle_call({mission, {line, SubMaps}}, _From, {CmdDict, MissionDict}) ->
	io:format("call handled\n"),
	Res = getSubMissionsResults(SubMaps),
	{reply, Res, {CmdDict, MissionDict}};
	%case lists:member({error, Reason}) of
	%	true -> 
	%		{reply, {error, Reason}, {CmdDict, MissionDict}};
	%	false -> 
	%		{ok, LastMissionRef} = lists:last(Res),
	%		LastRes = dict:fetch(LastMissionRef, MissionDict),
	%		{reply, {ok, LastRes}, {CmdDict, MissionDict}}
	%end.
handle_call(Msg, _From, {CmdDick, MissionDict}) -> 
	{reply,not_caught, {CmdDick, MissionDict}}.


% -------- Helper functions --------------
% get submission goals
getSubMissionsResults([]) -> [];
getSubMissionsResults([H|SubMaps]) -> 
	io:format("recurse"),
	ResH = gen_server:call(?MODULE, {mission, H}),
	case ResH of
		{error, Reason} -> 
			{error, Reason};
		{ok, MissionResult} ->
			[ResH|getSubMissionsResults(SubMaps)]
	end.