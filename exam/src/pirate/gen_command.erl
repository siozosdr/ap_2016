%-
%    AP Exam November 2016
%    Sokratis Siozos-Drosos (dnb823)
%
-module(gen_command).
-export([start/2, invoke/3, avast/2, ahoy/2, furl/1]).

%-------- Callback functions --------
-callback initialise(Args :: term()) -> 
	{ok, State :: term(), Limit :: atom()}.

-callback handle_invocations(Args :: term(), State :: term()) ->
    { reply, Reply :: term() } |
    { blimey} |
    { error, Reason :: term()} .

-callback carren(State :: term(), Args :: term()) ->
	{ok}.
-callback handle_furl(State :: term()) ->
	{ok}.

% ============= API ==========================
start(Mod, Args) -> 
	ServerRef = spawn(fun () ->
			{ok, State, Limit} = Mod:initialise(Args),
			process_flag(trap_exit, true),
			Super = self(),
			Worker = spawn_link(fun() -> worker_loop(Super, {Mod, State, Args}) end),
			Workers = dict:store(Worker, completed, dict:new()),
			supervisor(Mod, Workers, nothing, {Mod, State, Args}, Limit, 
				queue:new(), 0)
		end),
	case ServerRef of
		{error, Reason} -> {error, Reason};
		_ -> {ok, ServerRef} 
	end.
	

invoke(ServerRef, Args, From) -> 
	request_reply(ServerRef, {invoke, Args, From}).

avast(ServerRef, CID) ->
    async(ServerRef, {avast, CID}).

ahoy(ServerRef, CID) ->
    request_reply(ServerRef, {ahoy, CID}).

furl(ServerRef) ->
    request_reply(ServerRef, furl).

% =============== Internal functions and loops =================
%-- Workers: Map Pid -> status
supervisor(Mod, Workers, LastMsg, LastGoodState, Limit, QueuedWQ, CounterW) ->
	receive
		{'EXIT', Worker, avast_called} ->
			io:format("~p exited because of avast\n", [Worker]),
			NewWorkers = dict:erase(Worker, Workers),
			QueuedWL = queue:to_list(QueuedWQ),
			Temp = lists:map(fun ({ID, Msg}) ->
								if
								 	ID == Worker ->
								 		{ID,Msg}
								 end
							end, QueuedWL),
			NewQueuedWL = lists:delete(Temp, QueuedWL),
			NewQueuedWQ = queue:from_list(NewQueuedWL),
			supervisor(Mod, NewWorkers, LastMsg, LastGoodState, Limit, NewQueuedWQ, CounterW);
		{'EXIT', Worker, Reason} ->
            io:format("~p exited because of ~p~n", [Worker, Reason]),
            Me = self(),
            NewWorker = spawn_link(fun() -> worker_loop(Me, LastGoodState) end),
            case LastMsg of
                {Client, Ref, _Req} ->
                    Client ! {Ref, {you_killed_kenny_bastard, Reason}};
                _ -> NewWorker ! LastMsg
            end,
            Workers2 = dict:erase(Worker, Workers),
            NewWorkers = dict:store(NewWorker, complete, Workers2),
            supervisor(Mod, NewWorkers, LastMsg, LastGoodState, Limit, QueuedWQ, CounterW);
        {backup_this, State} ->
       	    supervisor(Mod, Workers, {backup_this, State}, State, Limit, QueuedWQ, CounterW);
		{From, {invoke, Args, ToAnswer}} ->
			Me = self(),
			Ref = make_ref(),
			CID = spawn_link(fun() -> worker_loop(Me, LastGoodState) end),
			if
				% if there are available workers, invoke the command
				CounterW < Limit -> 
					CID ! {From, Ref, {invoke, Args}},
					NewWorkers = dict:store(CID, running, Workers),
					NewCounterW = CounterW+1,
					ToAnswer ! {self(),{Ref, CID}},
					supervisor(Mod, NewWorkers, {From, {invoke, Args, ToAnswer}}, LastGoodState, Limit, QueuedWQ, NewCounterW);
				% if there are no available workers, queue the command
				CounterW == Limit ->
					NewQueuedWQ = queue:in({CID, {From, Ref, {invoke, Args}}}, QueuedWQ),
					NewWorkers = dict:store(CID, queued, Workers),
					ToAnswer ! {self(), {Ref, CID}},
					supervisor(Mod, NewWorkers, {From, {invoke, Args, ToAnswer}}, LastGoodState, Limit, NewQueuedWQ, CounterW)
			end;
		% When worker finishes, it changes status in the supervisor
		{result, FinishedWorker} ->
			NewCounterW = CounterW-1,
			case queue:out(QueuedWQ) of
				{empty, QueuedWQ} -> 
					NewWorkers = dict:store(FinishedWorker, completed, Workers),
					supervisor(Mod, NewWorkers, LastMsg, 
						{result, FinishedWorker} , Limit, QueuedWQ, NewCounterW);
				% in case there is a queued job, perform it
				{{value, {CID, Msg}}, NewQueuedWQ} ->
					NewWorkers = dict:store(FinishedWorker, completed, Workers),
					NewWorkers2 = dict:store(CID, busy, NewWorkers),
					CID ! Msg,
					supervisor(Mod, NewWorkers2, LastMsg, 
						{result, FinishedWorker} , Limit, NewQueuedWQ, CounterW)
			end;
		{avast, CID} -> 
			NewCounterW = CounterW-1,
			case dict:is_key(CID, Workers) of
				true ->
					{State, Args} = request_reply(CID, avast),
					NewWorkers = dict:erase(CID, Workers),
					QueuedWL = queue:to_list(QueuedWQ),
					Temp = lists:map(fun ({ID, Msg}) ->
										if
										 	ID == CID ->
										 		{ID,Msg}
										 end
									end, QueuedWL),
					NewQueuedWL = lists:delete(Temp, QueuedWL),
					NewQueuedWQ = queue:from_list(NewQueuedWL),
					Mod:carren(State, Args),
					supervisor(Mod, NewWorkers, {avast, CID}, LastGoodState, Limit, NewQueuedWQ, NewCounterW);
				false->
					supervisor(Mod, Workers, {avast, CID}, LastGoodState, Limit, QueuedWQ, CounterW)
			end;
			
			
		{From, {ahoy, CID}} ->
			case dict:is_key(CID, Workers) of
				true ->
					Status = dict:fetch(CID, Workers),
					From ! {self(), Status};
				false ->
					From !{self(), undefined_command}
			end,
			
			supervisor(Mod, Workers, {From, {ahoy, CID}}, LastGoodState, Limit, QueuedWQ, CounterW);
		{From, furl} ->
			dict:map(fun(Worker,_) -> self() ! {avast, Worker} end, Workers),
			Mod:handle_furl(LastGoodState),
			From ! {self(), ok}			
	end.

worker_loop(Supervisor, {Mod, State, Args}) ->
	Supervisor ! {backup_this, {Mod, State, Args}},
    receive
    	% once invocation is complete, send update to supervisor and From
        {From, Ref, {invoke, NewArgs}} ->
        	Reply = Mod:handle_invocations(NewArgs, State),
			Supervisor ! {result, self()},
        	case Reply of
        		blimey -> exit(blimey);
        		{reply, Res} -> 
        			Supervisor ! {result, self()},
	        		From ! {Ref, {reply, Res}},
	        		worker_loop(Supervisor, {Mod, State, NewArgs});
	        	Other -> 
	        		exit(Other)
        	end;
        	
        % send current State and Args to supervisor 
        % to be used for caren
        {From, avast} ->
        	From ! {self(), {State, Args}},
        	exit(avast_called);
        Msg -> 
        	io:format("Oops, Uncaught Message received: ~p\n", [Msg]),
        	io:format("to ~p\n", [self()]),
        	io:format("A group of highly trained monkeys is looking into it.\n")

    end.



%....... Utilities----------

request_reply(Pid, Request) ->
	Pid ! {self(), Request},
	receive
		{Pid, Response} -> Response;
		{Pid, error, Response} ->
			{Pid,{server_error, Response}}
	after 3000 ->
		throw(request_timeout)
	end.

async(Pid, Cont) -> Pid ! Cont.
