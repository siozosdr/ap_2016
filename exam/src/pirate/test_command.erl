%-
%    AP Exam November 2016
%    Sokratis Siozos-Drosos (dnb823)
%
%% Simple module implementing the gen_command behavior to test it
-module(test_command).
-compile(export_all).
-behavior(gen_command).
-import(gen_command, [start/2, invoke/3, avast/2, ahoy/2, furl/1]).

% set to number of maximum concurrent workers
start(Limit) -> start(test_command, Limit).

% Produces tables of [1,..,N]
count(ServerRef, N) -> 
	{Ref,CID} = invoke(ServerRef, {count, N}, self()),
	receive
		{Ref, Result} -> {CID, Result};
		Msg -> Msg
	end.

% checks current status of command
checkStatus(ServerRef, CID) ->
	ahoy(ServerRef, CID).

% terminates command
terminate(ServerRef, CID) ->
	avast(ServerRef, CID).

% shuts down command server
shut_down(ServerRef) ->
	furl(ServerRef).

%% callbacks
initialise(Limit) -> {ok, nothing, Limit}.

handle_invocations({count, N}, State) ->
	if
		N =< 0 ->
			blimey;
		N > 0 ->
			Result = lists:seq(1, N),
			{reply, Result}
	end.

carren(State, Args) -> 
	ok.

handle_furl(State) -> 
	ok.
