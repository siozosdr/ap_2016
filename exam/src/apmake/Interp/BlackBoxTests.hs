module Interp.BlackBoxTests where

import Ast
import Interp

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
-- import Test.Tasty.Golden

unitTests :: TestTree
unitTests = testGroup "Unit tests" 
	[
      testCase "Testing replace.1" $
        (replace "tem" [St, Lt "plate"]) @?=
            ("template"),
      testCase "Testing replace.2" $
        (replace "tem" [St, Lt "plate", St]) @?=
            ("templatetem"),
      testCase "Testing check.1" $
        (check mf1 ) @?=
            (True),
      testCase "Testing check.2" $
        (check mf2 ) @?=
            (True),
      testCase "Testing check.3" $
        (check bmf1 ) @?=
            (False),
      testCase "Testing check.4" $
        (check bmf2 ) @?=
            (False),
      testCase "Testing match.1" $
        (match "foo.cpp" [Lt "f",St, Lt ".cpp"]) @?=
            (Just (Just "oo")),
      testCase "Testing match.2" $
        (match "foo.cpp" [St, Lt ".cpp"]) @?=
            (Just (Just "foo")),
      testCase "Testing match.3" $
        (match "templateString.templateExtension" [Lt "t",St, Lt "Extension"]) @?=
            (Just (Just "emplateString.template")),
      testCase "Testing match.4" $
        (match "templateString.templateExtension" [Lt "t",St, Lt "ion"]) @?=
            (Just (Just "emplateString.templateExtens")),
      testCase "Testing match.5" $
        (match "templateString.templateExtension" [St, Lt "String.templateExtension"]) @?=
            (Just (Just "template")),
      testCase "Testing match.6" $
        (match "templateString.templateExtension" [Lt "template", St, Lt "xtension"]) @?=
            (Just (Just "String.templateE")),
      testCase "Testing match.7" $
        (match "templateString.templateExtension" [St, Lt "String.templateE", St]) @?=
            (Nothing),
      testCase "Testing match.8" $
        (match "templateString.templateExtension" [St, Lt "String.anothertemplateExtension"]) @?=
            (Nothing),
      testCase "Testing match.9" $
        (match "templateString.templateExtension" [Lt "templateString.templateExtension"]) @?=
            (Just Nothing),
      testCase "Testing match.10" $
        (match "templateString.templateExtension" [St, Lt ".", St]) @?=
            (Nothing),
      testCase "Testing build.1" $
        (build mf1 "foo" f1 2) @?=
            (Just ["cc -c foo.c","as -o bar.o bar.s","cc -o foo foo.o bar.o","nm foo > foo.map","strip foo"]),
      testCase "Testing build.2" $
        (build mf1 "foo.o" f1 2) @?=
            (Just ["cc -c foo.c"]),
      testCase "Testing build.3" $
        (build mf1 "bar.o" f1 2) @?=
            (Just ["as -o bar.o bar.s"]),
      testCase "Testing build.3" $
        (build mf2 "main.o" f2 2) @?=
            (Just ["g++ -c main.cpp"]),
      testCase "Testing build.4" $
        (build mf2 "factorial.o" f2 2) @?=
            (Just ["g++ -c factorial.cpp"]),
      testCase "Testing build.5" $
        (build mf2 "hello" f2 2) @?=
            (Just ["g++ -c main.cpp","g++ -c factorial.cpp","g++ -c hello.cpp","g++ main.o factorial.o hello.o -o hello"]),
      testCase "Testing build fail.6" $
        (build mf2 "factorial.cpp" f2 2) @?=
            (Nothing),
      testCase "Testing build fail.7" $
        (build mf2 "non_existent" f2 2) @?=
            (Nothing),
      testCase "Testing build fail.8" $
        (build mf1 "foo.c" f1 2) @?=
            (Nothing),
      testCase "Testing build fail.8" $
        (build mf2 "hello" f2 1) @?=
            (Nothing),
      testCase "Testing build fail.8" $
        (build mf1 "foo" f1 1) @?=
            (Nothing)]

qcTests :: TestTree
qcTests = testGroup "QuickCheck tests" []

goldenTests :: TestTree
goldenTests = testGroup "Golden tests" []

main :: IO ()
main = defaultMain $ testGroup "Black-box interpreter tests"
  [ unitTests, qcTests, goldenTests ]

-- ========= Testing definitions ==========

-- example good target,prerequisites, commands
ts11 :: [[Frag]]
ts11 = [[Lt "foo"],[Lt "foo.map"]] 
ps11 :: [[Frag]]
ps11 = [[Lt "foo.o"],[Lt "bar.o"]]
cs11 :: [[Frag]]
cs11 = [[Lt "cc -o foo foo.o bar.o"],[Lt "nm foo > foo.map"],
             [Lt "strip foo"]]
ts12 :: [[Frag]]             
ts12 = [[Lt "foo.o"]]
ps12 :: [[Frag]]
ps12 = [[Lt "foo.c"]]
cs12 :: [[Frag]]
cs12 = [[Lt "cc -c foo.c"]]

ts13 :: [[Frag]]
ts13 = [[Lt "bar.o"]]
ps13 :: [[Frag]]
ps13 = [[Lt "bar.s"]]
cs13 :: [[Frag]]
cs13 = [[Lt "as -o bar.o bar.s"]]

ts21 :: [[Frag]]
ts21 = [[Lt "hello"]]
ps21 :: [[Frag]]
ps21 = [[Lt "main.o"],[Lt "factorial.o"],[Lt "hello.o"]]
cs21 :: [[Frag]]
cs21 = [[Lt "g++ main.o factorial.o hello.o -o hello"]]

ts22 :: [[Frag]]
ts22 = [[Lt "main.o"]]
ps22 :: [[Frag]]
ps22 = [[Lt "main.cpp"]]
cs22 :: [[Frag]]
cs22 = [[Lt "g++ -c main.cpp"]]

ts23 :: [[Frag]]
ts23 = [[Lt "factorial.o"]]
ps23 :: [[Frag]]
ps23 = [[Lt "factorial.cpp"]]
cs23 :: [[Frag]]
cs23 = [[Lt "g++ -c factorial.cpp"]]

ts24 :: [[Frag]]
ts24 = [[Lt "hello.o"]]
ps24 :: [[Frag]]
ps24 = [[Lt "hello.cpp"]]
cs24 :: [[Frag]]
cs24 = [[Lt "g++ -c hello.cpp"]]
-- example good makefiles
mf1 :: Makefile
mf1 = 
      [ Rule ts11 ps11 cs11,
        Rule ts12 ps12 cs12 ,
        Rule ts13 ps13 cs13
      ]
mf2 :: Makefile
mf2 = 
      [ Rule ts21 ps21 cs21,
        Rule ts22 ps22 cs22,
        Rule ts23 ps23 cs23,
        Rule ts24 ps24 cs24
      ]

-- example bad targets, prerequisites, commands
bts11 :: [[Frag]]
bts11 = [[Lt "foo"],[Lt "foo.map"]] 
bps11 :: [[Frag]]
bps11 = [[Lt "foo.o"],[St, Lt ".o"]]
bcs11 :: [[Frag]]
bcs11 = [[Lt "cc -o foo foo.o bar.o"],[Lt "nm foo > foo.map"],
             [Lt "strip foo"]]
bts12 :: [[Frag]]
bps12 :: [[Frag]]
bts12 = [[Lt "foo.o"]]
bps12 = [[Lt "foo.c"]]
bcs12 :: [[Frag]]
bcs12 = [[Lt "cc -c", St, Lt ".c"]]

bts13 :: [[Frag]]
bts13 = [[Lt "bar.o"]]
bps13 :: [[Frag]]
bps13 = [[St, Lt ".s"]]
bcs13 :: [[Frag]]
bcs13 = [[Lt "as -o bar.o bar.s"]]

bts21 :: [[Frag]]
bts21 = [[Lt "hello"]]
bps21 :: [[Frag]]
bps21 = [[Lt "main.o"],[Lt "facto", St, Lt "rial.o"],[Lt "hello.o"]]
bcs21 :: [[Frag]]
bcs21 = [[Lt "g++ main.o factorial.o hello.o -o hello"]]

bts22 :: [[Frag]]
bts22 = [[Lt "main.o"]]
bps22 :: [[Frag]]
bps22 = [[Lt "main", St]]
bcs22 :: [[Frag]]
bcs22 = [[Lt "g++ -c main.cpp"]]

bts23 :: [[Frag]]
bts23 = [[Lt "factorial.o"]]
bps23 :: [[Frag]]
bps23 = [[Lt "factorial.cpp"]]
bcs23 :: [[Frag]]
bcs23 = [[Lt "g++ -c", St, Lt "rial.cpp"]]

bts24 :: [[Frag]]
bts24 = [[Lt "hello.o"]]
bps24 :: [[Frag]]
bps24 = [[St, Lt ".", St]]
bcs24 :: [[Frag]]
bcs24 = [[Lt "g++ -c hello.cpp"]]

bts25 :: [[Frag]]
bts25 = [[Lt "clean"]]
bps25 :: [[Frag]]
bps25 = [[Lt "prereq"]]
bcs25 :: [[Frag]]
bcs25 = [[Lt "rm ",St, Lt "o hello"]]

-- example bad makefiles
bmf1 :: Makefile
bmf1 = 
      [ Rule bts11 bps11 bcs11,
        Rule bts12 bps12 bcs12 ,
        Rule bts13 bps13 bcs13
      ]
bmf2 :: Makefile
bmf2 = 
      [ Rule bts21 bps21 bcs21,
        Rule bts22 bps22 bcs22,
        Rule bts23 bps23 bcs23,
        Rule bts24 bps24 bcs24,
        Rule bts25 bps25 bcs25
      ]
-- example functions
f1 :: [Char] -> Bool
f1 = (`elem` ["foo.c", "bar.s"])
f2 :: [Char] -> Bool
f2 = (`elem` ["factorial.cpp", "hello.cpp", "main.cpp"])