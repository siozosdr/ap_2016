{-
    AP Exam November 2016
    Sokratis Siozos-Drosos (dnb823)
-}
module Interp.Impl where

import Ast
import Control.Monad
import Data.Map as Map
import Data.List
import Data.List.Split
type TargetToCommandsMap = Map File ([FileT], [CommandT]) 
type Environment = TargetToCommandsMap

initialEnvironment :: Environment
initialEnvironment = Map.empty

newtype APMakeM a = APMakeM {runAPMakeM :: Environment -> (a, Environment)}

instance Functor APMakeM where
    fmap f m = m >>= \a -> return (f a)

instance Applicative APMakeM where
    pure = return
    (<*>) = ap

instance Monad APMakeM where
    -- return :: a -> APMake a
    return x = APMakeM $ \env -> (x, env)
    -- (>>=) :: APMake a -> (a -> APMake b) -> APMake b
    (APMakeM a) >>= f = APMakeM $ \env -> 
        let (result, newEnv) = a env
        in runAPMakeM (f result) newEnv  

-- get environment
getEnv :: APMakeM Environment
getEnv = APMakeM $ \env -> (env, env)

-- set environment
setEnv :: Environment -> APMakeM ()
setEnv env = APMakeM $ const ((), env)

-- evaluate preReq by fetching all the commands associated with it 
-- from the environment
evalPrereq :: File -> APMakeM ([FileT], [CommandT])
evalPrereq = getVar

-- evaluate prereqs by searching in the map and finding any
-- commands associated with each preReq
evalPrereqs :: [File] -> APMakeM [CommandT]
evalPrereqs ps = do
    cmds <- mapM evalPrereq ps
    let res = Data.List.map snd cmds 
    let flattenedRes = intercalate [] res
    return flattenedRes 

-- update the environment by adding the target to it
updateEnv :: String -> ([FileT], [CommandT]) -> APMakeM ()
updateEnv target (preReqs, commands) = do
    oldTemplateMap <- getEnv
    let newTemplateMap = Map.insert target (preReqs, commands) oldTemplateMap
    setEnv newTemplateMap

-- get prerequisites and commands of a target
getVar :: String -> APMakeM ([FileT], [CommandT])
getVar v = do
    oldTemplateMap <- getEnv
    case Map.lookup v oldTemplateMap of
        Nothing -> return ([[Lt ""]],[[Lt ""]])
        (Just cmds) -> return cmds

-- convert template to string
evalTemplate :: Template -> APMakeM String
evalTemplate frags = do
    -- iterate over frags of the template to produce a String for the environment
    let res = Data.List.foldr (\f acc->
            case f of
                Lt s -> s:acc
                St -> "%":acc) [] frags
    let flattenedRes = intercalate [] res
    return flattenedRes

-- convert multiple templates to strings
evalTemplates :: [Template] -> APMakeM [String]
evalTemplates = mapM evalTemplate        

-- register rules to the environment
regRules :: [Rule] -> APMakeM ()
regRules rules = void $ mapM regRuleCommands rules

-- add initial rules with commands
regRuleCommands :: Rule -> APMakeM ()
regRuleCommands (Rule ts ps coms) = do
    targets <- evalTemplates ts
    void $ mapM (\t -> updateEnv t (ps, coms)) targets

evalTarget :: (File -> Bool) -> Int -> Int -> File -> APMakeM [Command]
evalTarget f steplimit currentStep target
    -- end case of too many steps, append ["limit"], so we can pattern match
    -- in the result and return the corresponding result with build
    | currentStep >= steplimit = return ["limit"]

    | currentStep < steplimit = do
        (tPreReqs, tComms) <- getVar target
        targetCommands <- evalTemplates tComms
        if Data.List.null tPreReqs
            then return $ targetCommands ++ ["end"]
        else do
            preReqRes <- mapM (\p -> do
                -- check if prerequisite already exists. 
                -- if True, no need to append anything
                -- if False, fide prereq om emvorpm,emt amd evaluate it as a target
                preReq <- evalTemplate p
                if f preReq then return [] else
                    evalTarget f steplimit (currentStep + 1) preReq) tPreReqs
            let reqsFlattened = intercalate [] preReqRes
            case reqsFlattened of
                [] -> return $ reqsFlattened ++ targetCommands
                _ -> if head reqsFlattened /= "limit"
                    then do
                        let res = reqsFlattened++targetCommands
                        return res
                    else return reqsFlattened
    | f target = return [target]
evalTarget _ _ _ _ = return []
evalMakefile :: Makefile -> File -> (File -> Bool) -> Int -> APMakeM (Maybe [Command])
evalMakefile rules target f steplimit = 
    if f target then return Nothing else
        (do regRules rules
            res <- evalTarget f steplimit 0 target
            case res of
                [] -> return Nothing
                _ -> if head res /= "limit" then return $ Just res else
                       return Nothing)

-- ============ API functions =====================
build :: Makefile -> File -> (File -> Bool) -> Int -> Maybe [Command]
build mf targetFile fileCheck steps = 
    case runAPMakeM (evalMakefile mf targetFile fileCheck steps) initialEnvironment of
        (Just res, _)-> Just res
        (Nothing, _) -> Nothing



replace :: String -> Template -> String
replace s template = do
    -- iterate over frags of the template to produce a String for the environment
    let res = Data.List.foldr (\f acc->
            case f of
                Lt sRes -> sRes:acc
                St -> "%":acc) [] template
    let flattenedRes = intercalate [] res
    replaceS "%" s flattenedRes



match :: String -> Template -> Maybe (Maybe String)
match s template = --case St `elem` template of
    -- if there are no placeholders, check if the string matches the template
    if St `elem` template 
        then (do 
            let sTokens = tokens s
            let matches
                  = Data.List.foldl
                      (\ acc subS ->
                         case matchSubString s subS template of
                             (Just (Just s3)) -> s3
                             _ -> acc)
                      []
                      sTokens
            case matches of
                "" -> Nothing
                _ -> return $ Just matches)
    else (do 
        let t = replace s template
        if s == t then return Nothing else Nothing)

check :: Makefile -> Bool
check rules = 
    let res = Data.List.map checkRule rules
        in and res

---------Helpers------------
-- Source: http://stackoverflow.com/questions/14907600/how-to-replace-a-string-with-another-in-haskell
replaceS :: String -> String -> String -> String
replaceS old new = intercalate new . splitOn old

-- Helper function for match
matchSubString ::String -> String -> Template -> Maybe (Maybe String)
matchSubString initial subS template = do
    -- replace placeholder in template with subS and compare to initial
    let t = replace subS template
    if initial == t then
        return $ Just subS
    else Nothing
-- Helper function for match
replaceAll :: String -> String -> String
replaceAll [] s = s
replaceAll (c1:rest) string = do
    let newS = replaceS [c1] "" string
    replaceAll rest newS

-- Helper function for check
-- return True no placeholders are present or there are only in target
-- return False in any other case
checkRule :: Rule -> Bool
checkRule (Rule ts ps cs) = do 
    let tsCh = checkForPlaceholder ts
    let psCh = checkForPlaceholder ps 
    let csCh = checkForPlaceholder cs
    -- if both have placeholders
    not (psCh || csCh) || tsCh
-- Helper function for check
-- if there are any placeholders in templates, return True else False
checkForPlaceholder :: [Template] -> Bool
checkForPlaceholder templates = 
    let res = Data.List.map (\t -> St `elem` t) templates

    in True `elem` res 

-- Source: http://stackoverflow.com/questions/11495875/how-to-find-all-substrings-of-a-string-with-start-and-end-indices
tokens :: String -> [String]
tokens xs = Data.List.map fst $ zip s ind
    where s   = continuousSubSeqs xs
          ind = continuousSubSeqs [0..length xs-1]

continuousSubSeqs :: [a] -> [[a]]
continuousSubSeqs = Data.List.filter (not . Data.List.null) . concatMap inits . tails

{-
-- My attempt at replacing the placeholders
-- I placed comments inside explaining the way I thought of solving it
-- in case you decide to have a look at it.
-- This function fiven a set of rules, attempt to replace each target's
-- placeholder with the matched string for the prerequisites of the 
-- environment.
elimPlaceHolders :: [Rule] -> APMakeM ()
elimPlaceHolders rules = void $ mapM elimPlaceHoldersRule rules

elimPlaceHoldersRule :: Rule -> APMakeM ()
elimPlaceHoldersRule (Rule ts ps cs) = do
    preReqs <- evalTemplates ps
    comms <- evalTemplates cs
    -- for every target t in ts
    void $ mapM (\t -> case St `elem` t of
        True -> do
            (oldTemplateMap, oldCounterMap) <- getEnv
            -- extract the environment data
            let dataMap = toList oldTemplateMap
            -- go through each prerequisite in the environment 
            -- and check if it matches with the target template t
            let newDataMap = Data.List.foldl (\acc (k,v@(preRs, commands)) -> do
                -- if there is a match replace the placeholder in the template target t
                let matched = Data.List.foldl(\acc p -> do
                    pE <- evalTemplate p
                    m <- match pE t
                    case m of
                        (Just (Just s)) -> s
                        _ -> acc) [] preRs 
                case matched of
                    [] -> acc ++ [(k,v)]
                    m -> do
                        let r = replace m t
                        acc ++ [(r,(ps,cs))]) [] dataMap
            let newTemplateMap = Map.fromList newDataMap
            setEnv (newTemplateMap, oldCounterMap)
            return ()
        False -> return ()) ts
-}