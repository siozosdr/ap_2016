module Parser.BlackBoxTests where

import Ast
import Parser

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
--import Test.Tasty.Golden

unitTests :: TestTree
unitTests = testGroup "Unit tests" 
	[testCase "Testing parseString.1" $
        (parseString "foo: foo.o bar.o\n\tcc -o foo foo.o bar.o\n%.o: %.c\n\tcc -S %.c\n\tas -o %.o %.s\n        ") @?=
            (Right [Rule [[Lt "foo"]] [[Lt "foo.o"],[Lt "bar.o"]] [[Lt "cc -o foo foo.o bar.o"]],Rule [[St,Lt ".o"]] [[St,Lt ".c"]] [[Lt "cc -S ",St,Lt ".c"],[Lt "as -o ",St,Lt ".o ",St,Lt ".s"]]]),
     testCase "Testing parseString.2" $
        (parseString "foo: foo.o bar.o\n\t cc -o foo foo.o bar.o\n") @?=
            (Right [Rule [[Lt "foo"]] [[Lt "foo.o"],[Lt "bar.o"]] [[Lt " cc -o foo foo.o bar.o"]]]),
     testCase "Testing parseString.3" $
        (parseString "hello: main.o factorial.o hello.o\n\tg++ main.o factorial.o hello.o -o hello\n") @?=
            (Right [Rule [[Lt "hello"]] [[Lt "main.o"],[Lt "factorial.o"],[Lt "hello.o"]] [[Lt "g++ main.o factorial.o hello.o -o hello"]]]),
     testCase "Testing parseString.4" $
        (parseString "main.o: main.cpp\n\tg++ -c main.cpp\n") @?=
            (Right [Rule [[Lt "main.o"]] [[Lt "main.cpp"]] [[Lt "g++ -c main.cpp"]]]),
     testCase "Testing parseString.5" $
        (parseString "clean: prereq\n\trm %o hello\n") @?=
            (Right [Rule [[Lt "clean"]] [[Lt "prereq"]] [[Lt "rm ",St,Lt "o hello"]]]),
     testCase "Testing parseString.6" $
        (parseString "factorial.o: factorial.cpp\n\tg++ -c factorial.cpp\n") @?=
            (Right [Rule [[Lt "factorial.o"]] [[Lt "factorial.cpp"]] [[Lt "g++ -c factorial.cpp"]]]),
     testCase "Testing parseString.7" $
        (parseString "hellomake: hellomake.c hellofunc.c\n\tgcc -o hellomake hellomake.c hellofunc.c -I.\n") @?=
            (Right [Rule [[Lt "hellomake"]] [[Lt "hellomake.c"],[Lt "hellofunc.c"]] [[Lt "gcc -o hellomake hellomake.c hellofunc.c -I."]]]),
     testCase "Testing parseString.7" $
        (parseString "t1.o: p1.c %DEPS\n\t$(CC) -c -o $@ $< $(CFLAGS\n") @?=
            (Right [Rule [[Lt "t1.o"]] [[Lt "p1.c"],[St,Lt "DEPS"]] [[Lt "$(CC) -c -o $@ $< $(CFLAGS"]]])  
     ]

qcTests :: TestTree
qcTests = testGroup "QuickCheck tests" []

goldenTests :: TestTree
goldenTests = testGroup "Golden tests" []

main :: IO ()
main = defaultMain $ testGroup "Black-box parser tests"
  [ unitTests, qcTests, goldenTests ]
