{-
    AP Exam November 2016
    Sokratis Siozos-Drosos (dnb823)
-}
module Parser.Impl where

import Ast

import Control.Monad (void)
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Error
import Text.Parsec.String
import Text.Parsec.Combinator

--type ParseError = String -- Must be instance of (Eq, Show).

-- Your code here.
parseString :: String -> Either ParseError Makefile
parseString s = case parseme makeFile s of
    Left err -> Left err
    Right result -> Right result

parseFile :: FilePath -> IO (Either ParseError Makefile)
parseFile path = fmap parseString $ readFile path

makeFile :: Parser Makefile
makeFile = rules []

rules :: [Rule] -> Parser Makefile
rules r = (do
    void spaces
    r2 <- rule
    void spaces
    rules (r++[r2])
    )<|> return r 
rule :: Parser Rule
rule = do
    t <- targets
    void $ cToken ':'
    pr <- preReqs
    void $ cToken2 '\n'
    void $ cToken2 '\t'
    c <- commands
    void $ cToken2 '\n'
    return $ Rule t pr c

-- parse targets separated by whitespaces
targets :: Parser [FileT]
targets = sepBy template (cToken2 ' ')

-- parse prerequisites separated by whitespaces
preReqs :: Parser [FileT]
preReqs = sepBy template (cToken2 ' ')

commands :: Parser [CommandT]
commands = do
    c <- command
    commands' [c]
commands' :: [CommandT] -> Parser [CommandT]
commands' cmds = try(do
    void $ cToken2 '\n'
    void $ cToken2 '\t'
    c <- command
    commands' (cmds++[c])
    ) <|> return cmds


command :: Parser CommandT
command = cmdTemplate

templates :: Parser [Template]
templates = do
    t <- template
    templates' [t]

templates' :: [Template] -> Parser [Template]
templates' ts = (do
    void spaces
    t2 <- template
    void spaces
    templates' (ts ++ [t2])
    ) <|> return ts 

-- template parser for commands
cmdTemplate :: Parser Template
cmdTemplate = do
    f <- cmdFrag
    cmdTemplate' [f]

cmdTemplate' :: [Frag] -> Parser Template
cmdTemplate' fs = (do
    f <- cmdFrag
    cmdTemplate' (fs ++ [f])
    ) <|> return fs

-- frag parser for commands
-- parses arbitrary characters except '\n'
cmdFrag :: Parser Frag
cmdFrag = (do
    f <- many1(letter <|> digit <|> 
            cToken2 '_' <|> cToken2 '.' <|>
            cToken2 '/' <|> cToken2 '-' <|>
            cToken2 ':' <|> cToken2 ' ' <|> 
            cToken2 '<' <|> cToken2 '>' <|>
            cToken2 '+' <|> cToken2 '*' <|>
            cToken2 '$' <|> cToken2 '@' <|>
            cToken2 '(' <|> cToken2 ')')
    return $ Lt f
    ) <|> (do
        void stem
        return St)

-- template parser for targets and prerequisites
template :: Parser Template
template = do
    f <- frag
    template' [f]

template' :: [Frag] -> Parser Template
template' f = (do
    f2 <- frag
    template' (f ++ [f2])
    ) <|>  return f

-- frag parser for targets and prerequisites
frag :: Parser Frag
frag = (do
        void stem
        return St
    ) <|> (do
        l <- many1 literal
        return $ Lt l) 

-- parse a literal
-- it either contains characters of type 1 as described in the assignment text
-- or characters of type 2 as described in the assignment text
literal :: Parser Char
literal = parseChar1 <|> parseChar2

parseChar1 :: Parser Char
parseChar1 = letter <|> digit <|> 
        char '_' <|> char '.' <|>
        char '/' <|> char '-'

parseChar2 :: Parser Char
parseChar2 = (do
    void $ char '\\'
    c2 <- char '%' <|> char ':' <|> char ' ' <|> char '\\'
    return c2)

stem :: Parser String
stem = do
    c <- cToken2 '%'
    return [c]
-- eliminate spaces when parsing operators with 1 character
cToken :: Char -> Parser Char
cToken c = try (spaces >> char c <* spaces)

cToken2 :: Char -> Parser Char
cToken2 c = try(char c)

--helper
parseme :: Parser a -> String -> Either ParseError a
parseme parser = parse parser ""