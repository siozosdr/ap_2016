module Parser.WhiteBoxTests where

import Ast
import Parser.Impl

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Control.Monad(liftM)
--import Test.Tasty.Golden

unitTests :: TestTree
unitTests = testGroup "Unit tests"
    [ testCase "Testing stem.1" $
        (parseme stem "%_x0") @?=
            (Right "%"),
      testCase "Testing stem.2" $
        (parseme stem "%") @?=
            (Right "%"),
      testCase "Testing frag.1" $
        (parseme frag "testfrag") @?=
            (Right (Lt "testfrag")),
      testCase "Testing frag.2" $
        (parseme frag "testfrag.hs") @?=
            (Right (Lt "testfrag.hs")),
      testCase "Testing frag.3" $
        (parseme frag "testfrag2.hs") @?=
            (Right (Lt "testfrag2.hs")),
      testCase "Testing frag.3" $
        (parseme frag "test3frag2.hs") @?=
            (Right (Lt "test3frag2.hs")),
      testCase "Testing frag.4" $
        (parseme frag "test\\ 3frag2.hs") @?=
            (Right (Lt "test 3frag2.hs")),
      testCase "Testing frag.5" $
        (parseme frag "\\\\tes\\%t\\ 3fra\\%g2.hs") @?=
            (Right (Lt "\\tes%t 3fra%g2.hs")),
      testCase "Testing frag.6" $
        (parseme frag "\\\\tes") @?=
            (Right (Lt "\\tes")),
      testCase "Testing frag.7" $
        (parseme frag "%") @?=
            (Right St),
      -- Template and cmdTemplate
      testCase "Testing template.1" $
        (parseme template "testTemplate") @?=
            (Right [Lt "testTemplate"]),
      testCase "Testing template.2" $
        (parseme template "test\\ Template.hs") @?=
            (Right [Lt "test Template.hs"]),
      testCase "Testing template.3" $
        (parseme template "%.hs") @?=
            (Right [St, Lt ".hs"]),
      testCase "Testing template.4" $
        (parseme template "test%.hs") @?=
            (Right [Lt "test", St, Lt ".hs"]),
      testCase "Testing template.5" $
        (parseme template "test%temp\\ late.hs") @?=
            (Right [Lt "test", St, Lt "temp late.hs"]),
      testCase "Testing template.6" $
        (parseme template "test\\:%tem-p\\ late.hs") @?=
            (Right [Lt "test:", St, Lt "tem-p late.hs"]),
      testCase "Testing template.7" $
        (parseme template "testTemplate") @?=
            (Right [Lt "testTemplate"]),
      testCase "Testing cmdTemplate.1" $
        (parseme cmdTemplate "testTemplate") @?=
            (Right [Lt "testTemplate"]),
      testCase "Testing cmdTemplate.2" $
        (parseme cmdTemplate "stack exec ghci -- -Wall") @?=
            (Right [Lt "stack exec ghci -- -Wall"]),
      testCase "Testing cmdTemplate.3" $
        (parseme cmdTemplate "cat > %.dog") @?=
            (Right [Lt "cat > ",St,Lt ".dog"]),
      -- Targets, Prerequisites and Commands
      testCase "Testing command.1" $
        (parseme command "cat > %.dog") @?=
            (Right [Lt "cat > ",St,Lt ".dog"]),
      testCase "Testing commands.1" $
        (parseme commands "hello_world.foo > o") @?=
            (Right [[Lt "hello_world.foo > o"]]),
      testCase "Testing commands.2" $
        (parseme commands "hello_world.foo > o\n\t command2_%") @?=
            (Right [[Lt "hello_world.foo > o"],[Lt " command2_",St]]),
      testCase "Testing commands.3" $
        (parseme commands "hello_world.foo > o \n\t command2_%\n\t co%and3%") @?=
            (Right [[Lt "hello_world.foo > o "],[Lt " command2_",St],[Lt " co",St,Lt "and3",St]]),
      testCase "Testing preReqs.1" $
        (parseme preReqs "preReq_1") @?=
            (Right [[Lt "preReq_1"]]),
      testCase "Testing preReqs.2" $
        (parseme preReqs "preReq_1 preReq2_%") @?=
            (Right [[Lt "preReq_1"],[Lt "preReq2_",St]]),
      testCase "Testing preReqs.3" $
        (parseme preReqs "preReq_1 preReq2_% pre%req3%") @?=
            (Right [[Lt "preReq_1"],[Lt "preReq2_",St],[Lt "pre",St,Lt "req3",St]]),
      testCase "Testing targets.1" $
        (parseme targets "target_1") @?=
            (Right [[Lt "target_1"]]),
      testCase "Testing targets.2" $
        (parseme targets "target_1 target2_%") @?=
            (Right [[Lt "target_1"],[Lt "target2_",St]]),
      testCase "Testing targets.3" $
        (parseme targets "target_1 target2_% tar%get3%") @?=
            (Right [[Lt "target_1"],[Lt "target2_",St],[Lt "tar",St,Lt "get3",St]]),
      --- Rule
      testCase "Testing rule.1" $
        (parseme rule "target_1: preReq_1\n\t hello_world.foo > o \n") @?=
            (Right (Rule [[Lt "target_1"]] [[Lt "preReq_1"]] [[Lt " hello_world.foo > o "]])),
      testCase "Testing rule.2" $
        (parseme rule "target_1 target2_%: preReq_1 preReq2_\n\thello_world.foo > o \n\t command2_%\n") @?=
            (Right (Rule [[Lt "target_1"],[Lt "target2_",St]] [[Lt "preReq_1"],[Lt "preReq2_"]] [[Lt "hello_world.foo > o "],[Lt " command2_",St]])),
      testCase "Testing rule.3" $
        (parseme rule "target_1 target2_% tar%get3%: preReq_1 preReq2_ pre%req3%\n\t hello_world.foo > o \n\t command2_%\n\t co%and3%\n") @?=
            (Right (Rule [[Lt "target_1"],[Lt "target2_",St],[Lt "tar",St,Lt "get3",St]] [[Lt "preReq_1"],[Lt "preReq2_"],[Lt "pre",St,Lt "req3",St]] [[Lt " hello_world.foo > o "],[Lt " command2_",St],[Lt " co",St,Lt "and3",St]])),
      -- Rules
      testCase "Testing rules.1" $
        (parseme (rules []) "foo: foo.o bar.o\n\t cc -o foo foo.o bar.o\n %.o: %.c\n\tcc -S %.c\n\tas -o %.o %.s\n") @?=
            (Right [Rule [[Lt "foo"]] [[Lt "foo.o"],[Lt "bar.o"]] [[Lt " cc -o foo foo.o bar.o"]],Rule [[St,Lt ".o"]] [[St,Lt ".c"]] [[Lt "cc -S ",St,Lt ".c"],[Lt "as -o ",St,Lt ".o ",St,Lt ".s"]]]),
	  -- makeFile
      testCase "Testing makefile.1" $
        (parseme makeFile "foo: foo.o bar.o\n\t cc -o foo foo.o bar.o\n %.o: %.c\n\tcc -S %.c\n\tas -o %.o %.s\n") @?=
            (Right [Rule [[Lt "foo"]] [[Lt "foo.o"],[Lt "bar.o"]] [[Lt " cc -o foo foo.o bar.o"]],Rule [[St,Lt ".o"]] [[St,Lt ".c"]] [[Lt "cc -S ",St,Lt ".c"],[Lt "as -o ",St,Lt ".o ",St,Lt ".s"]]])
    ]

qcTests :: TestTree
qcTests = testGroup "QuickCheck tests" []

goldenTests :: TestTree
goldenTests = testGroup "Golden tests" []

main :: IO ()
main = defaultMain $ testGroup "White-box parser tests"
  [ unitTests, qcTests, goldenTests ]
