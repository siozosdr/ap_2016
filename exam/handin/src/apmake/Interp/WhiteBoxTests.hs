module Interp.WhiteBoxTests where

import Ast
import Interp.Impl

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
-- import Test.Tasty.Golden


unitTests :: TestTree
unitTests = testGroup "Unit tests" 
    [ testCase "Testing environment.1" $
        (runAPMakeM (getEnv) e1) @?=
            (e1,e1),
      testCase "Testing environment.2" $
        (runAPMakeM (getEnv) e2) @?=
            (e2,e2),
      testCase "Testing getVar.1" $
        (runAPMakeM (getVar "foo") e1) @?=
            ((ps11, cs11), e1),
      testCase "Testing getVar.2" $
        (runAPMakeM (getVar "bar.o") e1) @?=
            ((ps13, cs13), e1),
      testCase "Testing getVar.3" $
        (runAPMakeM (getVar "hello.o") e2) @?=
            ((ps24, cs24), e2),
      testCase "Testing getVar.4" $
        (runAPMakeM (getVar "hello") e2) @?=
            ((ps21, cs21), e2),
      testCase "Testing evalTemplate.1" $
        (runAPMakeM (evalTemplate [Lt "hello.o"]) e1) @?=
            (("hello.o"), e1),
      testCase "Testing evalTemplate.2" $
        (runAPMakeM (evalTemplate [Lt "g++ main.o factorial.o hello.o -o hello"]) e1) @?=
            (("g++ main.o factorial.o hello.o -o hello"), e1),
      testCase "Testing evalTemplates.1" $
        (runAPMakeM (evalTemplates cs21) e1) @?=
            (["g++ main.o factorial.o hello.o -o hello"], e1),
      testCase "Testing checkRule.1" $
        (checkRule (Rule ts11 ps11 cs11) ) @?=
            (True),
      testCase "Testing checkRule.2" $
        (checkRule (Rule ts12 ps12 cs12) ) @?=
            (True),
      testCase "Testing checkRule.3" $
        (checkRule (Rule ts13 ps13 cs13) ) @?=
            (True),
      testCase "Testing checkRule.4" $
        (checkRule (Rule ts21 ps21 cs21) ) @?=
            (True),
      testCase "Testing checkRule.5" $
        (checkRule (Rule ts22 ps22 cs22) ) @?=
            (True),
      testCase "Testing checkRule.6" $
        (checkRule (Rule ts23 ps23 cs23) ) @?=
            (True),
      testCase "Testing checkRule.7" $
        (checkRule (Rule ts24 ps24 cs24) ) @?=
            (True),
      testCase "Testing checkRule.8" $
        (checkRule (Rule ts12 ps12 cs12) ) @?=
            (True),
      testCase "Testing checkRule.9" $
        (checkRule (Rule ts12 ps12 cs12) ) @?=
            (True),
      testCase "Testing checkRule.10" $
        (checkRule (Rule ts12 ps12 cs12) ) @?=
            (True),
      testCase "Testing checkRule.11" $
        (checkRule (Rule bts12 bps12 bcs12) ) @?=
            (False),
      testCase "Testing checkRule.12" $
        (checkRule (Rule bts12 bps12 bcs12) ) @?=
            (False),
      testCase "Testing checkRule.13" $
        (checkRule (Rule bts12 bps12 bcs12) ) @?=
            (False),
      testCase "Testing checkRule.14" $
        (checkRule (Rule bts12 bps12 bcs12) ) @?=
            (False),
      testCase "Testing checkRule.15" $
        (checkRule (Rule bts12 bps12 bcs12) ) @?=
            (False),
      testCase "Testing checkRule.16" $
        (checkRule (Rule bts12 bps12 bcs12) ) @?=
            (False),
      testCase "Testing evalMakefile.1" $
        (runAPMakeM (evalMakefile mf1 "foo" f1 2) e1) @?=
            (Just ["cc -c foo.c","as -o bar.o bar.s","cc -o foo foo.o bar.o","nm foo > foo.map","strip foo"], e1),
      testCase "Testing evalMakefile.2" $
        (runAPMakeM (evalMakefile mf1 "foo.o" f1 2) e1) @?=
            (Just ["cc -c foo.c"], e1),
      testCase "Testing evalMakefile.3" $
        (runAPMakeM (evalMakefile mf1 "bar.o" f1 2) e1) @?=
            (Just ["as -o bar.o bar.s"], e1),
      testCase "Testing evalMakefile.3" $
        (runAPMakeM (evalMakefile mf2 "main.o" f2 2) e2) @?=
            (Just ["g++ -c main.cpp"], e2),
      testCase "Testing evalMakefile.4" $
        (runAPMakeM (evalMakefile mf2 "factorial.o" f2 2) e2) @?=
            (Just ["g++ -c factorial.cpp"], e2),
      testCase "Testing evalMakefile.5" $
        (runAPMakeM (evalMakefile mf2 "hello" f2 2) e2) @?=
            (Just ["g++ -c main.cpp","g++ -c factorial.cpp","g++ -c hello.cpp","g++ main.o factorial.o hello.o -o hello"], e2)]


qcTests :: TestTree
qcTests = testGroup "QuickCheck tests" []

goldenTests :: TestTree
goldenTests = testGroup "Golden tests" []

main :: IO ()
main = defaultMain $ testGroup "White-box interpreter tests"
  [ unitTests, qcTests, goldenTests ]

-- ========= Testing definitions ==========

-- initialize example environments
env1 :: ((), Environment)
env1@((), e1) = runAPMakeM (regRules mf1) initialEnvironment 
env2 :: ((), Environment)
env2@((), e2) = runAPMakeM (regRules mf2) initialEnvironment

-- example good target,prerequisites, commands
ts11 :: [[Frag]]
ts11 = [[Lt "foo"],[Lt "foo.map"]] 
ps11 :: [[Frag]]
ps11 = [[Lt "foo.o"],[Lt "bar.o"]]
cs11 :: [[Frag]]
cs11 = [[Lt "cc -o foo foo.o bar.o"],[Lt "nm foo > foo.map"],
             [Lt "strip foo"]]
ts12 :: [[Frag]]             
ts12 = [[Lt "foo.o"]]
ps12 :: [[Frag]]
ps12 = [[Lt "foo.c"]]
cs12 :: [[Frag]]
cs12 = [[Lt "cc -c foo.c"]]

ts13 :: [[Frag]]
ts13 = [[Lt "bar.o"]]
ps13 :: [[Frag]]
ps13 = [[Lt "bar.s"]]
cs13 :: [[Frag]]
cs13 = [[Lt "as -o bar.o bar.s"]]

ts21 :: [[Frag]]
ts21 = [[Lt "hello"]]
ps21 :: [[Frag]]
ps21 = [[Lt "main.o"],[Lt "factorial.o"],[Lt "hello.o"]]
cs21 :: [[Frag]]
cs21 = [[Lt "g++ main.o factorial.o hello.o -o hello"]]

ts22 :: [[Frag]]
ts22 = [[Lt "main.o"]]
ps22 :: [[Frag]]
ps22 = [[Lt "main.cpp"]]
cs22 :: [[Frag]]
cs22 = [[Lt "g++ -c main.cpp"]]

ts23 :: [[Frag]]
ts23 = [[Lt "factorial.o"]]
ps23 :: [[Frag]]
ps23 = [[Lt "factorial.cpp"]]
cs23 :: [[Frag]]
cs23 = [[Lt "g++ -c factorial.cpp"]]

ts24 :: [[Frag]]
ts24 = [[Lt "hello.o"]]
ps24 :: [[Frag]]
ps24 = [[Lt "hello.cpp"]]
cs24 :: [[Frag]]
cs24 = [[Lt "g++ -c hello.cpp"]]
-- example good makefiles
mf1 :: Makefile
mf1 = 
      [ Rule ts11 ps11 cs11,
        Rule ts12 ps12 cs12 ,
        Rule ts13 ps13 cs13
      ]
mf2 :: Makefile
mf2 = 
      [ Rule ts21 ps21 cs21,
        Rule ts22 ps22 cs22,
        Rule ts23 ps23 cs23,
        Rule ts24 ps24 cs24
      ]

-- example bad targets, prerequisites, commands
bts11 :: [[Frag]]
bts11 = [[Lt "foo"],[Lt "foo.map"]] 
bps11 :: [[Frag]]
bps11 = [[Lt "foo.o"],[St, Lt ".o"]]
bcs11 :: [[Frag]]
bcs11 = [[Lt "cc -o foo foo.o bar.o"],[Lt "nm foo > foo.map"],
             [Lt "strip foo"]]
bts12 :: [[Frag]]
bps12 :: [[Frag]]
bts12 = [[Lt "foo.o"]]
bps12 = [[Lt "foo.c"]]
bcs12 :: [[Frag]]
bcs12 = [[Lt "cc -c", St, Lt ".c"]]

bts13 :: [[Frag]]
bts13 = [[Lt "bar.o"]]
bps13 :: [[Frag]]
bps13 = [[St, Lt ".s"]]
bcs13 :: [[Frag]]
bcs13 = [[Lt "as -o bar.o bar.s"]]

bts21 :: [[Frag]]
bts21 = [[Lt "hello"]]
bps21 :: [[Frag]]
bps21 = [[Lt "main.o"],[Lt "facto", St, Lt "rial.o"],[Lt "hello.o"]]
bcs21 :: [[Frag]]
bcs21 = [[Lt "g++ main.o factorial.o hello.o -o hello"]]

bts22 :: [[Frag]]
bts22 = [[Lt "main.o"]]
bps22 :: [[Frag]]
bps22 = [[Lt "main", St]]
bcs22 :: [[Frag]]
bcs22 = [[Lt "g++ -c main.cpp"]]

bts23 :: [[Frag]]
bts23 = [[Lt "factorial.o"]]
bps23 :: [[Frag]]
bps23 = [[Lt "factorial.cpp"]]
bcs23 :: [[Frag]]
bcs23 = [[Lt "g++ -c", St, Lt "rial.cpp"]]

bts24 :: [[Frag]]
bts24 = [[Lt "hello.o"]]
bps24 :: [[Frag]]
bps24 = [[St, Lt ".", St]]
bcs24 :: [[Frag]]
bcs24 = [[Lt "g++ -c hello.cpp"]]

bts25 :: [[Frag]]
bts25 = [[Lt "clean"]]
bps25 :: [[Frag]]
bps25 = [[Lt "prereq"]]
bcs25 :: [[Frag]]
bcs25 = [[Lt "rm ",St, Lt "o hello"]]

-- example bad makefiles
bmf1 :: Makefile
bmf1 = 
      [ Rule bts11 bps11 bcs11,
        Rule bts12 bps12 bcs12 ,
        Rule bts13 bps13 bcs13
      ]
bmf2 :: Makefile
bmf2 = 
      [ Rule bts21 bps21 bcs21,
        Rule bts22 bps22 bcs22,
        Rule bts23 bps23 bcs23,
        Rule bts24 bps24 bcs24,
        Rule bts25 bps25 bcs25
      ]
-- example functions
f1 :: [Char] -> Bool
f1 = (`elem` ["foo.c", "bar.s"])
f2 :: [Char] -> Bool
f2 = (`elem` ["factorial.cpp", "hello.cpp", "main.cpp"])

