module Parser
  ( ParseError
  , parseString
  , parseFile
  ) where

-- Parsec
import Parser.Impl
  ( parseString
  , parseFile
  )

import Text.ParserCombinators.Parsec ( ParseError )
{-
-- SimpleParse/ReadP
import Parser.Impl
  ( ParseError
  , parseString
  , parseFile
  )

-}
