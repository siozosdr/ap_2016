-module(tests).
-compile(export_all).
-import(test_command, [start/1, checkStatus/2, terminate/2, shutdown/1,count/2 ]).

test1(Limit) ->
	{_, S} = start(Limit),
	Results = repeat_f(Limit,
		fun(_) -> count(S,Limit) end),
	io:format("got results\n"),
	Final = lists:map(fun({_,R})-> R end, Results),
	Final.
test2(Limit) ->
	{_, S} = start(Limit),
	Results = repeat_f(Limit,
		fun(_) -> count(S,Limit) end),
	io:format("got results\n"),
	% get CID of each command
	CIDs = lists:map(fun({C,_})-> C end, Results),
	io:format("evaluating:\n"),
	% terminate each command
	lists:foreach(fun(C) -> terminate(S,C) end,CIDs),
	% check final status of commands
	Final = lists:map(fun(C)-> checkStatus(S, C) end, CIDs),
	Eval = lists:member(busy, Final),
	if 
		Eval == true ->
			false;
		Eval == false ->
			true
	end.

% helper function
repeat_f(N,F) ->
	lists:map(F, lists:seq(0, N-1)).